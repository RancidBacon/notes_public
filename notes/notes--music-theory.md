# Notes: Music Theory

[TOC]

## Resources

 * [`vpavlenko/study-music`](https://github.com/vpavlenko/study-music)
   -- "An 'awesome music theory' kinda wiki with books, resources & courses for studying everything about music & sound"


## Related Software/Libraries

 * [`adam-mcdaniel/music-generation`](https://github.com/adam-mcdaniel/music-generation)
   -- "music theory library in Rust for generating songs"

 * [`kennethreitz/pytheory`](https://github.com/kennethreitz/pytheory)
   -- "Music Theory for Humans."

 * [`ozankasikci/rust-music-theory`](https://github.com/ozankasikci/rust-music-theory)
   -- "music theory guide written in Rust."

 * [`CPJKU/partitura`](https://github.com/CPJKU/partitura)
   -- "python package for handling modern staff notation of music"

 * _(See also: <https://github.com/topics/music-theory>)_


## My Learning Log
