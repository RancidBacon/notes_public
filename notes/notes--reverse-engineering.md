# Notes: Reverse Engineering

_(See also: [Notes: C (Programming Language)](notes--c-lang.md), [Notes: Debugging with `gdb` and/or `lldb`](notes--debugging-with-gdb-lldb.md))_

## Tools

 * ["Mr Crowbar"](https://github.com/moralrecordings/mrcrowbar/) -- "[Python] library & model framework for reverse engineering binary file formats"

   _(See also: [web](https://moral.net.au/mrcrowbar/), [pypi](https://pypi.org/project/mrcrowbar/))_

   (Note: I'm not entirely sure what the docs situation is but [this exists](https://github.com/moralrecordings/mrcrowbar/blob/0.9.0/doc/source/components.rst).)

   Pre-built CLI tools include:

    * [`mrcdump`](https://github.com/moralrecordings/mrcrowbar/blob/0.9.0/README.rst#mrcdump-mrcrowbarutilshexdump)

    * [`mrcdiff`](https://github.com/moralrecordings/mrcrowbar/blob/0.9.0/README.rst#mrcdiff-mrcrowbarutilsdiff)

    * [`mrchist`](https://github.com/moralrecordings/mrcrowbar/blob/0.9.0/README.rst#mrchist-mrcrowbarutilshistdump) -- "slice up file & generate histogram of the byte data, which will give you a unique fingerprint for the type of data"

      This is the tool from this project which I've most used & think
      is most novel/unique[0].

      It's helpful for getting overview of/insight into the structure
      of an unknown binary file format (especially one that is likely
      to be a "container" comprised of multiple sections/chunks
      containing different types of data or data with different
      "characteristics", e.g. "ASCII text", "16-bit PCM audio data",
      "8-bit colour palette", "compressed data") by way of identifying
      sections of data with distinct characteristics (e.g. byte value
      ranges or entropy).

      [0] (And was the tool I most recently was trying to remember
          what it was named... :D (My guess of `mc-something` or
          `bce-something` or "something toolkit" was _~"close"_ but
          not quite the actual `mrc-something`... :) ))

    * [`mrcpix`](https://github.com/moralrecordings/mrcrowbar/blob/0.9.0/README.rst#mrcpix-mrcrowbarutilspixdump) -- "print out the data as a bitmap [image/picture]"

      IIRC I used this tool in some game-related reverse engineering
      for viewing textures contained in data/exe files.


 * [`Mahlet-Inc/hobbits`](https://github.com/Mahlet-Inc/hobbits)
   -- "multi-platform GUI for bit-based analysis, processing, & visualization"

 * [`MatrixEditor/caterpillar`](https://github.com/MatrixEditor/caterpillar)
   -- "Python 3.12+ library to pack & unpack structured binary data."

 * [`WerWolv/ImHex`](https://github.com/WerWolv/ImHex) (GUI)

   _(See also:
   [patterns repo](https://github.com/WerWolv/ImHex-Patterns),
   [web](https://imhex.werwolv.net/),
   [docs](https://docs.werwolv.net/imhex) (the docs do seem reasonable :) ),
   [patterns docs](https://docs.werwolv.net/pattern-language)
   )_

   (Update: *Amazing*, `imhex-1.34.0-x86_64.AppImage` appears to
            actually be packaged correctly & runs! :o
            (Although crash uploads were enabled in prefs, despite
            clicking "deny". :/ )
            (Also, it's rather easy to not notice
            the "continue" button in bottom right corner on the
            initial "splash screen/dialog". :p )
            (Though it _does_ still have some rather *\*cough\**
            "novel" UX choices...))

    * Important UI font size information: <https://docs.werwolv.net/imhex/common/fonts>

      (I changed UI font to `NotoMono-Regular` at a larger size.)

    * Apparently "Sound Visualizer" is one of the available
      "visualizers", how potentially convenient. :)

    * _See also: [Notes: Software : ImHex](notes--software--imhex.md)_

 * <https://binvis.io/> -- "visual analysis of binary files" (browser based)

 * [`gcarmix/HexWalk`](https://github.com/gcarmix/HexWalk)
   -- "Hex Viewer/Editor/Analyzer" (GUI + Cross-platform)



## Other Resources

 * [`dloss/binary-parsing`](https://github.com/dloss/binary-parsing) -- "list of generic tools for parsing binary data structures, such as file formats, network protocols or bitstreams."



## Uncategorized

 * "[Show HN: A Ghidra extension for exporting parts of a program as object files](https://news.ycombinator.com/item?id=41318133)"

    * Repo: <https://github.com/boricj/ghidra-delinker-extension>

   Related projects mentioned in comments:

    * [`wbenny/pdbex`: "utility for reconstructing structures and unions from the PDB into compilable C headers"](https://github.com/wbenny/pdbex)

    * <https://gitlab.com/dvdkon/pdb2hpp>

    * <https://github.com/intel/fffc>
      (via <https://news.ycombinator.com/item?id=41322371>: "...automatically generate [...] type-aware fuzzers for C APIs from DWARF info [...] Generating headers [...] was part of that.")

    * <https://github.com/endrazine/wcc> -- "Witchcraft Compiler Collection"

      (A classic...)

      (Oh! It's seemingly still being "actively" developed.)

      (Also: hey, it me: <https://github.com/endrazine/wcc/issues/32> :D With a "timely" reply! :D :D )

    * <https://github.com/jnider/delinker>

    * <https://github.com/acmel/dwarves> -- "Shows, manipulates data structure layout and pretty prints raw data." (via <https://man.archlinux.org/man/extra/pahole/pahole.1.en>)
