## Notes: Text-To-Speech Synthesis

> See also: [TODO]

### `espeak-ng`

#### `SSML`

 * [`docs/markup.md`](https://github.com/espeak-ng/espeak-ng/blob/ae74f733f3334a644b547c39e238f8c1ebfeec63/docs/markup.md)


##### Related `espeak-ng` + `SSML` issues

 * General:

    * `espeak-ng` has a hand-rolled XML parser with e.g. hardcoded numeric offsets.

       * e.g.: [`libespeak-ng/ssml.c`](https://github.com/espeak-ng/espeak-ng/blame/ae74f733f3334a644b547c39e238f8c1ebfeec63/src/libespeak-ng/ssml.c#L498-L979)

    * It seems to attempt to handle both valid & in-valid SSML markup--which makes *sense* in terms of "real world" usage but also makes implementation a lot more difficult.

      * e.g.: "[Add unquoted attributes support in SSML parser PR #1515](https://github.com/espeak-ng/espeak-ng/pull/1515/files)"


 * Specific issues:

    * "[strange pronunciation on some strings `#1533`](https://github.com/espeak-ng/espeak-ng/issues/1533#issuecomment-1321057240)"

    * "[Restructure ReadClause and TranslateClause to better handle Japanese, emoji and other things. `#369`](https://github.com/espeak-ng/espeak-ng/issues/369)"

   * "[SSML input `<voice name="value" age="value">` changes to default voice mid-sentence `#737`](https://github.com/espeak-ng/espeak-ng/issues/737)"

   * "[SSML prosody mostly unsupported? `#1711`](https://github.com/espeak-ng/espeak-ng/issues/1711#issuecomment-1526191574)"

   * "[intonation change in SSML stack `#975`](https://github.com/espeak-ng/espeak-ng/issues/975)"

   * "[consecutive SSML tags aren't processed `#410`](https://github.com/espeak-ng/espeak-ng/issues/410)"

   * "[`tests/ssml.test`:`test_ssml_audio` `<prosody>`: wrong test fails `#947`](https://github.com/espeak-ng/espeak-ng/issues/947)"


#### Pronunciation data & rules

 * [`dictsource/en_list`](https://github.com/espeak-ng/espeak-ng/blob/ae74f733f3334a644b547c39e238f8c1ebfeec63/dictsource/en_list)

 * [`dictsource/en_rules`](https://github.com/espeak-ng/espeak-ng/blob/ae74f733f3334a644b547c39e238f8c1ebfeec63/dictsource/en_rules)

 * [`dictsource/en_emoji`](https://github.com/espeak-ng/espeak-ng/blob/ae74f733f3334a644b547c39e238f8c1ebfeec63/dictsource/en_emoji)


### Rust language

#### Crates

##### Language & Pronunciation

 * https://lib.rs/crates/lngcnv

 * [`grapheme_to_phoneme`](https://lib.rs/crates/grapheme_to_phoneme#readme-grapheme_to_phoneme) -- "prediction tool to turn graphemes (words) into Arpabet phonemes"

    * "...only focuses on the prediction model (OOV prediction). CMUDict lookup and hetronym handling are best handled by other libraries, such as my Arpabet crate." (See below.)

 * https://lib.rs/crates/arpabet -- "library for speech synthesis that leverages Carnegie Mellon University's CMUdict"

 * "`shnewto/ttaw`: a piecemeal natural language processing library"
   <https://github.com/shnewto/ttaw> / https://lib.rs/crates/ttaw

 * <https://lib.rs/crates/rspanphon> -- "extracts articulatory features from IPA strings and implements operations on them"

 * <https://lib.rs/crates/text2num>

 * <https://lib.rs/crates/text-to-sounds>


##### `espeak`-related

 * https://lib.rs/crates/espeaker -- "eSpeak NG playback library"

 * https://lib.rs/crates/espeakng -- "A safe Rust wrapper around espeakNG via `espeakNG-sys`" (See: [`text_to_phonemes()`](https://github.com/GnomedDev/espeakNG-rs/blob/5e13b342a8f37e7e14acef30edeb0ee353dd9eec/src/lib.rs#L299-L330).)

   * https://github.com/Discord-TTS/tts-service -- "HTTP microservice using Axum to generate TTS from an HTTP reqwest" (!)

      * (a la `opentts`, maybe? [TODO: Mention Piper. :) ])

      * (See also: [`src/polly.rs` for `SSML` generation](https://github.com/Discord-TTS/tts-service/blob/8dc3d045b4218737614237f29839385b806203ba/src/polly.rs#L53).)


##### `SSML` & `XML` Parsing

 * https://github.com/RazrFalcon/roxmltree -- "Represent an XML 1.0 document as a read-only tree."

   * Uses: https://lib.rs/crates/xmlparser

 * https://lib.rs/crates/quick-xml -- "High performance xml reader and writer"

 * https://lib.rs/crates/xml5ever -- "push based XML parser library that trades well-formedness for error recovery."

    * https://lib.rs/crates/html5ever -- "High-performance browser-grade HTML5 parser"

 * https://github.com/untitaker/html5gum/blob/main/README.md#alternative-html-parsers

 * https://lib.rs/crates/rxml -- "Minimalistic, restricted XML 1.0 parser which does not include dangerous XML features"

 * https://lib.rs/crates/maybe_xml

    * https://docs.rs/maybe_xml/0.3.0/maybe_xml/index.html#well-formed-vs-malformed-document-processing

 * https://lib.rs/crates/html_parser

 * https://lib.rs/crates/html_editor

 * https://lib.rs/crates/reader_for_microxml

 * <https://lib.rs/crates/text-to-polly-ssml> -- "Converts text to [AWS] Polly SSML. Using a bad format."


### Other

 * https://www.w3.org/TR/pronunciation-lexicon/ -- "Pronunciation Lexicon Specification (PLS) Version 1.0" (via AWS Polly SDK docs)

 * "`words/weasels`: List of (possible) English weasel words"
   <https://github.com/words/weasels>

 * "`words/fillers`: List of (possible) English filler words"
   <https://github.com/words/fillers>

 * "`words/hedges`: List of (possible) English hedge words"
   <https://github.com/words/hedges>

 * "`words/buzzwords`: List of (possible) English buzzword words"
   <https://github.com/words/buzzwords>

 * "`sibozhang/Text2Video`: ICASSP 2022: 'Text2Video: text-driven talking-head video synthesis with phonetic dictionary'."
   <https://github.com/sibozhang/Text2Video>



#### Data

 * <https://github.com/CUNY-CL/wikipron> -- "Massively multilingual pronunciation mining". Sure.

 * "`JoseLlarena/Britfone`: British English pronunciation dictionary"
   <https://github.com/JoseLlarena/Britfone>

 * "`words/cmu-pronouncing-dictionary`: The 134,000+ words and their pronunciations in the CMU pronouncing dictionary"
   <https://github.com/words/cmu-pronouncing-dictionary>

 * "`abuccts/wikt2pron`: A Python toolkit converting pronunciation in enwiktionary xml dump to cmudict format"
   <https://github.com/abuccts/wikt2pron>

 * "`rhasspy/wiktionary2dict`: Tool for extracting IPA pronunciations from Wiktionary XML dump"
   <https://github.com/rhasspy/wiktionary2dict> lol. oh. :D

    * "See gruut for a pre-compiled set of lexicons."

 * "`rhasspy/gruut`: A tokenizer, text cleaner, and phonemizer for many human languages."
   <https://github.com/rhasspy/gruut> Soo... Hmmm. (Includes SSML.)

 * "`rhdunn/amepd`: American English Pronunciation Dictionary"
   <https://github.com/rhdunn/amepd>

 * "[The CMU Pronouncing Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict)"

 * Wiktionary-related:

    * <https://en.wiktionary.org/wiki/Appendix:English_pronunciation> -- includes regional Engish-related comparisons.

    * "[Wiktionary data & API](https://en.wiktionary.org/wiki/User:Amgine/Wiktionary_data_%26_API)"

    * <https://github.com/Kyubyong/pron_dictionaries>
