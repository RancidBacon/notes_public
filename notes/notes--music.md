## Notes: Music

### Notation

 * <https://github.com/DigiScore/neoscore> -- "Python library for notating music in a graphics-first paradigm"


### Applications

 * <https://lib.rs/crates/songrec> -- "open-source Shazam client for Linux, written in Rust"

    * "Recognize audio from an arbitrary audio file."

    * "Ability to recognize songs from your speakers...on compatible PulseAudio setups."

    * "Generate a lure from a song that, when played, will fool Shazam into thinking that it is the concerned song." :D

 * <https://github.com/freealise/vocalise> -- "Compose song lyrics, poetry or rap..."

   * See also: <https://github.com/freealise/vowelsynth>


### Uncategorized

 * "Python Notebooks for Fundamentals of Music Processing": <https://www.audiolabs-erlangen.de/resources/MIR/FMP/C0/C0.html>
