## Notes: Rust (Programming Language)

### Crates

Some "interesting" crates I may want to revisit in future.

#### Audio

 * "[Plop Grizzly / audio / pitch](https://gitlab.com/plopgrizzly/audio/pitch)" / "[`pitch`](https://lib.rs/crates/pitch)" -- "Quickly and accurately determine the pitch and volume of a sound sample"

 * [`pitch-detection`](https://lib.rs/crates/pitch-detection) -- "collection of algorithms to determine the pitch of a sound sample"

 * [`Rocoder`](https://lib.rs/crates/rocoder) / [`ajyoon/rocoder`](https://github.com/ajyoon/rocoder) -- "live-codeable phase vocoder"

 * [`aubio-rs`](https://lib.rs/crates/aubio-rs) -- "bindings for aubio library to label music and sounds"

 * [`aubio`](https://lib.rs/crates/aubio) (More recently updated than `aubio-rs`?)

 * https://lib.rs/crates/microdsp -- "DSP algorithms and utilities...embedded friendly"

 * <https://lib.rs/crates/pyin> -- "pYIN pitch detection algorithm written in Rust" Mentions "...implementation is based on `librosa`", which also has other feature extraction functionality: <https://librosa.org/doc/0.10.0/feature.html#spectral-features>

 * <https://lib.rs/crates/pitch_shift> -- "pitch-shifting using the phase vocoder technique"

 * <https://lib.rs/crates/irapt> -- "implementation of the IRAPT pitch estimation algorithm"

   * See also:

      * https://github.com/in-formant/in-formant
      * https://github.com/in-formant/libformants
      * https://github.com/in-formant/in-formant-vst-midi
      * https://github.com/in-formant/offline-speech-analysis

 * <https://lib.rs/crates/pink-trombone> -- "Vocal cords simulator...revised version of the Pink Trombone speech synthesizer"

   * Online demo is amazing: <https://chdh.github.io/pink-trombone-mod> :D

   * See also: [`PinkTrombone.set_musical_note()`](https://docs.rs/pink-trombone/0.2.1/pink_trombone/struct.PinkTrombone.html#method.set_musical_note)

 * <https://lib.rs/crates/bliss-audio> -- "song analysis library for making playlists"



#### Music / Music Theory

 * [Staff](https://lib.rs/crates/staff) -- "Music theory library with midi, notes, chords, scales, and more"

 * [kord](https://lib.rs/crates/kord) -- "tool to easily explore music theory principles" (includes NPM functionality)

 * [`pitch_calc`](https://lib.rs/crates/pitch_calc) -- "provides functions and methods for converting between frequency, midi-step and letter-octave"
