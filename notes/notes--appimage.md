# Notes: AppImage

## Documented CLI options (non-exhaustive)

### `--appimage-help`

### `--appimage-mount`

### `--appimage-offset`


## Undocumented CLI options & environment variables

Common AppImage runtimes have a number of (intentionally) undocumented
CLI options & environment variables.

These may be helpful in "non-typical" circumstances but their use is
at your own risk; explicitly not supported; not guaranteed to exist in
any/future runtimes; and, (occasional and/or regular use) may be in
direct contravention of the AppImage format designer's wishes.


### `--appimage-extract-and-run` & `APPIMAGE_EXTRACT_AND_RUN`


### `FUSERMOUNT_PROG`

e.g. `FUSERMOUNT_PROG=/bin/fusermount </path/to/file.AppImage>`

This is a way to override the location/name of the privileged (via
set-user-ID) `fusermount` executable binary which may be used to mount
the SquashFS file system embedded in the `.AppImage` file if the
current user has insufficient privileges to mount it directly.

This might be useful if the runtime embedded in the AppImage is
e.g. buggy and/or makes incorrect assumptions about the name/location
of the relevant executable on the platform/distro you are using
(e.g. whether the executable is named `fusermount` or `fusermount3`
and/or whether the executable is located in `/bin/` or `/usr/bin/`.

This environmental variable is what you may want to try if you get the
following error message when trying to run an AppImage file (e.g. in a
terminal CLI via `</path/to/file.AppImage> --version`):

```
fuse: failed to exec fusermount: Permission denied

Cannot mount AppImage, please check your FUSE setup.
You might still be able to extract the contents of this AppImage
if you run it with the --appimage-extract option.
See https://github.com/AppImage/AppImageKit/wiki/FUSE
for more information
open dir error: No such file or directory
```

NOTE: Contrary to the man page for `fusermount` (quoted below) it is
      *not* only used to *unmount* FUSE filesystems--while a _user_
      can not use the `fusermount` binary to mount FUSE filesystems
      directly, the FUSE library (`libfuse`) may/*does* use/require
      `fusermount` in order to mount FUSE filesystems for unprivileged
      users:

      > "fusermount is a program to unmount FUSE filesystems."


### `TARGET_APPIMAGE`

e.g. `TARGET_APPIMAGE=</path/to/target/file.AppImage> </path/to/runtime/file.AppImage>  --appimage-offset`

This is a way to use the "AppImage runtime" executable from one
AppImage file (e.g. `/path/to/runtime/file.AppImage`) to operate on a
different AppImage file (e.g. `/path/to/target/file.AppImage`).

This might be useful if the runtime embedded in the target AppImage is
e.g. incorrectly built/packaged, buggy, or, doesn't support your
platform architecture but you have another AppImage that works
correctly. Note: You may still run into issues if e.g. the target
AppImage uses a compression format that the working AppImage runtime
does not support.


## `AppRun`-related environment variables

(See also: <https://docs.appimage.org/packaging-guide/environment-variables.html>)

 * `APPDIR`

 * `APPIMAGE`

 * `OWD`

 * `ARGV0` ("Type 2" AppImage Runtime only)
