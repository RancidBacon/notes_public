## Notes: Music Composition

_See also:_ [Notes: Music Theory](notes--music-theory.md), [Notes: Music](notes--music.md)

### Resources

 * While these _are_ "promotional"/"marketing" videos for Splice, the
   educational content IMO seems *excellent* and is delivered by someone
   ([`@nickthechen`](http://www.youtube.com/@nickthechen),
   @[yt](https://www.youtube.com/channel/UCTr_KCnk2nXeXvReB6FlMZw),
   @[web](https://www.nickthechen.net/))
   who clearly seems to have _in depth_ knowledge of the material (and
   more) while also being skilled at communicating to an audience who
   possess a wide level of skill levels (from absolute beginner to
   more experienced) and keeping them engaged:

    * ["How to make drum patterns" (Part One)](https://www.youtube-nocookie.com/embed/zOVSOvsTXto)
      <br/>
      _(Actual "YouTubey"-style title: "How to make drum patterns - rhythms every producer SHOULD know [...]")_

    * ["How to make drum patterns" (Part Two)](https://www.youtube-nocookie.com/embed/33Fk1G1DKRY)
      <br/>
      _(Actual "YouTubey"-style title: "9 drum patterns every producer should know | How to make beats in many genres [...]")_

    * ["chord progressions [...] how to play/examples/music theory"](https://www.youtube-nocookie.com/embed/LhqkIZ25lUI)
      <br/>
      _(Actual "YouTubey"-style title: "7 chord progressions every producer SHOULD know - how to play/examples/music theory [...]")_

      This one probably benefits from some explanation [not least because
      when writing up this list I needed to rewatch the start of the
      video to remind myself why on earth I'd thought a "list of chord
      progressions" video was useful?! :D ]:

      The valuable part of this video is not the "list of chord
      progressions" itself but, rather:

       * the _excellent_ introductory visual explanation of *how* to play
         chords/chord progession [featuring "The T-Rex" :) ];

       * the explanation of some of music theory-related notation used;

       * the visualisation presented alongside associated example songs; and,

       * the chord progression depicted as it appears within a DAW piano roll.

      The multiple forms of visualisation/"aural"isation is particularly
      helpful in terms of providing multiple opportunities for viewers
      with different experience/familiarity to achieve understanding
      while also helping "make connections" between more casual and more
      formal ways of presenting/notating chords & chord progressions.

      (Make sure to check out _my_ personal favourite: #4. :D )

      (Note: Directly linked Embed URL may not play, so you may need to
      click again to watch on main YouTube site.)

    * ["4 Ways to Make Better Chord Progressions"](https://www.youtube.com/embed/fuJG7z2uGAs)
      <br/>
      _(Actual "YouTubey"-style title: "4 EASY Ways to Make BETTER Chord Progressions [...]")_

      Includes: Orchestration, Asymmetrical Shapes, Rootless Voicings, Inversions.

    * ["How to make basslines - house plucks to 808s"](https://www.youtube.com/embed/hf2VbYno0_4)

    * ["10 types of melodies"](https://www.youtube.com/embed/Q8qSHzrz-Tc)
      <br/>
      _(Actual "YouTubey"-style title: "10 types of melodies you should know about [...]")_

   Apparently these videos are part of this playlist:

    * ["LEVEL UP Your Music Theory" playlist](https://www.youtube.com/playlist?list=PL1PEZ--NZi2Kru9AxxGk9-yoMQ5PqVukh)

   There is also this playlist, which presumably has other videos by the same person:

    * ["Nick from Splice" playlist](https://www.youtube.com/playlist?list=PLKpnn0YIz1FuI-Uwc0iLdm-L4gTsl6AwX)

   (Also of note: it seems the videos & animations are apparently created using Blender. :) )
