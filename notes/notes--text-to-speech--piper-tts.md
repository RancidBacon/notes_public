## Notes: Piper TTS (a.k.a. Larynx 2)

_"...fast, local neural text to speech system that sounds great..."_

URL: <https://github.com/rhasspy/piper>

### Voice Models

#### LibriTTS (a.k.a. `libritts`)

 * Number of Speakers: **904!!!**

 * [`speaker_id_map`  in `en-us/libritts/high/config.json`](https://github.com/rhasspy/piper-samples/blob/a2635079d893e2846b590c9c17da42a963e44499/en-us/libritts/high/config.json#L407):

    ```
    ...
        "num_speakers": 904,
        "speaker_id_map": {
            "3922": 0,
            "8699": 1,
            "4535": 2,
    ...
    ```

    e.g. Speaker IDs from original `LibriTTS` paper IIUC (*Paper Speaker ID* to *Piper Model Speaker ID*):

    * [`204` `-->` `547`](https://github.com/rhasspy/piper-samples/blob/a2635079d893e2846b590c9c17da42a963e44499/en-us/libritts/high/config.json#L956)
    * [`1121` `-->` `441`](https://github.com/rhasspy/piper-samples/blob/a2635079d893e2846b590c9c17da42a963e44499/en-us/libritts/high/config.json#L850)
    * [`5717` `-->` `66`](https://github.com/rhasspy/piper-samples/blob/a2635079d893e2846b590c9c17da42a963e44499/en-us/libritts/high/config.json#L475)
    * *Missing(?):* 19, 103, 1841 (Seem to all be in `train-clean-100` (another ~250 voices?!) subset whereas others are in `train-clean-360` subset?)

 * Seemingly there is metadata about the original LibriTTS release data available, e.g. [`filelists/libritts_speakerinfo.txt`](https://github.com/NVIDIA/flowtron/blob/d149bc466b8325026aeb68aa2fef24337e7463b2/filelists/libritts_speakerinfo.txt).

   > *Update:*
   >
   > *It seems the file `libritts_speakerinfo.txt` was originally named `SPEAKERS.TXT` and contained in the `raw-metadata.tar.gz` archive file of the "[LibriSpeech ASR corpus](http://www.openslr.org/12/)".*

   This includes technical metadata:

    * Speaker ID number
    * Corpus subset (e.g. `train-clean-360`)
    * Total minutes of speech from the speaker

   > *Note:*
   >
   > Eventually I learned the number in the name of the subset refers to a **number of *hours*** *not* a number of *speakers*, e.g. "`train-clean-100" is "training set of 100 *hours* 'clean' speech".
   >
   > (The number of hours refers to the duration in the *original* LibriSpeech corpus not the shorter duration derived LibriTTS corpus.)
   >
   > -- *via "[LibriSpeech ASR corpus](http://www.openslr.org/12/)" page*

   However, the metadata *also* includes *speaker demographic* information:

    * A field referred to as both:

       * `SEX` (in column name); and,

       * `gender` (in accompanying documentation)

      with the description:

       * "`'F' for female, 'M' for male`"

    * Another field with, perhaps most concerning, the *Speaker's Name*, named:

       * `NAME`/`name`

      with the description:

       * "`the name under which the reader is registered in LibriVox`".

      ("LibriVox" being the source of the original audio recordings which were used by "LibriSpeech" & subsequently "LibriTTS".)

      While some of entries in the name field are clearly pseudonyms, others seem to be *a person's actual/real/legal name*--and, in *at least* a couple of instances noticed while skim reading, what seem to be *"(year of birth - year of death)" annotations*!

   #### Ethics of LibriVox Recordings use for TTS

   For me, in spite of the obviously exciting potential for a dataset of at least one thousand individual voices--not to mention one curated with at least an effort to reduce gender-related bias, the existence of per-speaker metadata is one of at least a couple ethical questions raised by the use of LibriVox recordings for TTS synthesis purposes.

   (Regardless of whether such use is *compliant* with and/or such information is even required for compliance with the original ~~recording's~~ *datasets's* `CC BY 4.0` license.

   On closer inspection, the license for the original *recordings* is [Public Domain](https://librivox.org/pages/public-domain/) and the LibriVox site includes a section ["What Can Other People Do with LibriVox Recordings"](https://librivox.org/pages/public-domain/#3) which, in part, attempts to (IMO, responsibly) make clear the implications of the speaker's donation:

   > *LibriVox recordings are in the public domain, which means people can do anything they like with them.*
   >
   > *[...example uses which speakers may not like elided...]*
   >
   > *So be aware of what you are doing when you free your recordings and text into the public domain. You really have to let go!*

   "*Using Machine Learning to clone your voice & associate it with your LibriVox username*" is *not* one of the possibilities listed--which I suspect reflects the time when it was written.)

   I had already been curious about what the LibriVox's speaker community's response had been--if they were aware & if there had been any response--to the use of their work for single voice TTS synthesis but now I'm doubly so.

   I do think there's nuances between use of the recordings for:

    * Speech Recognition (the original intent for the LibriSpeech dataset);

    * TTS Voice Models which are an amalgam of multiple voices; and,

    * TTS Voice Models generated from a single voice (let alone with associated name).

   Will a generous LibriVox speaker one day be watching a video, suddenly hear their own voice speaking words they've never said and then find themselves listed by name in the video's credits?!

   As a point of comparison, it is interesting to note that [downloads of Mozilla's Common Voice (CC-0 licensed) dataset](https://commonvoice.mozilla.org/en/datasets) (specifically created *for* "machine-learning based speech technology") require the downloader to agree "*to not attempt to determine the identity of speakers in the Common Voice dataset*".

   Given there is no license-compliance-driven *requirement* to associate speaker names with individual Speaker IDs it seems that moving forward it might be prudent/responsible to use generated pseudonyms for single-speaker trained voices from the LibriVox/LibriSpeech/LibriTTS dataset.

   (Indeed, I'm not currently aware of any other similar "large" datasets that use much more than a number for speaker id. Whether there are benefits for having generated pseudonyms rather than numbers that outweigh potential concerns, e.g. around bias and/or stereotyping, is a topic for another time.)

   #### Is this merely a brief historical oddity?

   Perhaps the ethical questions related to single-speaker based voice models are going to mostly be a moot point outside of the small window of time we currently find ourselves in?

   With source recordings available for so many voices it seems likely only a matter of time before a CC-0/PD/CC-BY licensed multi-speaker trained voice model is produced from the same data with controls provided to generate a unique voice which matches no particular speaker in the original dataset.

   Surely it's only a "simple matter" of a "small amount GPU time" donated to the Commons to quiet the most egregious of these concerns?

   > *Also, this is in no way specific/unique to Piper, it's just this happened to be the page where I started typing these thoughts.* :)


### Potential Enhancements

#### LibriTTS: Voice Audition/Selection/Navigation Workflows

The 900+ speaker voices available in the LibriTTS voice model means that people using Open Source offline speech synthesis now find themselves in the extremely unfamiliar situation of having an almost unmanageable number of voices to choose from!

This suggests opportunities for:

 * Automated analysis/classification/tagging of voices (e.g. vocal characterstics and audio quality).
 * Search/filter available voices based on vocal characteristics.
 * Manual/semi-automated tagging of speakers based on original metadata.


#### LibriTTS: Improved Audio Quality

 * [TODO]


### Related Links / References

 * <https://en.wikipedia.org/wiki/Mel-frequency_cepstrum#MFCC_for_speaker_recognition> -- "MFCC can efficiently be used to characterize speakers"


### Uncategorized

 * "[Question: Why is the splitting performed on the result from phonemize? · Issue #57 · rhasspy/piper · GitHub](https://github.com/rhasspy/piper/issues/57)"
