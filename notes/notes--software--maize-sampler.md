# Notes: Software: Maize Sampler

_See also:_ ["A-maize-ing Player" proof-of-concept "player" of `.mse` format instruments](https://gitlab.com/RancidBacon/a-maize-ing-player/)

Maize Sampler is a closed source audio sample player/editor available
for Windows & Mac.


## File format(s)

There are two (data) file formats associated with Maize:

 * `.mse` -- exported, distributed form of sample instruments
             (includes embedded audio sample data).

 * `.ms2` -- file format used by the editor application which doesn't
             embed the audio data and instead uses file paths to
             external audio `.wav` files. (This format is (very?)
             occasionally used for distribution also.)

As far as I'm aware neither of these file formats are
publicly/officially documented.

In addition to these data files, a platform-specific "generic"
plugin/library is required to actually use the instrument files within
e.g. a DAW.


### Reverse engineering

#### The `.mse` file format

Motivated in part by wanting to use free `.mse` instruments on Linux
(which is not supported by any official "player" plugin) (and an
apparent compulsion to research undocumented file formats :D ) I've
performed some initial research into understanding/documenting/reading
the `.mse` format.

As of Q3 2024 this has been sufficient to extract:

 * Instrument name & author.

 * Custom embedded PNG UI background images.

 * Custom XML UI description (and partially understand its content).

 * Manually extract audio data with enough metadata to "somewhat play"
   it back correctly--for a drum sample instrument.

I've been using ImHex and its `.hexpat` pattern language to
investigate/define/document the file format's structure.

In addition I've been implementing a standalone "player" app with
Godot/GDScript which so far can display the custom UI background
images and indicate the location of various UI widgets/elements but
doesn't yet load/play the "raw" audio (nor use the custom images
associated with certain UI widgets; nor apply associated UI
functionality).


##### "Custom Image" format embedded in XML UI description

The primary remaining unknown "visual"-related aspect of `.mse` files
is the method by which the binary "custom image"s (used for knobs &
buttons) are encoded/stored (as text strings) in the XML UI
description.

 * `IBkSG0fBZn`

_The following is (most of) the text of the commit message related to
the immediately above notes which provides additional details not yet
added to this document, so I'm copying them here for "visibility":_

```
This commit is the current state of my incomplete/in-progress
notes about the way in which the binary "custom image"s
(used for knobs & buttons) are encoded/stored (as text strings)
in the XML UI description.

It includes the text "prefix" fragment `IBkSG0fBZn` which appears in the
encoded form of all(?) the stored images.

An internet/source code search for this text fragment reveals other files
that contain this string and that contain encoding which seems very
similar/identical to that used within the `.mse` files.

Most commonly the fragment seems to appear in `.maxpat` files but my
research into the `.maxpat` format seems to suggest that the content is
encoded/written by a function contained within a closed source proprietary
DLL used by Max.

While the binary format seems to be encoded with base64(?) the
`.` characters that seem to be used as some sort of separators(?) mean
the string cannot be directed decoded.

And the binary data encoded seems not to be anything as straight forward
as the entire original PNG file itself.

I include/mention the text fragment here so that at least that breadcrumb
of information will no longer only exist in my brain/local repo.

A bunch of (possibly) related URLs I found exist as local bookmarks but
I'm yet to add them to my notes.
```

_Quick link dump of some of current search results for the
aforementioned text fragment (some of which are probably in my related
unsorted local bookmarks):_

 * <https://github.com/flucoma/flucoma-max/blob/main/extras/Fluid%20Corpus%20Manipulation%20Toolkit.maxpat>
   [["permalink"](https://github.com/flucoma/flucoma-max/blob/b11251e0fe7737858486698911da9ae2578cb892/extras/Fluid%20Corpus%20Manipulation%20Toolkit.maxpat)]

 * <https://github.com/CNMAT/CNMAT-odot/blob/master/help/o.compose.maxhelp>
   [["permalink"](https://github.com/CNMAT/CNMAT-odot/blob/ac753e280dcdc44b0e1aace9bc73714a025f0f35/help/o.compose.maxhelp)]
   [[raw](https://raw.githubusercontent.com/CNMAT/CNMAT-odot/refs/heads/master/help/o.compose.maxhelp)]

 * "[Re: Mapping ID for the Reloop Beatpad ?](https://forum.mixvibes.com/viewtopic.php?p=207671&sid=0775a1ce4c203b08fd9bdac5b11495ef#p207671)"

   Note: The related text data (& file) is truncated but apparently
         the file is an XML MIDI mapping definition file for an
         Android app named "Cross DJ" (a.k.a `com.mixvibes.crossdjapp`)
         and the format is apparently also used by the related desktop
         application version.

 * "[Xdj aero map [...] ](https://forum.mixvibes.com/viewtopic.php?p=211112#p211112)"

   An (untruncated) file is visible when the "Show" button next to the
   bolded "Changelog" text is pressed.

   There is a (no longer found) image to which the poster refers with
   the statement "Here a sketch of the project:".

   My current theory is that these other occurences may be from
   (standalone) applications built with/on Max in some capacity and
   thus have access to the same Max (a.k.a. MaxMSP?) utility function DLL.


#### The `.ms2` file format

While researching possible existing information about the `.mse` file
format I learned about the existence of the related `.ms2` file format
which at first glance appears to either have large similarities to
`.mse` and/or be identical content in places.

Even *better*, someone actually wrote & shared source code for a tool
that is intended to "auto-populate" a `.ms2` file from a folder of
systematically named audio sample files. And, as part of the source
code a comment documents a large portion of the structure/fields of a
`.ms2` file (much/most of which I hadn't got as far as working out for
the similar sections in `.mse` files).

Original forum thread where "Maize Maker" tool/script is shared:

 * "[Maize sample batch import script - can anyone transform it into a program?](https://www.kvraudio.com/forum/viewtopic.php?t=491378)"

Most recent Dropbox link to the associated `maize-maker.zip` archive file:

 * <https://www.dropbox.com/s/4e3irtur2aftf4d/maize-maker.zip?dl=0> ([via](https://www.kvraudio.com/forum/viewtopic.php?p=7803336#p7803336))

(Better to discover this *before* I unnecessarily duplicated all the
reverse engineering involved. :) )

Unfortunately the source code in question currently exists in a `.zip`
file on Dropbox, so the information in question is not very
discoverable.

In light of this I'm going to reproduce some of the relevant source
code below to "preserve" it for posterity & improve discoverability of
the information which doesn't appear to exist elsewhere and has been
of interest to enquirers in the past.

The source code extracts below are currently reproduced without
explicit permission of the author/copyright holder Marco Scherer/Beat
Magazine; so, hopefully, in the interests of preserving the knowledge
"in the now" (before I get rabbit-holed by something else), they will
forgive me for not explicitly asking first. :)

(I may eventually "mirror" the entire source code and/or `.zip`
archive in a separate repository for preservation but that's not going
to happen today, for related reasons.)


##### Partial `Maize Maker 0.5` source code

From the file `maize-maker/index.php`:

```
<h1>Maize Maker 0.5</h1>
<span class="copyright">(c) 2017 by Marco Scherer for Beat Magazine</span>
```

...

```
        /*
                MAIZE FORMAT
                ------------
                [9 bytes static, do not change]
                Instrument name
                00
                Vendor name
                00
                [597 bytes, then GUI XML starts]
                [GUI ends with </player> tag, followed by 7 bytes, then first groups starts]

                Number of groups (XX)

                Group name
                00

                Selection (4 bytes) signed int little
                MIDI Channel (4 bytes) signed int little
                Key switch (4 bytes) signed int little
                CC (4 bytes) signed int little
                CC Range min (4 bytes) signed int little
                CC Range max (4 bytes) signed int little
                Program (4 bytes) signed int little
                Overlap (4 bytes) signed int little
                Attack (Float little)
                Decay (Float little)
                Sustain (Float little)
                Release (Float little)
                Key range start (4 bytes) signed int little
                Key range end (4 bytes) signed int little
                Velo max (4 bytes) signed int little
                Velo min (4 bytes) signed int little
                Release Trigger (0/1) (4 bytes) signed int little
                RT Decay (Float little)
                Gain (4 bytes) signed int little
                Disabled (4 bytes) signed int little
                Output (4 bytes) signed int little
                Pan (4 bytes) signed int little
                Selected (4 bytes) signed int little
                [488 yet unknown bytes]

                Sample file name, relative path to .ms2 file
                00
                Root note (XX000000) signed int little
                Start note (XX000000) signed int little
                End note (XX000000) signed int little
                Velocity max (XX000000) signed int little
                Velocity min (XX000000) signed int little
                Loop mode (XX000000) signed int little
                Sample start (XXXX0000 00000000) signed int little
                Sample end (XXXX0000 00000000) signed int little
                Loop start (XXXX0000 00000000) signed int little
                Loop end (XXXX0000 00000000) signed int little
                Gain (XXXXXXXX) float little [FC FF BF BF = -1.500000]
                Pan (XXXXXXXX) float little [EE 7C 7F BF = -0.998000]
                Attack (XXXXXXXX) float little [00 00 D4 44 = 1696.0000]
                Decay (XXXXXXXX) float little
                Sustain (XXXXXXXX) float little
                Release (XXXXXXXX) float little
                Tune (XXXXXXXX) float little [00 00 10 41 = 9.000000]
                Static Pitch (XX000000) signed int little
                Static Vel. (XX000000) signed int little
                Choke Cluster (XX000000) signed int little
                512 bytes (empty?)
        */

```

There may also be additional information in inline comments in the
rest of the source code and/or that can be gleaned from specific file
reading/writing functions but that will require further work to
extract the relevant content.
