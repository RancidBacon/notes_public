# Notes: Software : Firefox

## Widevine related

_(See also: <https://gitlab.com/RancidBacon/floss-various-contribs/-/blob/main/firefox/_issues.md#widevine-related>)_

Apparently individual versions of the "Widevine Content Decryption
Module" addon/plugin available from Google (required for "DRM"
"protected" playback of streaming media such as video) can "expire"
and cease to be accepted by streaming services.

This fact is not well communicated (or, really, communicated at all)
and the primary visible symptom is that (after playing streaming media
flawlessly over a period of months to years) suddenly such media
ceases to play--likely with little more than an uninformative "Oops
something went wrong." message on the associated streaming media web
site.

(I encountered this issue most recently in early November 2024.)

Google seemingly doesn't go out of their way to provide much
information about the "Widevine Content Decryption Module" to people
not considered "partners with a license agreement"--seemingly not even
information about the latest known version or when particular versions
are known to expire!

(*Some* might point out that this is yet another example where
"legitimate" consumers of streaming media face barriers that _\*cough\*_
others _\*cough\*_ don't--but I couldn't possibly comment.)


### Symptoms of "Widevine Content Decryption Module" addon expiry

The streaming site on which I encountered this issue suddenly
"overnight" would unhelpfully display the following message after a
second or two of seeming to buffer/load/play a streaming video:

> Oops! Something went wrong. Please refresh the page and try again.

After looking at the browser Network Monitor logs I notice a request
around this time that failed with the HTTP Status Code `406 Not Acceptable`.

(Outrageous!)

The URL of the (XHR) POST request that generated this error was of the
form:

```
https://manifest.prod.boltdns.net/license/v1/cenc/widevine/[...snip...]
```

Subdomains of `boltdns.net` are listed as required by the "Brightcove
Player" here: <https://studio.support.brightcove.com/general/architecture/domains-and-ports-must-be-accessible-video-cloud.html>

The data for the POST request is a "partially" binary blob that includes
readable ASCII text such as `ChromeCDM` and `widevine_cdm_version`
(apparently `4.10.2710.0` in this case).

The JSON response data returned was:

```json
{"error":"retrieving license: 406 "}
```

(And, yes, apparently the trailing space character is actually in the
response. :D )

The Developer Console shows a cascade of errors occurring after the
failed request, the first being:

```
VIDEOJS: ERROR: (CODE:5 MEDIA_ERR_ENCRYPTED) The media is encrypted and we do not have the keys to decrypt it.
```

Web searches for some of the related text didn't reveal much in the
way of helpful results. (Maybe this document will help with that in
future? :) )


### "Widevine Content Decryption Module" version information

#### via Google

It turns out that a list of "Widevine Content Decryption Module"
version numbers can be found at the following URL:

```
https://dl.google.com/widevine-cdm/versions.txt
```

(The above URL was originally found via
<https://chromium.woolyss.com/#notes>, thanks! :) )

The most recent/current version number seems to be the number listed
on the last line of the file (`4.10.2830.0` at the time of writing in
early November 2024).

A web search for this URL reveals some references to this URL and
scripts etc (untested/unverified, listed here as a courtesy :) ) for
"automatically" downloading the latest version of the module based on
the information contained in the file, e.g.:

 * <https://github.com/proprietary/chromium-widevine/blob/fcaae76eeca9d5d900bd1a5b9ed2911555a6bf61/fetch-latest-widevine.sh>

>
> Note:
>
> There are also some items that mention/use the URL
> `https://dl.google.com/widevine-cdm/current.txt` however this
> appears to *not* be an up-to-date version (contrary to what the file
> name might suggest :p ).
>
> The single version number listed in `current.txt` is *actually* the
> *oldest* version number listed in `versions.txt`.
>
> The following items may use/mention the outdated `current.txt` file
> but are listed here in case they still contain other useful
> information:
>
>  * "[Widevine Content Decryption Module for Chromium](https://stackoverflow.com/questions/39514901/widevine-content-decryption-module-for-chromium#71400814)"
>
>  * "[Improve Widevine installation for Windows and fix FAQ](https://github.com/ungoogled-software/ungoogled-chromium/issues/1439)"
>
>  * [`federicotorrielli/widevine-updater.sh`](https://gist.github.com/federicotorrielli/0f8e222b00a46387b8ffad43c43675cb)
>


### Options for testing Widevine operation without streaming service login

#### Option One

This commercial site (not an endorsement) appears to provide a way
(semi-untested) to verify the current state of Widevine support in
your browser without need for a streaming service login:

 * <https://bitmovin.com/demos/drm>
   (via <https://github.com/proprietary/chromium-widevine/issues/17#issuecomment-2294829962>)

Amusingly, the above page _also_ seems not to entirely handle the
situation of an "expired" "Widevine Content Decryption Module" version
but at least gives a _slightly_ more informative error message:

> ```
> License request failed
> (DRM_FAILED_LICENSE_REQUEST)
> ```

Firefox's Network Monitor indicates the page's license POST request
(with URL `https://cwip-shaka-proxy.appspot.com/no_auth` and
associated semi-binary blob data) actually fails with an HTTP status
of `500 Internal Server Error` and a plain text `ACCESS_DENIED`
response. :D


#### Option Two

After looking at the URL of license request used by the site in Option
One, I realised it actually seems to use the same "license server"[^license-server]
used by the *initial* Widevine test site I tried:

 * <https://shaka-player-demo.appspot.com>

[^license-server]: Side note: Comment with some background on Widevine license servers & proxies: <https://github.com/shaka-project/shaka-player/issues/7356#issuecomment-2372319669>

In order to verify Widevine operation you will need to click the "All Content" "tab" and select "Widevine" from the "DRM" selector and then click "play" on one of the media options listed, this link may also work:

 * [Shaka Player Demo content filtered by Widevine DRM](https://shaka-player-demo.appspot.com/demo/#audiolang=en-US;textlang=en-US;uilang=en-US;panel=ALL_CONTENT;panelData=drm:WIDEVINE;build=uncompiled)

Alternatively, this (horrendous!) URL should be a direct link to an
individual video which may also "work":

 * [Shaka Player Demo content labeled "Angel One (multicodec, multilingual, Widevine)"](https://shaka-player-demo.appspot.com/demo/#audiolang=en-US;textlang=en-US;uilang=en-US;assetBase64=eyJuYW1lIjoiQW5nZWwgT25lIChtdWx0aWNvZGVjLCBtdWx0aWxpbmd1YWwsIFdpZGV2aW5lKSIsInNob3J0TmFtZSI6IiIsImljb25VcmkiOiJodHRwczovL3N0b3JhZ2UuZ29vZ2xlYXBpcy5jb20vc2hha2EtYXNzZXQtaWNvbnMvYW5nZWxfb25lLnBuZyIsIm1hbmlmZXN0VXJpIjoiaHR0cHM6Ly9zdG9yYWdlLmdvb2dsZWFwaXMuY29tL3NoYWthLWRlbW8tYXNzZXRzL2FuZ2VsLW9uZS13aWRldmluZS9kYXNoLm1wZCIsInNvdXJjZSI6IlNoYWthIiwiZm9jdXMiOmZhbHNlLCJkaXNhYmxlZCI6ZmFsc2UsImV4dHJhVGV4dCI6W10sImV4dHJhVGh1bWJuYWlsIjpbXSwiZXh0cmFDaGFwdGVyIjpbXSwiY2VydGlmaWNhdGVVcmkiOm51bGwsImRlc2NyaXB0aW9uIjpudWxsLCJpc0ZlYXR1cmVkIjpmYWxzZSwiZHJtIjpbIldpZGV2aW5lIERSTSJdLCJmZWF0dXJlcyI6WyJEQVNIIiwiRG93bmxvYWRhYmxlIiwiTVA0IiwiTXVsdGlwbGUgbGFuZ3VhZ2VzIiwiU3VidGl0bGVzIiwiVk9EIiwiV2ViTSJdLCJsaWNlbnNlU2VydmVycyI6eyJfX3R5cGVfXyI6Im1hcCIsImNvbS53aWRldmluZS5hbHBoYSI6Imh0dHBzOi8vY3dpcC1zaGFrYS1wcm94eS5hcHBzcG90LmNvbS9ub19hdXRoIn0sImxpY2Vuc2VSZXF1ZXN0SGVhZGVycyI6eyJfX3R5cGVfXyI6Im1hcCJ9LCJyZXF1ZXN0RmlsdGVyIjpudWxsLCJyZXNwb25zZUZpbHRlciI6bnVsbCwiY2xlYXJLZXlzIjp7Il9fdHlwZV9fIjoibWFwIn0sImV4dHJhQ29uZmlnIjpudWxsLCJleHRyYVVpQ29uZmlnIjpudWxsLCJhZFRhZ1VyaSI6bnVsbCwiaW1hVmlkZW9JZCI6bnVsbCwiaW1hQXNzZXRLZXkiOm51bGwsImltYUNvbnRlbnRTcmNJZCI6bnVsbCwiaW1hTWFuaWZlc3RUeXBlIjpudWxsLCJtZWRpYVRhaWxvclVybCI6bnVsbCwibWVkaWFUYWlsb3JBZHNQYXJhbXMiOm51bGwsInVzZUlNQSI6dHJ1ZSwibWltZVR5cGUiOm51bGx9;panel=ALL_CONTENT;panelData=drm:WIDEVINE;build=uncompiled)

Even *more* amusingly, while this site *does* report a
`DRM.LICENSE_REQUEST_FAILED` error[^player-error] in a "notification" overlay (and
also reguritates what appears to be a full unformatted stack trace in
a JSON blob into the "notification")... it actually seems to *play*
the media in spite of the error! :D

[^player-error]: See [potentially related Shaka Player source](https://github.com/shaka-project/shaka-player/blob/a01b5fd449f5494beaba9c4b62efb5749be306c6/lib/media/drm_engine.js#L1436-L1471).

(_Update:_ After further investigation of the associated `.m3u8`
playlist file[^asset-info], it seems they made the "interesting" choice of _not
encoding the first two segments of the video_. Thus it *seems* to work
with the failed license request for the first ~7 seconds of video and
*then* fails/stops playback! Not entirely helpful for diagnostic
purposes--who has an attention span of more than seven seconds when
troubleshooting these kind of things?! :p )

[^asset-info]: See ["Angel One (multicodec, multilingual, Widevine)" asset info](https://github.com/shaka-project/shaka-player/blob/be0e10c0840000b2c54746312897999018272441/demo/common/assets.js#L313-L325) & ["Angel One (HLS, MP4, multilingual, Widevine)" asset info](https://github.com/shaka-project/shaka-player/blob/be0e10c0840000b2c54746312897999018272441/demo/common/assets.js#L358-L370).

Side note: *Very* non-obviously, the gear/cog icon in the top left of
the page (in the page "header") actually opens a slide-out "Shaka
Player Demo Config" panel that enables configuring a bunch of settings
including log level and various DRM-related settings. Seems this
configuration setup can also be passed along in a link, e.g.:

 * [Shaka Player Demo content filtered by Widevine DRM (with additional config & logging enabled)](https://shaka-player-demo.appspot.com/demo/#audiolang=en-US;textlang=en-US;delayLicenseRequestUntilPlayed=true;logLicenseExchange=true;uilang=en-US;panel=ALL_CONTENT;panelData=drm:WIDEVINE;build=uncompiled;vv)


### Option Three

Until its deprecation[^deprecation-archive] is complete: <https://integration.widevine.com/player>

Which uses: <https://proxy.staging.widevine.com/proxy>

And the error displayed (HTTP Status `400 Bad Request` with `category:
6` & `code: 6007`) includes:

```json
{
"detail":"status=ACCESS_DENIED,internal_status=DRM_DEVICE_CERTIFICATE_REVOKED"
}
```

So, you know, that's informative! Why doesn't everyone else handle it
too then..?

(Oh, and, yet again the first ~10 seconds plays correctly even with
the license error...)

[^deprecation-archive]: See also: <https://web.archive.org/web/*/https://integration.widevine.com*>



### Potential related project issues

This section is primarily a "dumping ground" for links to issue
tracker items that are in at least some way related to the topic of
Widevine CDM expiry/revocation and/or failed license requests in case
they're relevant in future (i.e. this enables me to close tabs :D ):

 * Shaka Player

    * "[Handling revoked widevine clients](https://github.com/shaka-project/shaka-player/issues/4100)"

      It seems the specific error text mentioned _"DRM client models
      with revoked certificates are not allowed to receive licenses"_
      is generated by a license server from "Axinom Mosaic" who has an
      entry in their FAQ with some high level background on Google's
      Widevine CDM revocation process:

       * <https://docs.axinom.com/services/drm/faq#how-to-fix-invalid-license-request--drm-client-models-with-revoked-certificates-are-not-allowed-to-receive-licenses-error-message>

      It turns out that there *are* Shaka Player demos[^shaka-axinom-demo]
      that originate from Axinom[^axinom-asset], so it's actually possible to generate
      the error (which still isn't *handled* by Shaka Player) but
      enables us to see what the Axinom license server returns (which
      seems, actually, wow, almost informative!):

       * POST request URL: `https://drm-widevine-licensing.axprod.net/AcquireLicense`

       * HTTP status response: `400 Bad Request`

       * Response Headers (partial list):

         ```
         x-axdrm-errormessage: Invalid license request. DRM client models with revoked certificates are not allowed to receive licenses.
         x-axdrm-identity: Axinom DRM - Widevine API
         x-axdrm-server: widevine-production-abcdefgabc-abcde
         x-axdrm-version: 6.26.0
         ```

         So, clearly Widevine license servers *can* return useful
         feedback on license failures but most just... *don't*.

         The Shaka Player error response seems to include the returned headers accessible via, e.g.:

         ```javascript
         //
         // Use "Store as Global Variable" context menu option on
         // console log error output object to create `temp0`.
         //
         temp0.data[0].data[3]
         temp0.data[0].data[3]["x-axdrm-errormessage"] // --> "Invalid license request. DRM client models with revoked certificates are not allowed to receive licenses."
         ```

       * Response body: Empty

      Additionally, it seems that Axinom hosts some other related tools
      that might be useful for troubleshooting:

       * "[HTML5 VideoTestBench (Shaka Player)](https://vtb.axinom.com/shaka.html)"

       * "[Browser DRM info](https://vtb.axinom.com/drminfo.html)"

       * "[HTML5 VideoTestBench (Axinom DRM Test Player)](https://vtb.axinom.com/index.html)"

       * "[DRM Video Playback - Mosaic Tools | Axinom Portal](https://portal.axinom.com/mosaic/tools/drm-video-playback)"

       * "[Widevine Common Encryption (Key Service) - Mosaic Tools | Axinom Portal](https://portal.axinom.com/mosaic/tools/widevine-common-encryption)"

       * "[Test Vectors for DRM (multi-DRM / single-key, multi-audio) - Mosaic Tools | Axinom Portal](https://portal.axinom.com/mosaic/tools/test-vectors)"

         Hosted here: <https://github.com/Axinom/public-test-vectors>

      See also:

       * "[License Request Decoder returns outdated info](https://github.com/Axinom/media-tools/issues/12)"

       * "[License Request Decoder for Widevine is broken](https://github.com/Axinom/media-tools/issues/9)"

         Includes some "examples" and also a mention of <https://integration.widevine.com/diagnostics>[^deprecation] (!).

       * <https://docs.axinom.com/general/portal/error-logs-tool/#viewing-the-logs>

         (Mentions "Invalid license request. DRM client models with
         revoked certificates are not allowed to receive licenses." as
         example logged error.)

       * via <https://shaka-project.github.io/shaka-packager/html/documentation.html#general-encryption-options>:

         >
         > `--clear_lead <seconds>`
         >
         > _Clear lead in seconds if encryption is enabled. Shaka
         > Packager does not support partial encrypted segments, all
         > the segments including the partial segment overlapping with
         > the initial ‘clear_lead’ seconds are not encrypted, with
         > all the following segments encrypted. If segment_duration
         > is greater than ‘clear_lead’, then only the first segment
         > is not encrypted. Default: 5_
         >

         So, I guess that explains the initial unencrypted parts?

      [^deprecation]: Which, in a surprise to no one, mentions "The Integration Console interface will be deprecated starting January 15th, 2025."[^deprecation-2] :D

      [^deprecation-2]: And, also, in a surprise to no one, the "replacement" at <https://partnerdash.google.com/apps/widevineintegrationconsole> now requires log in.

      [^axinom-asset]: See: [Axinom hosted "Multi-DRM (CBCS), multi-key" demo asset info](https://github.com/shaka-project/shaka-player/blob/be0e10c0840000b2c54746312897999018272441/demo/common/assets.js#L794-L823).

      [^shaka-axinom-demo]: You "simply" have to use this *even more* horrendous URL: [Shaka Player Demo "Multi-DRM (CBCS), multi-key" hosted by Axinom](https://shaka-player-demo.appspot.com/demo/#audiolang=en-US;textlang=en-US;delayLicenseRequestUntilPlayed=true;logLicenseExchange=true;uilang=en-US;assetBase64=eyJuYW1lIjoiTXVsdGktRFJNIChDQkNTKSwgbXVsdGkta2V5Iiwic2hvcnROYW1lIjoiIiwiaWNvblVyaSI6Imh0dHBzOi8vc3RvcmFnZS5nb29nbGVhcGlzLmNvbS9zaGFrYS1hc3NldC1pY29ucy90ZWFyc19vZl9zdGVlbC5wbmciLCJtYW5pZmVzdFVyaSI6Imh0dHBzOi8vbWVkaWEuYXhwcm9kLm5ldC9UZXN0VmVjdG9ycy9NdWx0aUtleS9DbWFmX2gyNjRfMTA4MHBfY2Jjcy9tYW5pZmVzdC5tcGQiLCJzb3VyY2UiOiJBeGlub20iLCJmb2N1cyI6ZmFsc2UsImRpc2FibGVkIjpmYWxzZSwiZXh0cmFUZXh0IjpbXSwiZXh0cmFUaHVtYm5haWwiOltdLCJleHRyYUNoYXB0ZXIiOltdLCJjZXJ0aWZpY2F0ZVVyaSI6bnVsbCwiZGVzY3JpcHRpb24iOm51bGwsImlzRmVhdHVyZWQiOmZhbHNlLCJkcm0iOlsiUGxheVJlYWR5IERSTSIsIldpZGV2aW5lIERSTSJdLCJmZWF0dXJlcyI6WyJEQVNIIiwiSGlnaCBkZWZpbml0aW9uIiwiTVA0IiwiTXVsdGlwbGUgbGFuZ3VhZ2VzIiwiU3VidGl0bGVzIiwiVk9EIl0sImxpY2Vuc2VTZXJ2ZXJzIjp7Il9fdHlwZV9fIjoibWFwIiwiY29tLndpZGV2aW5lLmFscGhhIjoiaHR0cHM6Ly9kcm0td2lkZXZpbmUtbGljZW5zaW5nLmF4cHJvZC5uZXQvQWNxdWlyZUxpY2Vuc2UiLCJjb20ubWljcm9zb2Z0LnBsYXlyZWFkeSI6Imh0dHBzOi8vZHJtLXBsYXlyZWFkeS1saWNlbnNpbmcuYXh0ZXN0Lm5ldC9BY3F1aXJlTGljZW5zZSJ9LCJsaWNlbnNlUmVxdWVzdEhlYWRlcnMiOnsiX190eXBlX18iOiJtYXAifSwicmVxdWVzdEZpbHRlciI6bnVsbCwicmVzcG9uc2VGaWx0ZXIiOm51bGwsImNsZWFyS2V5cyI6eyJfX3R5cGVfXyI6Im1hcCJ9LCJleHRyYUNvbmZpZyI6eyJkcm0iOnsiYWR2YW5jZWQiOnsiY29tLndpZGV2aW5lLmFscGhhIjp7ImhlYWRlcnMiOnsiWC1BeERSTS1NZXNzYWdlIjoiZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV3b2dJQ0oyWlhKemFXOXVJam9nTVN3S0lDQWlZMjl0WDJ0bGVWOXBaQ0k2SUNJMk9XVTFOREE0T0MxbE9XVXdMVFExTXpBdE9HTXhZUzB4WldJMlpHTmtNR1F4TkdVaUxBb2dJQ0p0WlhOellXZGxJam9nZXdvZ0lDQWdJblI1Y0dVaU9pQWlaVzUwYVhSc1pXMWxiblJmYldWemMyRm5aU0lzQ2lBZ0lDQWlkbVZ5YzJsdmJpSTZJRElzQ2lBZ0lDQWliR2xqWlc1elpTSTZJSHNLSUNBZ0lDQWdJbUZzYkc5M1gzQmxjbk5wYzNSbGJtTmxJam9nZEhKMVpRb2dJQ0FnZlN3S0lDQWdJQ0pqYjI1MFpXNTBYMnRsZVhOZmMyOTFjbU5sSWpvZ2V3b2dJQ0FnSUNBaWFXNXNhVzVsSWpvZ1d3b2dJQ0FnSUNBZ0lIc0tJQ0FnSUNBZ0lDQWdJQ0pwWkNJNklDSmlOVFJsWXpreE5DMHhPVEprTFRSbFlURXRZV014T1MxbU5ESTVaV0kwT1RneU5qZ2lMQW9nSUNBZ0lDQWdJQ0FnSW1WdVkzSjVjSFJsWkY5clpYa2lPaUFpUjFaRVJuSlpVVTlCYjFrelptcHhWVlZ0YW1zd1FUMDlJaXdLSUNBZ0lDQWdJQ0FnSUNKMWMyRm5aVjl3YjJ4cFkza2lPaUFpVUc5c2FXTjVJRUVpQ2lBZ0lDQWdJQ0FnZlN3S0lDQWdJQ0FnSUNCN0NpQWdJQ0FnSUNBZ0lDQWlhV1FpT2lBaVl6Z3pZelJsWVRndE1HWXlZUzAwTlRJekxUZzFNV010Wm1KbFkyTmtZekJtTWpBeUlpd0tJQ0FnSUNBZ0lDQWdJQ0psYm1OeWVYQjBaV1JmYTJWNUlqb2dJbFJLWkdac1dtSkxZbVpYUVhsNUsxZHRhMjFVVUVFOVBTSXNDaUFnSUNBZ0lDQWdJQ0FpZFhOaFoyVmZjRzlzYVdONUlqb2dJbEJ2YkdsamVTQkJJZ29nSUNBZ0lDQWdJSDBzQ2lBZ0lDQWdJQ0FnZXdvZ0lDQWdJQ0FnSUNBZ0ltbGtJam9nSW1NNE5qaGpOekF5TFdNM01XSXROREEyTkMxaFpUSmlMV015TkdZM1kyTXhNRGM1TWlJc0NpQWdJQ0FnSUNBZ0lDQWlaVzVqY25sd2RHVmtYMnRsZVNJNklDSjRRWEpwVWtwT2NVRlRkWHA2UkV4RFJ6TlhTamRuUFQwaUxBb2dJQ0FnSUNBZ0lDQWdJblZ6WVdkbFgzQnZiR2xqZVNJNklDSlFiMnhwWTNrZ1FTSUtJQ0FnSUNBZ0lDQjlDaUFnSUNBZ0lGMEtJQ0FnSUgwc0NpQWdJQ0FpWTI5dWRHVnVkRjlyWlhsZmRYTmhaMlZmY0c5c2FXTnBaWE1pT2lCYkNpQWdJQ0FnSUhzS0lDQWdJQ0FnSUNBaWJtRnRaU0k2SUNKUWIyeHBZM2tnUVNJc0NpQWdJQ0FnSUNBZ0luQnNZWGx5WldGa2VTSTZJSHNLSUNBZ0lDQWdJQ0FnSUNKdGFXNWZaR1YyYVdObFgzTmxZM1Z5YVhSNVgyeGxkbVZzSWpvZ01UVXdMQW9nSUNBZ0lDQWdJQ0FnSW5Cc1lYbGZaVzVoWW14bGNuTWlPaUJiQ2lBZ0lDQWdJQ0FnSUNBZ0lDSTNPRFkyTWpkRU9DMURNa0UyTFRRMFFrVXRPRVk0T0Mwd09FRkZNalUxUWpBeFFUY2lDaUFnSUNBZ0lDQWdJQ0JkQ2lBZ0lDQWdJQ0FnZlFvZ0lDQWdJQ0I5Q2lBZ0lDQmRDaUFnZlFwOS5YQzBZSWJacEtHRmMzSVpST2tsUDRMdklTYzZjWkdwRTlVTC1YY3BjcVdnIn19LCJjb20ubWljcm9zb2Z0LnBsYXlyZWFkeSI6eyJoZWFkZXJzIjp7IlgtQXhEUk0tTWVzc2FnZSI6ImV5SmhiR2NpT2lKSVV6STFOaUlzSW5SNWNDSTZJa3BYVkNKOS5ld29nSUNKMlpYSnphVzl1SWpvZ01Td0tJQ0FpWTI5dFgydGxlVjlwWkNJNklDSTJPV1UxTkRBNE9DMWxPV1V3TFRRMU16QXRPR014WVMweFpXSTJaR05rTUdReE5HVWlMQW9nSUNKdFpYTnpZV2RsSWpvZ2V3b2dJQ0FnSW5SNWNHVWlPaUFpWlc1MGFYUnNaVzFsYm5SZmJXVnpjMkZuWlNJc0NpQWdJQ0FpZG1WeWMybHZiaUk2SURJc0NpQWdJQ0FpYkdsalpXNXpaU0k2SUhzS0lDQWdJQ0FnSW1Gc2JHOTNYM0JsY25OcGMzUmxibU5sSWpvZ2RISjFaUW9nSUNBZ2ZTd0tJQ0FnSUNKamIyNTBaVzUwWDJ0bGVYTmZjMjkxY21ObElqb2dld29nSUNBZ0lDQWlhVzVzYVc1bElqb2dXd29nSUNBZ0lDQWdJSHNLSUNBZ0lDQWdJQ0FnSUNKcFpDSTZJQ0ppTlRSbFl6a3hOQzB4T1RKa0xUUmxZVEV0WVdNeE9TMW1OREk1WldJME9UZ3lOamdpTEFvZ0lDQWdJQ0FnSUNBZ0ltVnVZM0o1Y0hSbFpGOXJaWGtpT2lBaVIxWkVSbkpaVVU5QmIxa3pabXB4VlZWdGFtc3dRVDA5SWl3S0lDQWdJQ0FnSUNBZ0lDSjFjMkZuWlY5d2IyeHBZM2tpT2lBaVVHOXNhV041SUVFaUNpQWdJQ0FnSUNBZ2ZTd0tJQ0FnSUNBZ0lDQjdDaUFnSUNBZ0lDQWdJQ0FpYVdRaU9pQWlZemd6WXpSbFlUZ3RNR1l5WVMwME5USXpMVGcxTVdNdFptSmxZMk5rWXpCbU1qQXlJaXdLSUNBZ0lDQWdJQ0FnSUNKbGJtTnllWEIwWldSZmEyVjVJam9nSWxSS1pHWnNXbUpMWW1aWFFYbDVLMWR0YTIxVVVFRTlQU0lzQ2lBZ0lDQWdJQ0FnSUNBaWRYTmhaMlZmY0c5c2FXTjVJam9nSWxCdmJHbGplU0JCSWdvZ0lDQWdJQ0FnSUgwc0NpQWdJQ0FnSUNBZ2V3b2dJQ0FnSUNBZ0lDQWdJbWxrSWpvZ0ltTTROamhqTnpBeUxXTTNNV0l0TkRBMk5DMWhaVEppTFdNeU5HWTNZMk14TURjNU1pSXNDaUFnSUNBZ0lDQWdJQ0FpWlc1amNubHdkR1ZrWDJ0bGVTSTZJQ0o0UVhKcFVrcE9jVUZUZFhwNlJFeERSek5YU2pkblBUMGlMQW9nSUNBZ0lDQWdJQ0FnSW5WellXZGxYM0J2YkdsamVTSTZJQ0pRYjJ4cFkza2dRU0lLSUNBZ0lDQWdJQ0I5Q2lBZ0lDQWdJRjBLSUNBZ0lIMHNDaUFnSUNBaVkyOXVkR1Z1ZEY5clpYbGZkWE5oWjJWZmNHOXNhV05wWlhNaU9pQmJDaUFnSUNBZ0lIc0tJQ0FnSUNBZ0lDQWlibUZ0WlNJNklDSlFiMnhwWTNrZ1FTSXNDaUFnSUNBZ0lDQWdJbkJzWVhseVpXRmtlU0k2SUhzS0lDQWdJQ0FnSUNBZ0lDSnRhVzVmWkdWMmFXTmxYM05sWTNWeWFYUjVYMnhsZG1Wc0lqb2dNVFV3TEFvZ0lDQWdJQ0FnSUNBZ0luQnNZWGxmWlc1aFlteGxjbk1pT2lCYkNpQWdJQ0FnSUNBZ0lDQWdJQ0kzT0RZMk1qZEVPQzFETWtFMkxUUTBRa1V0T0VZNE9DMHdPRUZGTWpVMVFqQXhRVGNpQ2lBZ0lDQWdJQ0FnSUNCZENpQWdJQ0FnSUNBZ2ZRb2dJQ0FnSUNCOUNpQWdJQ0JkQ2lBZ2ZRcDkuWEMwWUliWnBLR0ZjM0laUk9rbFA0THZJU2M2Y1pHcEU5VUwtWGNwY3FXZyJ9fX0sInNlcnZlcnMiOnsiY29tLndpZGV2aW5lLmFscGhhIjoiaHR0cHM6Ly9kcm0td2lkZXZpbmUtbGljZW5zaW5nLmF4cHJvZC5uZXQvQWNxdWlyZUxpY2Vuc2UiLCJjb20ubWljcm9zb2Z0LnBsYXlyZWFkeSI6Imh0dHBzOi8vZHJtLXBsYXlyZWFkeS1saWNlbnNpbmcuYXh0ZXN0Lm5ldC9BY3F1aXJlTGljZW5zZSJ9fX0sImV4dHJhVWlDb25maWciOm51bGwsImFkVGFnVXJpIjpudWxsLCJpbWFWaWRlb0lkIjpudWxsLCJpbWFBc3NldEtleSI6bnVsbCwiaW1hQ29udGVudFNyY0lkIjpudWxsLCJpbWFNYW5pZmVzdFR5cGUiOm51bGwsIm1lZGlhVGFpbG9yVXJsIjpudWxsLCJtZWRpYVRhaWxvckFkc1BhcmFtcyI6bnVsbCwidXNlSU1BIjp0cnVlLCJtaW1lVHlwZSI6bnVsbH0=;panel=ALL_CONTENT;panelData=source:AXINOM,drm:WIDEVINE;build=uncompiled;vv) :O

    * "[Is there any way to detect CDM version installed in the browser?](https://github.com/shaka-project/shaka-player/issues/3461)"

      > """
      >
      > _We know CDM releases are rolled out to users automatically,
      > and Widevine, is blocking some releases that are vulnerable
      > after a grace period._
      >
      > _However, we would like to inform the user showing a popup
      > before launching the playback if the CDM is not updated._
      >
      > """

    * "[License Server Error Response from NetworkEngine()](https://github.com/shaka-project/shaka-player/issues/2864)"

    * "[Chrome Widevine CDM Deprecation](https://github.com/shaka-project/shaka-player/issues/1831)"

      > """
      >
      > _If I've understood correctly it sounds like the Widevine
      > server will return an error. So your license proxy should be
      > configured to understand that error and send an error response
      > back from the license request. Sending an error response back
      > will cause Shaka to publish 6007 LICENSE_REQUEST_FAILED. Your
      > client app should inspect this error to determine if the
      > failure is because the user's CDM is out of date._
      >
      > """

      -- <https://github.com/shaka-project/shaka-player/issues/1831#issuecomment-470234059>

    * "[Allow playback with LICENSE_REQUEST_FAILED](https://github.com/shaka-project/shaka-player/issues/3051)"

      Implemented by:

       * "[fix: Do not make LICENSE_REQUEST_FAILED fatal if other keys are successful](https://github.com/shaka-project/shaka-player/pull/6457)"


[TODO: Continue this..?]
