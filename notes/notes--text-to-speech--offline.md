## Notes: Offline Text-To-Speech for Creatives

_Information about use of & resources for FLOSS (Free/Libre/Open Source Software) offline (i.e. no network/cloud service required) Text-To-Speech generation (i.e. speech synthesis) for creative projects (e.g. voice over and/or character dialogue for video games, animations, films, radio dramas, extortion disguised as a revival of retro radio dramas, etc)._


### Synthesis Software

The software that takes the text you want to be spoken and the desired "voice" to speak with and does the work required to generate an audio file that contains the spoken text.

 * [Larynx](https://github.com/rhasspy/larynx)

 * [Piper](https://github.com/rhasspy/piper) (a.k.a. [Larynx 2](https://github.com/rhasspy/larynx2))

   _See also: [Notes: Piper TTS](notes--text-to-speech--piper-tts.md)_

 * [`MycroftAI/mimic3`](https://github.com/MycroftAI/mimic3): "A fast local neural text to speech engine for Mycroft"



### Voices

#### Voice Models

 * <https://github.com/MycroftAI/mimic3-voices>

#### Voice Source Data

 * [LibriSpeech ASR corpus (SLR12)](http://www.openslr.org/12/) -- CC-BY-4.0 (dataset) & PD (original recordings)

 * [LibriTTS corpus (SLR60)](http://www.openslr.org/60/) -- CC-BY-4.0 (dataset) & PD (original recordings)

#### Licensing (Voice Source Data & Voice Models)

##### Derivative Works & Generated Audio

#### Ethics

##### Consent

##### Bias



### Workflow Tools

 * [OpenTTS: Open Text to Speech Server](https://github.com/synesthesiam/opentts)  
   "Unifies access to multiple open source text to speech systems and voices"  
   (Supports SSML subset)

 * [Dialogue Tool for Larynx TTS](https://rancidbacon.itch.io/dialogue-tool-for-larynx-text-to-speech)

   Improves workflow for offline text to speech generation for narrative dialogue scripts with:

    * Easier "auditioning" of voices.

    * Easier iterative process of turning script into audio files (e.g. with file cache & "intelligent" re-generation control).

   (Developed by me!)



### Uncategorized links

 *



### Bugs/Issues

 * "poor performance on short phrases · Issue #31 · jaywalnut310/vits"  
   <https://github.com/jaywalnut310/vits/issues/31>
