# Notes: Software : ImHex

_(See also: [Notes: Reverse Engineering](notes--reverse-engineering.md))_


## Project links

 * Repo: <https://github.com/WerWolv/ImHex>

 * Patterns repo: <https://github.com/WerWolv/ImHex-Patterns>

 * Project site: <https://imhex.werwolv.net/>

 * App docs: <https://docs.werwolv.net/imhex>

 * Patterns docs: <https://docs.werwolv.net/pattern-language>


## Usage notes

### Documentation

 * While some of the project documentation is quite good, there does
   seem to be a couple of overarching issues with the docs:

    * Lack of updates.

    * Lack of coverage.



### Tips & Tricks

 * The `.hexproj` ImHex project file format is an uncompressed `.tar`
   file.

   Quick hack to dump the project file content (sans NUL bytes), for
   the curious:

   ```
   tr --squeeze-repeats '\0' '\n' < imhex-project.hexproj | less
   ```

   Primarily contains `.json` files.



### Issues/Bugs encountered

 - [ ] "Export Patterns as..." seems to work okay for JSON & YAML but
       generates a ~230MB HTML file for same input!

 - [ ] File Dialog appears behind app window.

       Related: <https://github.com/WerWolv/ImHex/pull/1771>

 - [ ] "Export Selection" doesn't finish unless manually stopped.

       Maybe related:

        * <https://github.com/WerWolv/ImHex/issues/1733>

        * <https://github.com/WerWolv/ImHex/issues/1619>

        * <https://github.com/WerWolv/ImHex/issues/1597>

 - [ ] Replaying (short) audio clip rapidly may lead to GUI/app hang.

       Maybe related:

        * <https://github.com/mackron/miniaudio>

        * <https://github.com/mackron/miniaudio/issues/760>

        * <https://github.com/mackron/miniaudio/issues/714>

        * <https://sourcegraph.com/github.com/WerWolv/ImHex@v1.34.0/-/blob/plugins/visualizers/source/content/pl_visualizers/sound.cpp>

 - [ ] Rust no longer supported for plugins?

       Maybe related:

        * <https://github.com/turbidsoul/imhex-flv/blob/master/lib/libimhex-rs/build.rs>

        * <https://github.com/WerWolv/ImHex/commit/b55c6fa3e1f325bdd3772d01c1cbc67ba9302740>

        * <https://github.com/clayne/ImHex-Rust-Plugin-Template>

        * <https://web.archive.org/web/20231128133357/https://github.com/WerWolv/ImHex-Rust-Plugin-Template> (But not files?)

        * <https://github.com/WerWolv/ImHex/discussions/681#discussioncomment-3408414>:

          > "The Rust template is much further behind because nobody
          >  ever put the work into making a nice Rust <-> C++
          >  interface for it so there's not a ton you can do in Rust
          >  anyways."

 * "[zlib_decompress to process raw deflate data (w/o zlib or gzip header). · Issue #1756 · WerWolv/ImHex](https://github.com/WerWolv/ImHex/issues/1756)"
