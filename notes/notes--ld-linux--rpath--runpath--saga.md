# Notes: A `ld-linux`, `RPATH` & `RUNPATH` saga.

## TL;DR: RTLD :D

 * If an executable has `RPATH` (a.k.a. `DT_RPATH`) set but a shared
   library that is a (direct or indirect(?)) dependency of that
   executable has `RUNPATH` (a.k.a. `DT_RUNPATH`) set then **the
   executable's `RPATH` is _ignored_**!

 * This means a shared library dependency can "force" loading of an
   incompatible dependency version in certain situations.

   Specifically, this was encountered when a shared library with a
   `RUNPATH` of (something like) `${ORIGIN}/../lib` depended on
   `libm.so.6` but the executable itself *didn't* depend on
   `libm.so.6`, so didn't list `libm.so.6` as `DT_NEEDED`.

   What seems to occur is that once the runtime dynamic linker/loader
   fails to find `libm.so.6` in the shared library's
   (misattributed[^exe-rpath-bug]) `RUNPATH`, it skips directly to
   searching the `ld.so` cache/default system search path.

   This means that if e.g. `libc.so.6` for the executable was found
   via its `RPATH` (and it is a newer version than the system's
   default version) then the required more recent versions of any of
   its closely "integrated"/associated shared libraries (e.g. `libm`
   et al) will not be found by shared library dependencies with their
   own `RUNPATH` set--and so the executable will fail to start due to
   a `version lookup error: version `GLIBC_X.XX' not found` error (or,
   potentially worse if a `GLIBC_PRIVATE` version symbol is involved).

   (AIUI this issue may occur with a dependency on *any* of the
   "standard libraries" that are included in a distro's `libc6`
   package[^so-file-list] (e.g. `libm`, `libpthread`, `librt` &
   others).)


[^exe-rpath-bug]:
    See later section about `LD_DEBUG` search path debug log bug.


[^so-file-list]:
    A list of such libraries can be determined via this or other
    means[^other-file-list] appropriate to your distribution:

    ```bash
    dpkg-query --listfiles libc6 | grep '/lib/x86_64-linux-gnu/[^/]*\.so$'
    ```

[^other-file-list]:
    Other files with information about potentially affected library
    dependencies (along with more details which might be useful in
    other shared library symbol-related troubleshooting) can be found
    with:

    ```bash
    $ ls -1 /var/lib/dpkg/info/libc6\:*@(.list|.shlibs|.symbols)
    /var/lib/dpkg/info/libc6:amd64.list
    /var/lib/dpkg/info/libc6:amd64.shlibs
    /var/lib/dpkg/info/libc6:amd64.symbols
    ```


 * There is seemingly a bug in `LD_DEBUG`-related functionality that
   means it incorrectly states that the source of the search path for
   a shared library dependency of a shared library (which is itself a
   dependency of the executable) is `(RPATH from file <executable>)`
   when the search path used is *actually* from the `RUNPATH` of the
   shared library!

   (This can most obviously be seen by grepping/filtering the debug
   output for the phrase `(RPATH from file <executable>)` and
   observing that the search path listed as being from that source is
   different at multiple points in the shared library loading
   process.)

   (This seemingly affects at least v2.27 to v2.40, the most recent at
   time of writing.)


## The nuances & consequences of `RPATH` vs `RUNPATH`

In the beginning was `RPATH` but apparently not all was good because
later `RUNPATH` came along and `RPATH` was... demoted(?) to "level 2"
of the ["System V Application Binary Interface"](<https://gitlab.com/x86-psABIs/x86-64-ABI/-/wikis/home>).

[TODO: Continue...]

----

## Other Related Notes

(Following specification documents reached via <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-AMD64/LSB-Core-AMD64/tocdynlnk.html> & <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-AMD64/LSB-Core-AMD64/normativerefs.html#STD.GABI41>.)


via "[System V Application Binary Interface, Edition 4.1](https://www.sco.com/developers/devspecs/gabi41.pdf)":

Note here `DT_RPATH` is listed as _ignored_ for shared objects:

> Figure 5-10: Dynamic Array Tags, `d_tag`
>
> ```
> | Name     | Value | `d_un` | Executable | Shared Object |
> [...]
> | DT_RPATH | 15    | d_val  | optional | ignored |
>
> ```


> " `DT_NEEDED` _[...] The dynamic array may contain multiple entries
> with this type.  These entries’ relative order is significant,
> though their relation to entries of other types is not._"


Note here the mention of the trailing `:` to also include the current
directory in the search path:

> """
>
> _If a shared object name has one or more slash (`/`) characters
> anywhere in the name, such as `/usr/lib/lib2` above or
> `directory/file`, the dynamic linker uses that string directly as
> the path name. If the name has no slashes, such as `lib1` above,
> three facilities specify shared object path searching, with the
> following precedence._
>
>  * _First, the dynamic array tag `DT_RPATH` may give a string that
>    holds a list of directories, separated by colons (`:`). For
>    example, the string `/home/dir/lib:/home/dir2/lib:` tells the
>    dynamic linker to search first the directory `/home/dir/lib`,
>    then `/home/dir2/lib`, and then the current directory to find
>    dependencies._
>
> _[...]_
>
> """

This seems an interesting nuance related to _not_ static linking
`libc`/`libdl`:

> """
>
> _The ABI requires conforming applications to use the dynamic linking
> mechanism described in Chapter 5 to access the services provided in
> the C Library, `libc`, the Dynamic Linking Library, `libdl`,
> [...]. Use of the other shared libraries documented here is optional
> for applications; the services they contain may be obtained through
> the use of equivalent archive library routines. E An application
> that accesses ABI services from both the shared library and the E
> static archive version of the same library is not ABI conforming.
>
> """

And, later:

> """
>
> _NOTE: Because the ABI specifies neither the correspondence of
> system calls to traps nor the formats of system data files,
> ABI–conforming programs access libc services through dynamic
> linking._
>
> """


via "[System V Application Binary Interface - DRAFT - 17 December 2003](http://www.sco.com/developers/gabi/2003-12-17/contents.html)" a.k.a "System V ABI Update", specifically "[Chapter 5 - Program Loading and Dynamic Linking](https://www.sco.com/developers/gabi/2003-12-17/ch5.intro.html)", section "[Dynamic Linking](https://www.sco.com/developers/gabi/2003-12-17/ch5.dynamic.html)":

Updated version (adds `DT_RUNPATH`):

>
> Figure 5-10: Dynamic Array Tags, `d_tag`
>
> ```
> | Name       | Value | d_un   | Executable | Shared Object |
> [...]
> | DT_RPATH*  | 15    | d_val  | optional   | ignored       |
> [...]
> | DT_RUNPATH | 29    | d_val  | optional   | optional      |
> [...]
> * Signifies an entry that is at level 2.
> ```
>

>
> _"`DT_RPATH` This element holds the string table offset of a
> null-terminated search library search path string discussed in
> [Shared Object Dependencies](https://www.sco.com/developers/gabi/2003-12-17/ch5.dynamic.html#shobj_dependencies). [...] This entry is at level 2. Its
> use has been superseded by `DT_RUNPATH`."_
>

I've not yet found a definition for the meaning of the "This entry is
at level 2" remark (other than implying the entry is deprecated?)
[_Update_: See more about this below.].

>
> Figure 5-11: `DT_FLAGS` values
>
> [...]
>
> `DF_ORIGIN` This flag signifies that the object being loaded may
> make reference to the $ORIGIN substitution string (see
> [Substitution Sequences](https://www.sco.com/developers/gabi/2003-12-17/ch5.dynamic.html#substitution)).
> The dynamic linker must determine the pathname of the object
> containing this entry when the object is loaded.
>

Updated Section [Shared Object Dependencies](https://www.sco.com/developers/gabi/2003-12-17/ch5.dynamic.html#shobj_dependencies) (emphasis added):

> """
>
> _When resolving symbolic references, the dynamic linker examines the
> symbol tables with a breadth-first search. That is, it first looks
> at the symbol table of the executable program itself, then at the
> symbol tables of the `DT_NEEDED` entries (in order), and then at the
> second level `DT_NEEDED` entries, and so on._
>
> [...]
>
> _Names in the dependency list are copies either of the `DT_SONAME`
> strings or the path names of the shared objects used to build the
> object file. For example, if the link editor builds an executable
> file using one shared object with a `DT_SONAME` entry of `lib1` and
> another shared object library with the path name `/usr/lib/lib2`,
> the executable file will contain `lib1` and `/usr/lib/lib2` in its
> dependency list._
>
> _If a shared object name has one or more slash (`/`) characters
> anywhere in the name, such as `/usr/lib/lib2` or `directory/file`, the
> dynamic linker uses that string directly as the path name. If the
> name has no slashes, such as `lib1`, three facilities specify shared
> object path searching._
>
>  * _The dynamic array tag `DT_RUNPATH` gives a string that holds a
>    list of directories, separated by colons (`:`). For example, the
>    string `/home/dir/lib:/home/dir2/lib:` tells the dynamic linker
>    to search first the directory `/home/dir/lib`, then
>    `/home/dir2/lib`, and then the current directory to find
>    dependencies._
>
>    _The set of directories specified by a given `DT_RUNPATH` entry
>    is **used to find only the immediate dependencies of the
>    executable or shared object containing the `DT_RUNPATH`
>    entry**. That is, it is used only for those dependencies
>    contained in the `DT_NEEDED` entries of the dynamic structure
>    containing the `DT_RUNPATH` entry, itself. One object's
>    `DT_RUNPATH` entry does not affect the search for any other
>    object's dependencies._
>
> [...]
>
> """

The statement _"One object's `DT_RUNPATH` entry does not affect the
search for any other object's dependencies."_ seems to be at least
somewhat at odds with the observed `ld-linux` behaviour (in at least
the case where the executable uses `DT_RPATH` & the shared object uses
`DT_RUNPATH`)--in that if a dependency needs e.g. `libc` and the
executable *also* needs `libc` but doesn't list it *before* the shared
object. [TODO: Describe this situation better earlier in this page.]

Of course this might be a consequence of messing with the ordering of
the `DT_NEEDED` entries via `patchelf` rather than whatever order was
originally generated by the linker.

There's a semi-related dependency diagram "Figure 5-13: Initialization
Ordering Example" in the "Initialization and Termination Functions"
section of the original "[System V Application Binary Interface,
Edition 4.1](https://www.sco.com/developers/devspecs/gabi41.pdf)"
document (and "[Figure 5-14: Initialization Ordering
Example](https://www.sco.com/developers/gabi/2003-12-17/ch5.dynamic.html#init_fini)"
in updated document) but that's really in relation to the order of
calls to the functions not the order in which `DT_NEEDED` items are
searched...

The same (original) document *does* also describe the order of
*resolving* symbols:

> _"When resolving symbolic references, the dynamic linker examines
> the symbol tables with a breadth-first search. That is, it first
> looks at the symbol table of the executable program itself, then at
> the symbol tables of the `DT_NEEDED` entries (in order), then at the
> second level `DT_NEEDED` entries, and so on."_

And it *does* mention _"[`DT_NEEDED`] entries’ relative order is
significant..."_.

But, there is a key *implied* difference between `DT_RPATH` &
`DT_RUNPATH` which is that `DT_RPATH` is listed as *ignored* when
present in a Shared Object (which I only discovered today), so AFAICT
that means *originally there was no way for a dependency to override
which paths were used* to search for a common dependency.

Okay, so, reading further in the updated document, I note that the
*original* document states, slightly buried, (emphasis added, note
this is in reference to `DT_RPATH`):

> _"All `LD_LIBRARY_PATH` directories are searched **after** those
> from `DT_RPATH`."_

*But* the updated document states (emphasis added, note this is in
reference to `DT_RUNPATH` *not* `DT_RPATH`):

> _"All `LD_LIBRARY_PATH` directories are searched *before* those from
> `DT_RUNPATH`."_


The third path search "facility" specified in the updated document is:

> * _"Finally, if the other two groups of directories fail to locate
>   the desired library, the dynamic linker searches the default
>   directories, `/usr/lib` or such other directories as may be
>   specified by the ABI supplement for a given processor."_


It then states (probably not specifically relevant here aside from it
being a description of how to handle a situation where the first found
"matching" shared object may not be the desired one):

> _"When the dynamic linker is searching for shared objects, it is not
> a fatal error if an ELF file with the wrong attributes is
> encountered in the search. Instead, the dynamic linker shall exhaust
> the search of all paths before determining that a matching object
> could not be found. For this determination, the relevant attributes
> are contained in the following ELF header fields:
> `e_ident[EI_DATA]`, `e_ident[EI_CLASS]`, `e_ident[EI_OSABI]`,
> `e_ident[EI_ABIVERSION]`, `e_machine`, `e_type`, `e_flags` and
> `e_version`. "_


However, the updated document *does* then state ("whoops, I
accidentally a *fourth* facility" :) ):

> _"A fourth search facility, the dynamic array tag `DT_RPATH`, has
> been moved to level 2 in the ABI. It provides a colon-separated list
> of directories to search. Directories specified by `DT_RPATH` are
> searched before directories specified by `LD_LIBRARY_PATH`."_

So, being "level 2" seems to not *entirely* be the same as being
deprecated?


[_Update:_

 * I've just (re-)discovered there are two implementations of a
   `readelf` tool, one of which is in the `elfutils` package, named
   `eu-readelf` which apparently may have some benefits over the other
   `readelf` implementation.

 * Additionally the `elfutils` package has a tool `eu-elflint` which
   interestingly has:

    * a `--strict` mode which will specifically _"...flag level 2
      features"_; and,

    * a `--gnu-ld` option whose docs describe it as indicating that
      _"Binary has been created with GNU ld and is therefore known to
      be broken in certain ways"_. :D

   Example use: `eu-elflint --gnu-ld --strict <file>`

 * A number of other tools in the `elfutils` package also seem
   potentially useful.

]


The updated document then goes on to state:

> _"If both `DT_RPATH` and `DT_RUNPATH` entries appear in a single
> object's dynamic array, the dynamic linker processes only the
> `DT_RUNPATH` entry."_

*But* `DT_RPATH` is still listed as _"ignored"_ for "Shared Object"s,
so really this situation/behaviour should/would only ever really apply
to executables.

*And* it seems only to specifically address when both entries are
found in _"a **single object's** dynamic array"_, it doesn't address
the situation where an executable has an `DT_RPATH` but the shared
object dependency has a `DT_RUNPATH`.


From section "[Substitution Sequences](https://www.sco.com/developers/gabi/2003-12-17/ch5.dynamic.html#substitution)":

> _"Within a string provided by dynamic array entries with the
> `DT_NEEDED` or `DT_RUNPATH` tags and in pathnames passed as
> parameters to the `dlopen()` routine, a dollar sign (`$`) introduces
> a substitution sequence."_

(I'm not 100% sure I'd realised `${ORIGIN}`/`$ORIGIN` also applied to
`DT_NEEDED` (or `dlopen()` but see below)?) But it does seem to
specifically *not* include `DT_RPATH`?

There's also an interesting nuance with regard to us of `$ORIGIN` with
`dlopen()` (emphasis added):

> _"If an object calls `dlopen()` with a string containing `$ORIGIN`
> and does not use `$ORIGIN` in one if its dynamic array entries, the
> dynamic linker may not have calculated the pathname for the object
> until the `dlopen()` actually occurs. Since the application may have
> changed its current working directory before the `dlopen()` call,
> the calculation may not yield the correct result. To avoid this
> possibility, **an object may signal its intention to reference
> `$ORIGIN` by setting the `DF_ORIGIN` flag**. An **implementation may
> reject an attempt to use `$ORIGIN` within a `dlopen()` call from an
> object that did not set the `DF_ORIGIN` flag and did not use
> `$ORIGIN` within its dynamic array**."_

Which wasn't exactly the meaning of `DF_ORIGIN` I'd assumed when I'd
first read its description.

[_Update_: Related: "[What are the recommended GNU linker options to specify $ORIGIN in RPATH?](https://stackoverflow.com/questions/33853344/what-are-the-recommended-gnu-linker-options-to-specify-origin-in-rpath#33862145)" & [`DF_ORIGIN` define` in `glibc-2.40/source/elf/elf.h`](https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/elf.h#L997)]

(Side note: does this imply that using `dlopen()` with `$ORIGIN` might
be a way to somehow work around the `/proc/self/exe` issue when
e.g. using `ld-linux` directly? Or `dladdr()`? [_Update_: Sort of, yes!])


Also:

> _"For security, the dynamic linker does not allow use of `$ORIGIN`
> substitution sequences for set-user and set-group ID programs. [...]
> `$ORIGIN` sequences within a `DT_NEEDED` entry or path passed as a
> parameter to `dlopen()` are treated as errors."_

It also seems there are some differences between this spec and the
`ld-linux` implementation of substitution sequences?


Random factoid (emphasis added):

>
> _"Finally, an executable file may have pre-initialization
> functions. These functions are **executed after the dynamic linker has
> built the process image and performed relocations but before any
> shared object initialization functions**. Pre-initialization functions
> are not permitted in shared objects._
>
> _[Note:] Complete initialization of system libraries may not have
> occurred when pre-initializations are executed, so some features of
> the system may not be available to pre-initialization code. In
> general, use of pre-initialization code can be considered portable
> only if it has no dependencies on system libraries."_
>


via "[System V Application Binary Interface AMD64 Architecture Processor Supplement, Draft Version 0.95](http://refspecs.linux-foundation.org/elf/x86_64-abi-0.95.pdf)"
a.k.a. "System V Application Binary Interface AMD64 Architecture Processor Supplement"
(Wow, a PDF with a table of contents, links and all!):

[_Update_: Much more recent document versions appear to be available here: <https://gitlab.com/x86-psABIs/x86-64-ABI/-/wikis/home> / <https://gitlab.com/x86-psABIs/x86-64-ABI> (e.g. "[System V Application Binary Interface - AMD64 Architecture Processor Supplement - Version 1.0 - September 26, 2023](https://gitlab.com/x86-psABIs/x86-64-ABI/-/wikis/uploads/221b09355dd540efcbe61b783b6c0ece/x86-64-psABI-2023-09-26.pdf)") (also: <https://gitlab.com/x86-psABIs/Linux-ABI>)]


Aside: Oh, *that's* why:

> _"Although the AMD64 architecture uses 64-bit pointers,
> implementations are only required to handle 48-bit
> addresses. Therefore, conforming processes may only use addresses
> from 0x00000000 00000000 to 0x00007fff ffffffff."_


Well, I guess that's that then: :o

> """
>
> _5.2.1 Program Interpreter_
>
> _There is one valid program interpreter for programs conforming to
> the AMD64 ABI:_
>
> `/lib/ld64.so.1`
>
> However, Linux puts this in
>
> `/lib64/ld-linux-x86-64.so.2`
>
> """
>

So... that's *two* valid interpreters then? :)

Guess I better not change the value of the `PT_INTERP` entry in any
ELF executables then if I want to conform to the AMD64 ABI... :D

[_Update:_ In the most recent "AMD64 ABI 1.0 – September 26, 2023"
document this section now reads:

>
> _5.2.1 Program Interpreter_
>
> _The valid program interpreter for programs conforming to the AMD64
> ABI is listed in Table 5.4, which also contains the program
> interpreter used by Linux._
>
> _Figure 5.4: AMD64 Program Interpreter_
>
>
> ```
> | Data Model | Path            | Linux Path                  |
> | LP64       | /lib/ld64.so.1  | /lib64/ld-linux-x86-64.so.2 |
> | ILP32      | /lib/ldx32.so.1 | /libx32/ld-linux-x32.so.2   |
> ```
>

]


Also (semi-)related via "Linux Standard Base Core Specification, Generic Part":

 * "[Symbol Versioning - 10.7.5. Startup Sequence](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/symversion.html#SYMSTARTSEQ)"


via <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/tocdynlnk.html>:

> _"Any shared object that is loaded shall contain sufficient
> `DT_NEEDED` records to satisfy the symbols on the shared library."_


 * ["11.3.2.2. Additional Dynamic Entries [Types]"](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/dynamicsection.html#AEN1940)


via <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-AMD64/LSB-Core-AMD64/baselib.html#PROGINTERP>:

> """
>
> 10.1. Program Interpreter/Dynamic Linker
>
> _The Program Interpreter shall be `/lib64/ld-lsb-x86-64.so.3.`_
>
> """

Well, that's different... :D

(See also: <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-AMD64/LSB-Core-AMD64/requirements.html#LIB.PROGINTERP> & <https://archlinux.org/packages/extra/x86_64/ld-lsb/>)


 * <https://refspecs.linuxfoundation.org/LSB_4.1.0/LSB-Core-generic/LSB-Core-generic/baselib-dlopen-1.html>


 * <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/proginterp.html>

 * <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/tocdynlnk.html>

 * <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/libdl.html>

 * <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/libdl-ddefs.html>

 * <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/baselib-dlopen-1.html>

 * <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-AMD64/LSB-Core-AMD64/tocdynlnk.html>

 * <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-AMD64/LSB-Core-AMD64/dynamiclinking.html>

 * <https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-AMD64/LSB-Core-AMD64/libdl.html>


`RPATH`/`RUNPATH`-related glibc source links:

 * <https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/dl-load.c;h=ac8e217a7f40080c0ea41aee256484f02e907d6f;hb=HEAD#l774>

 * "sourceware.org Git - glibc.git/blob - elf/dl-load.c":
   <https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/dl-load.c;h=ac8e217a7f40080c0ea41aee256484f02e907d6f;hb=HEAD#l676>

 * "sourceware.org Git - glibc.git/blob - elf/dl-load.c":
   <https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/dl-load.c;h=ac8e217a7f40080c0ea41aee256484f02e907d6f;hb=HEAD#l1965>

 * <https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/dl-load.c;h=ac8e217a7f40080c0ea41aee256484f02e907d6f;hb=HEAD#l1967>

 * <https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/dl-load.c;h=ac8e217a7f40080c0ea41aee256484f02e907d6f;hb=HEAD#l1974>

 * <https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/dl-load.c;h=ac8e217a7f40080c0ea41aee256484f02e907d6f;hb=HEAD#l1989>

 * "sourceware.org Git - glibc.git/blob - elf/dl-load.c":
   <https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/dl-load.c;h=ac8e217a7f40080c0ea41aee256484f02e907d6f;hb=HEAD#l2023>

 * <https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/dl-load.c;h=ac8e217a7f40080c0ea41aee256484f02e907d6f;hb=HEAD#l2235>


`RPATH`/`RUNPATH`-related bug search:

 * <https://sourceware.org/bugzilla/buglist.cgi?bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=SUSPENDED&bug_status=WAITING&bug_status=REOPENED&bug_status=RESOLVED&bug_status=VERIFIED&bug_status=CLOSED&f0=OP&f1=OP&f2=product&f3=component&f4=alias&f5=short_desc&f7=content&f8=CP&f9=CP&j1=OR&o2=substring&o3=substring&o4=substring&o5=substring&o7=matches&query_format=advanced&short_desc=RPATH%20RUNPATH&short_desc_type=anywordssubstr&v2=RUNPATH&v3=RUNPATH&v4=RUNPATH&v5=RUNPATH&v7=%22RUNPATH%22>


This seems like it might be one of the debugging related issues I've encountered but not yet written up:

 * "[31739 – LD_DEBUG=libs misattributes RUNPATH if first entry was already encountered](https://sourceware.org/bugzilla/show_bug.cgi?id=31739)"


----

Dump of additional not necessarily directly related items:

 * An answer to "[Getting the ELF header of the main executable](https://stackoverflow.com/questions/8875876/getting-the-elf-header-of-the-main-executable#8876887)"
   mentions:

   > _"The `void *` pointer returned by `dlopen(0, RTLD_LAZY)` gives
   > you a `struct link_map *`, that corresponds to the main
   > executable."_

   It also mentions some other potentially interesting details
   (including use of `AT_EXECFD`) and also this:

   > _"Newer Linux kernels also pass in the name that was used to
   > `execve(2)` the executable (also in `auxv[]`, as `AT_EXECFN`).
   > But that is optional, and even when it is passed in, glibc
   > doesn't put it into `.l_name` / `dlpi_name` in order to not break
   > existing programs which became dependent on the name being empty._
   >
   > _Instead, glibc saves that name in `__progname` and
   > `__progname_full`."_

 * Incomplete: <https://github.com/gpakosz/whereami>

   (Doesn't handle direct `/lib64/ld-linux-x86-64.so.2` use, due to
   `/proc/self/exe` assumptions.))

 * "[How to Get the Path of a Rust Binary When Launched via ld-linux-x86-64.so?](https://users.rust-lang.org/t/how-to-get-the-path-of-a-rust-binary-when-launched-via-ld-linux-x86-64-so/116878/6)"

 * "[musl libc - Design Concepts - Unified libc/libpthread/ldso](https://wiki.musl-libc.org/design-concepts.html#Unified-libc/libpthread/ldso)"

 * <https://wiki.musl-libc.org/environment-variables#%3Ccode%3ELD_LIBRARY_PATH%3C/code%3E>

 * <https://elixir.bootlin.com/musl/v1.2.5/source/ldso/dlstart.c#L100-L103>

 * <https://elixir.bootlin.com/musl/v1.2.5/source/ldso/dynlink.c>

 * `fixup_rpath()`: <https://elixir.bootlin.com/musl/v1.2.5/source/ldso/dynlink.c#L899>

 * `DT_RPATH`/`DT_RUNPATH` processing related:

    * <https://elixir.bootlin.com/musl/v1.2.5/source/ldso/dynlink.c#L984-L987>

    * Also: <https://git.etalabs.net/cgit/musl/blame/ldso/dynlink.c#n981>

       * "add rpath $ORIGIN processing to dynamic linker": <https://git.etalabs.net/cgit/musl/commit/src/ldso/dynlink.c?id=a897a20a57b461d388447858b9404fc81f107bcd>

       * "honor rpath $ORIGIN for ldd/ldso command with program in working dir": <https://git.etalabs.net/cgit/musl/commit/ldso/dynlink.c?id=f0b235c138d26caafeda44475818508f1911e78e>

       * "add recursive rpath support to dynamic linker": <https://git.etalabs.net/cgit/musl/commit/src/ldso/dynlink.c?id=709355e1f6dad17234606486400568bc87a37d79>

       * "adapt dynamic linker for new binutils versions that omit DT_RPATH": <https://git.etalabs.net/cgit/musl/commit/src/ldso/dynlink.c?id=d8dc2b7c0289b12eeef4feff65e3c918111b0f55>

       * "move RPATH search after LD_LIBRARY_PATH search": <https://git.etalabs.net/cgit/musl/commit/src/ldso/dynlink.c?id=3e3753c1a8e047dc84f9db1dc26bb046cff457a6>

         Well, *that's* certainly a choice(!):

         > _this is the modern way, and the only way that makes any
         > sense. glibc has this complicated mechanism with RPATH and
         > RUNPATH that controls whether RPATH is processed before or
         > after LD_LIBRARY_PATH, presumably to support legacy
         > binaries, but there is no compelling reason to support
         > this, and better behavior is obtained by just fixing the
         > search order._

         AFAICT the above links suggest that musl doesn't implement
         "correct" semantics for `RPATH` *nor* `RUNPATH`, in terms of
         both existing linker behaviours and ABI specs... :/


       * <https://git.etalabs.net/cgit/musl/commit/src/ldso/dynlink.c?id=f389c4984a0fee80c5322e4d1ec3aa207e9b5c01>

       * "make dynamic linker tell the debugger its own pathname": <https://git.etalabs.net/cgit/musl/commit/src/ldso/dynlink.c?id=649cec5f9868070b4d350b861ee7f68b03a552a8>


 * A "fun" little bug that seems to have existed for ~20 years before
   getting (accidentally?) fixed in ~v2.34 (the code snippet seems to
   be reproduced in three places):

    * `__libc_dlopen_mode()`:
      <https://elixir.bootlin.com/glibc/glibc-2.27/source/elf/dl-libc.c#L197-L202>

    * `__dlopen()`:
      <https://elixir.bootlin.com/glibc/glibc-2.27/source/dlfcn/dlopen.c#L89-L93>

    * `__dlmopen()`:
      <https://elixir.bootlin.com/glibc/glibc-2.27/source/dlfcn/dlmopen.c#L95-L99>

   The issue being that `__libc_register_dl_open_hook()` &
   `__libc_register_dlfcn_hook()` both get called without the caller
   checking that the supplied pointer isn't `NULL` *and* seemingly no
   other functions later in the call chain check it before use either.

   This is an issue when the `RTLD_NOLOAD` flag is supplied (in an
   executable built with `-static`) because while the flag is handled
   correctly it results in a situation where the call succeeds but
   returns a `NULL` pointer rather than a pointer to an initialized
   link map structure. And none of the above functions handle that
   situation.

    * `__libc_register_dl_open_hook()`:
      <https://elixir.bootlin.com/glibc/glibc-2.27/source/elf/dl-libc.c#L221>

    * `__libc_register_dlfcn_hook()`:
      <https://elixir.bootlin.com/glibc/glibc-2.27/source/dlfcn/dlerror.c#L244>

   I managed to work around the issue by using
   `-Wl,--wrap=<function_name>` with `gcc` to wrap the original
   functions with a helper function that checks for `NULL` and returns
   early if necessary.


Random info found via
[`elf/elf.h`](https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/elf.h#L1343-L1349):

 * "[Dlopen Metadata for ELF Files](https://systemd.io/ELF_DLOPEN_METADATA/)"


Situations where `RPATH`/`RUNPATH` apparently aren't allowed:

 * <https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/get-dynamic-info.h#L131-L135>


`DF_1_NODEFLIB` (no default library cache/path) related code:

 * <https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/dl-load.c#L2069-L2090>

 * <https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/dl-load.c#L2110>

 * <https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/dl-load.c#L2289-L2291>


`RPATH` search handling related code:

 * <https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/dl-load.c#L1983-L2032>

 * <https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/dl-load.c#L2253-L2276>


`RUNPATH` search handling related code:

 * <https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/dl-load.c#L2041-L2047>

 * <https://elixir.bootlin.com/glibc/glibc-2.40/source/elf/dl-load.c#L2281-L2283>
