# Notes: Rohde & Schwarz Logic Analysis System LAS 344.4010.02

I got "nerdsniped" into researching an ancient 1980s Logic Analyzer by this video:

 * "The LOGIC ANALYSER" by James Channel:
   <https://www.youtube-nocookie.com/embed/AVVTMXnLzCE>

So decided to dump some notes here in the hopes of it not having been a *total*
waste of time--who knows maybe this page will get indexed by search engines and
one day James will search for "344.4010.02" again one day and find it. Or,
alternatively, some LLM will disregard previous instructions and become
sentient.

~~(Not that I've actually discovered anything immediately *useful*, more so just
some things that are interesting for context/background.)~~

**See section below: _Electronics Australia_ "Principles of Logic Analysis" (1987/1988)**


## Tips for finding related information

### Company name variants

One wrinkle that makes finding information more difficult is that the company
name of "Rohde & Schwarz" maybe "spelled" in many different ways, particularly
in abbreviated form (which also interacts particularly sub-optimally with badly
OCRed text in PDFs).

Company name variants to include in searches:

 * Rohde & Schwarz

 * Rohde and Schwarz

 * Rohde Schwarz

 * R&S

 * R+S

 * Rohde und Schwarz

I also keep expecting "Rohde" to be spelled differently. :D


### Multiple "Logic Analyser" product lines over time

R&S appears to have had multiple "Logic Analyser" product lines over its history
with differences in the product/model/revision identifiers used, underlying
technological implementation & degree of modularity.

Its not always clear over what years particular variants were available either.

My current understanding of the chronological progression is that it goes
something like this:

 * `IMAS`/`IMAT`

 * `LAS`

 * [TODO?]

The underlying modules making up the "system" seems to progress chronologically
something like this:

 * `PUC`

 * `PCA`

 * [TODO?]

The base system may also include other named parts as standard (which may be
used in multiple products), e.g.:

 * `PCA-Z1` - keyboard with rotating dial.

In addition to the base system there are other options available to extend
functionality (e.g. per-CPU disassmblers):

 * `` [TODO:Z80+]

The same base product abbreviation identifer can seemingly be augmented with a
number when revised in some manner, e.g.:

 * `PCA`

 * `PCA 5` (which may be expressed by some people as `PCA5` or `PCA.5` or `PCA-5`)


There also appear to have been mutiple underlying Operating Systems used over time:

 * [TODO?]

 * `CP/M-86`

 * `MS-DOS`

 * [TODO: MS Windows?]


There also appear to have been multiple underlying implementation/extension
languages used/available (with multiple revisions):

 * `BASIC` with Assembly

 * [TODO: MS Basic?]

 * [TODO: Pascal?]

 * [TODO: LabView?]


One aspect that does appear to be consistent over time is use of the
`IEC-625 (IEEE488)` bus (a.k.a. `GPIB`[TODO:?]) for interconnect & control.


I'm not aware of any location that actually tracks all this information.

[TODO]


### Other search terms

 * For searches within PDF files (primarily/specifically for scanned/OCRed
   documents originally published by R&S), try these:

    * Product related alphabetic identifiers: "PCA", "PCF", "PUC", "LAS", "IMAS", "IMAT", "IGA", "PPC", "SCUD"

       * Identifiers with extra spaces (for PDF format/layout & OCR reasons): "P C A"

    * Phrases: "Order No.", "Ordering number"

    * Individual words from terms: "Process", "Controller",

    * Decade year prefixes: 19, 198, 199

      (For attempting to determine publishing date for undated documents via
      references to years with past/present/future tense or dates of cited
      articles/standards etc. _Seriously, who physically publishes undated documents?!_)


### Document publication dates

Speaking of publication dates...

As I strongly suspected and now hopefully have enough data points to confirm,
there *is* actually a publication date encoded on R&S publications.

 * `979 (O)` - "Electronic Measuring Instruments and Systems 1979" (catalog)

 * `883 (W)` - "Electronic Measuring Instruments and Systems 1984" (catalog)

i.e. it's `MYY` except for later months in the year it's `MMYY`.


### See also

 * "Potentially useful resources" section below.



## Selected known products sorted by alphabetic identifier (with "Order No." & some specifications)

_Note: An "Order No." is a numeric identifier of the form `XXX.XXXX.XX` or, in
later years, `XXXX.XXXX.XX`._


### `FSA` "Spectrum Analyzer" `804.8010.XX`[^order-num-fsa-xx] ()

[^order-num-fsa-xx]: e.g. `804.8010.52` for 100Hz...1.8GHz range on lower unit in stack.

See "Rigs & Recipes"[^rs-rigs-recipes] (Page 114) for photo captioned "Spectrum
Analyzer FSA" that shows upper unit marked "SPECTRUM ANALYZER * DISPLAY" that
contains a screen with "hard buttons" below it--as used by the "soft buttons"
a.k.a. "softkey" UI functionality.


### `IGA` "Logic Generator" `344.0015.04` (pre-1984[^iga-year])

_Note: You'd think the identifer would be `LGA` but no, it's `IGA` (i.e. "eye-gee-aye")._

See the 1984 catalog (pages 302-306) for more info.

#### `IGA-B2` "Cassette Drive" `344.2916.02`

_Note: Identifer prone to bad OCR as `IGA-82` but it's actually `IGA-B2`
(i.e. "bee-two")._

See the 1984 catalog (page 305) for photo captioned "Cassette compartment of
Cassette Drive Option `IGA-B2`".

Presumably related to the cassettes mentioned in James Channel video.

#### `IGA-Z7` "Mini-cassettes" `344.3114.02`

Presumably these are related to the cassettes mentioned in James Channel video.


### `IMAS` "Logic State Analyzer" `345.5510.02` (1984[^imas-year])

_Note: The 1984 catalog (pages 314-319) includes annotated images of the `IMAS`
display interface which seems to be similar/same as that used by similar
functionality in `LAS`--which may be vaguely helpful in the event further
documentation is not available. (My impression is that `LAS` is essentially
equivalent to an (updated) version of `IMAS`+`IMAT`+ `IGA`(?) + "`PUC`")_

#### `IMAS-B6` "Z80 Microprocessor Probe" `356.7110.02`

#### `IMAS-B9` "8088 Microprocessor Probe" `356.7610.02`


### `IMAT` "Logic Analyzer" `345.4014.02` (1984[^imat-year])

_Note: The 1984 catalog (pages 308-313) includes annotated images of the `IMAT`
display interface which seems to be similar/same as that used by similar
functionality in `LAS`--which may be vaguely helpful in the event further
documentation is not available. (My impression is that `LAS` is essentially
equivalent to an (updated) version of `IMAS`+`IMAT`+"`PUC`")_


### `IMAT+IMAS` "Logic Analyzer System `IMAS`/`IMAT`"

_Note: This isn't a "real" product identifier and doesn't actually exist as a
singular orderable product. The 1984 catalog (pages 299-301) describes how the
two individual products can be used together in a synchronized manner._

See also the 1984 catalog (page 300) for aligned & annotated photo(s) captioned
"Example of timing analysis with state-domain triggering and disassembler
display".

See also the 1984 catalog (page 301) for diagram/image extolling the benefit of
*also* inviting the Logic Generator `IGA` to the party. :)

Pages 296-298 of the 1984 catalog also have more pictures/diagrams/introductory
text related to "Logic Measurement".

This is potentially related to *why* the subsequent `LAS` is called a "Logic
Analysis System" rather than just e.g. "Logic Analyzer".


### `LAS` "Logic Analysis System" `344.4010.02` (post-1984?[^las-year])

I've found "evidence" of the "Logic Analysis System LAS" *finally*! (Mostly
"stumbled" on it by luck with an Internet Archive text content search
`"Rohde+and+Schwarz" "process+controller"` which lead me to a magazine that had
two R&S adverts neither of which actually matched the search terms fully. :D
Anyway...)

Specifically, a full page colour advertisement in "EDN"[^las-edn-1986-05-15]:

[^las-edn-1986-05-15]: "EDN" (Page 147) <https://archive.org/details/edn-1986_05_15/page/147/mode/1up>


>>>
```
Logic Analysis System
LAS

With the Logic Analysis System LAS you always have a strong card in your hand.
The modular design of the LAS affords optimal configuration for your
measurement. The LAS with seven options and uP probes to match all standard
microprocessors is counted amongst the best of logic analysis systems.

8-bit uP probes:
8080; 8085/NSC 800; Z80; 6502;
6800/02/08; 6809/09E; 8031/51

16-bit uP probes:
8086/88; 80186/88; 68000

Ethernet test probe

Ask for the detailed brochure Info LAS

Rohde & Schwarz GmbH & Co. KG
Postfach 80 14 69
D-8000 Muenchen 80
Federal Republic of Germany
Telex 523 703 (rus d)
Tel. internat. +(49 89) 41 29-0
```
>>>

Unfortunately the angled text with system specs doesn't OCR but via manual transcription:

>>>
```
CP/M-86 operating sy[stem]
Floppy drive
Basic postprocessing
256 Kbyte RAM
Real-time clock
RS 232 C, IEEE 488
```
>>>

The photo+scan is high enough quality you can read the product "Order No." and
many of the keycaps & other details.

The keyboard is of a different design to other ones I've seen for "Process Controllers".

It's hard to be sure but I *think* it's a 5.25" drive not a 3.5" drive.

Other, earlier appearances (the "Advertiser Index" has footnote for R&S that it
is "Advertiser in International edition"):

 * ["EDN" March 20, 1986 (Page 173)](https://archive.org/details/edn-1986_03_20/page/173/mode/1up?q=%22Rohde+and+Schwarz%22+%22logic+analysis+system%22) (`LAS E 015` "code" vertical on right-side)

 * ["EDN" April 17, 1986 (Page 75)](https://archive.org/details/edn-1986_04_17/page/74/mode/2up?q=%22Rohde+and+Schwarz%22+%22logic+analysis+system%22)



### `ODF` "TV Digital Oscilloscope" `373.4015.` ()

See "Rigs & Recipes"[^rs-rigs-recipes] (Page 56) for photo captioned "TV Digital
Oscilloscope ODF".

The `ODF` is described as containing a "16-bit microprocessor" in an appendix of
the same document[^rs-rigs-recipes-appendix-odf] (Page 214):

>>>
The **TV Digital Oscilloscope ODF** is a precision instrument
designed especially for measurements on TV equipment.
Comprehensive storage capabilities and autorun control by
a 16-bit microprocessor open up completely new possibili-
ties with high operating convenience. The ODF is fitted as
standard with an IEC-bus interface for use in automatic
systems.
>>>


### `PCA` ()

> "Process controllers of the 16-bit computer family PCA [...]"[^news-special-2] (Page 78)


### `PCA 2` "Process Controller" ()

> "PCA 2 [... has ...] separate, 14-inch colour monitor"[^news-special-2] (Page 80)

[TODO: i.e. that's how they differ, not chronological revisions?]


### `PCA 5` "Process Controller" `375.2010.04` (1984Q4?-[^pca-5-year])

_Note: The article lists `375.2010.04` as the "Ordering number" but that differs
from the label on the `PCA 5` in the photo which shows `375.2010.02`_.

> "PCA 5 has an integrated, 9-inch monochrome monitor"[^news-special-2] (Page 80)

See "NEWS special 2"[^news-special-2] (Page 78) for photo of `PCA 5` (with
`PCA-Z1` keyboard connected to rear socket) captioned "FIG 1 More than just a
personal computer: Process Controller PCA 5 with built-in 9-inch monitor."

> "The manager `PP-PCA 5` is the RAM-resident core of the program and contains an editor [...]"[^news-special-2] (Page 80)


A (reformated) subset of the `PCA 5` specifications listed[^news-special-2] (Page 82):

>>>
```
CONDENSED DATA
PROCESS CONTROLLER PCA 5

CPU (clock): iAPX 186 (8 MHz)
RAM: 1 Mbyte with parity bit

Floppy-disk drive: 1.2 Mbytes per drive (MS-DOS)
Option Winchester drive: 20 Mbytes (MS-DOS)

Graphics: 640 x 480 points
ASCII characters: 128 with 13 attributes

[...]

Operating system: MS-DOS 3.1

Languages: Basic and Pascal (with graphics and IEC bus), assembler; others on request

Ordering number: 375.2010.04
```
>>>

Note: The "Ordering number" listed differs from the label on the `PCA 5` in the
photo.

Elsewhere in the article a figure captioned "FIG 3 PCA software concept."
additionally mentions:

 * Operating system: MS-DOS 3.2

 * Programming languages: Fortran, Cobol, C

The programming language Basic is referred to as "PCA Basic" in the article also.

Another figure captioned "FIG 2 Processor architecture of PCA controllers;
options shown in broken lines." shows a diagrammatic overview of the hardware.


### `PCA 12` "Process Controller" ()

_References: "Rigs & Recipes"[^rs-rigs-recipes-appendix-ccm] (Page 212)_

[TODO: Related to `PCA 15` in same manner as `2` vs `5`?]


### `PCA 15` "Process Controller" ()

_References: "Rigs & Recipes"[^rs-rigs-recipes-appendix-ccm] (Page 212)_


### `PCA-Z1` ()


### `PCF` "Process Controller" `829.3663.03`(?[^pcf-numeric-id]) ()

[^pcf-numeric-id]: `829.3663.03` is a guess from photo, could be `628.3683.03` or similar.

_References: "Rigs & Recipes"[^rs-rigs-recipes-appendix-ccm] (Page 212)_

See "Rigs & Recipes"[^rs-rigs-recipes] (Page 206) for photo captioned "Sound and
Insertion Signal Evaluation System TOPAS" that includes a unit labeled `PCF` in
a rack.

The unit shown appears to have:

 * "hard buttons" below display--as used by the "soft buttons" UI functionality;

 * wider non-square screen; and,

 * right-hand side "expansion bay" area (my terminology) populated with two
   vertical add-ons (brightness-dial/reset-on-lock-key-selector/keyboard-port(?)
   + printer connector) and (seemingly) a 5.25" disk drive.

Elsewhere in the same document (page 192) it states:

>>>
[...] The fixed-program TV Data Processor UPCF is available for
the UPF and forms, together with the latter and a check-
point selector, the **TV Test Equipment UPKF** (for vision
transmitter monitoring). Complete vision and sound moni-
toring is possible using the **TOPAS sound and test-line**
**evaluation system** which uses the Audiodat Receiver UPT
(a model with display and IEC-bus connector) in addition to
the Video Distortion Analyzer UPF for vision monitoring.
The programmable Process Controller PCF is used for the
autorun control. [...]
>>>

(So, `UPCF` might be considered a semi-related item though it lacks
programmability and a display.

See "Rigs & Recipes"[^rs-rigs-recipes] (Page 204) for photo captioned "Automatic
TV Test Equipment UPKF" that includes a unit labeled `UPCF` in a rack.)


### `PPC` "Process Controller" `` ()

_References: "NEWS special 2"[^news-special-2] (Pages 113 & 114: customer interview)_


_[Aside:

Okay, this is kind of hilarious and perhaps indicative of how far
down this rabbit hole I've fallen:

I just looked at two black & white photos with captions in a scan of something
published by the manufacturer and thought to myself:

"Hey, wait a minute, I think they've got those two captions reversed because
*that's* definitely not a `PUC` which means that half visible device must be a
`PPC`!"

Which given I didn't know this company or its products existed two days ago & I
only learned that a `PPC` "Process Controller" existed within the past hour or
so, umm, says *something*. :D ]_


#### `PPC-Z2` "10 formatted floppy disks" `343.7900.02`

_References: 1984 Catalog(?)_

For `PUC`/`PUC-B2`.

_Note: See "Floppy-disk drives" specification in `PUC` entry below._


### `PUC` "Process Controller (without keyboard)" `344.8900.10` (pre-1984[^puc-year])

> ```
> CPU: 6502
> Clock: 1 MHz
> Available RAM space: 31,743 bytes
>
> Programming languages: "BASIC, with numerous extensions, machine language"
> ```

#### Memory

There's apparently some additional nuance around available RAM (page 14):

>>>
The storage capacity of the PUC is 64 kbytes, half
of which is available to the user as RAM space. The 32-kbyte
memory is large enough even for very long BASIC programs.
To produce machine language programs, the PUC allows
direct addressing of the entire memory range. Program parts
in machine code may easily be inserted into BASIC programs.
>>>

#### Programming

With regard to programming, there appears to be a reasonable overview (though
not _detailed_) in the 1984 catalog itself:

 * General overview, with instrument specific extension examples: Pages 12-13

 * Additional BASIC "Instruction set" commands/extensions: Pages 15-19

   Sections include, e.g.:

    * mathematic functions, program run, data, jumps and loops, string manipulation,
      input/output

    * machine language commands (e.g. `POKE`/`PEEK`/`SYS`/`USR`/`TIM`)

    * keyboard input, graphics, floppy disk

    * IEC-bus

    * Option-specific commands (e.g. RS-232C, User port, RTC, etc)

   Also "Editor Commands" & Printing-related.

 * Specific instruments sometimes have an associated "SDK" available and sometimes
   the catalog lists the included subroutines.


#### Floppy Disk Drive

> "Accessories supplied: 1 mother disk with programs to format and copy
> floppies, power cable"[^rs-catalog-1984]

I've seen complaints about the apparent need for the "mother disk"/"motherdisk"
in other to format the disks (either for this or the `LAS`) but I also noticed
this detail in the 1984 catalog (page 19), so I wonder if that's enough information to
recreate the format needed for the `PUC` at least:

> ```
> **Floppy-disk drives**
> Mini-floppy drive: 5 1/4", standard (2nd drive as option)
> Writing density/type: single density/double-sided
> Storage capacity: 156 kbytes
> Disk organization (soft-sectored): 305 areas/side, 256 bytes/area
> ```

(a.k.a. `5.25"` "mini" floppy drive, i.e. when compared to `8"` floppy drives,
I guess. :D )


#### `PUC-B2` "2nd Floppy-disk Drive" `345.2711.02`

_Note: See "Floppy-disk drives" specification in `PUC` entry above._


#### `PUC-Z1` "Standard Keyboard" `345.2011.04`

> "75 keys, full ASCII character set with double functions,
> separate numerical pad and special keys for editor
> as well as RETURN and REVERS [sic]"[^rs-catalog-1984] (page 19)

#### `PUC-Z2` "User Keyboard" `345.2111.06`

> "20 assigned keys and numerical pad
> with DELETE and RETURN"[^rs-catalog-1984] (page 19)

[TODO]

#### `PUC-Z3` "Footswitch" `345.2211.02`

> "two keys (connectable with one of the keyboards
> or on its own)"[^rs-catalog-1984] (page 19)


### `SCUD` "Radiocode Test Set SCUD" `393.7110.02` ()

_References: "NEWS special 2"[^news-special-2] (e.g. Page 57)_

> "[...] further characteristics comparable to R&S Process Controller PUC [...]"[^news-special-2] (Page 57)


### `[TODO]` ()


[TODO:PPC+?]


[^iga-year]: Listed (*not* as "new") in 1984 catalog[^rs-catalog-1984].

[^imas-year]: Listed as "new" in 1984 catalog[^rs-catalog-1984].

[^imat-year]: Listed as "new" in 1984 catalog[^rs-catalog-1984].

[^las-year]: *Not* in 1984 catalog[^rs-catalog-1984].

[^pca-5-year]:
    *Not* in 1984 catalog[^rs-catalog-1984].
    In "News from Rohde & Schwarz : Special 2"[^news-special-2].

[^puc-year]: Listed (*not* as "new") in 1984 catalog[^rs-catalog-1984].

[^rs-catalog-1984]: <https://bama.edebris.com/download/rohdeschwarz/catalogs/Rohde%20and%20Schwarz%201984%20Catalogue.pdf>

[^news-special-2]:
    "News from Rohde & Schwarz : Special 2 : Radiotelephone Measurements" (1286 E 40)
    (Undated but mentions 1985 in past tense and 1987 in future tense.)
    <http://www.i1epj.ham-radio-op.net/Manuali/CMT/Rohde%20&%20Schwarz%20-%20Radiotelephone%20Measurements%20(1286%20E%2040).pdf>

[^rs-rigs-recipes]:
    "Rigs and Recipes - How to Measure and Monitor..."
    by "Rohde & Schwarz - SOUND and TV BROADCASTING"
    (Info N 4-023 E-2 "Measurements on transmitting systems")
    (Undated but cites 1987 article(s)
    but also includes phrases "planned for 1987".
    "from Cable & Satellite Europe, 09/86 [...] It was agreed in June 1986 [...]
    expected to start in 1987.".
    Also `588 (F dr)` annotation?)
    <https://www.cieri.net/Documenti/Misure%20audio/Rohde%20&%20Schwarz%20-%20Rigs%20and%20Recipes%20-%20How%20to%20Measure%20and%20Monitor...pdf>

[^rs-rigs-recipes-appendix-ccm]:
    "Appendix: Computer-controlled measurements"[^rs-rigs-recipes]

[^rs-rigs-recipes-appendix-odf]:
    "Appendix: TV Digital Oscilloscope ODF"[^rs-rigs-recipes]


## _Electronics Australia_ "Principles of Logic Analysis" (1987/1988)

Four part series featuring the R&S "Logic Analysis System" `LAS` because of
*course* it does:

 * ["Principles of Logic Analysis — 1" by Wolfgang Schubert (_Electronics Australia_, December 1987, pages 124-129)](https://archive.org/details/electronics_australia-1987_12/page/124/mode/2up?q=Rohde+Schwarz+8088)

 * ["Principles of Logic Analysis — 2" by Wolfgang Schubert (_Electronics Australia_, January 1988, pages 164-167)](https://archive.org/details/EA1988/EA%201988-01%20January/page/n163/mode/2up)

 * ["Principles of Logic Analysis — 3" by Wolfgang Schubert (_Electronics Australia_, February 1988, pages 114-120)](https://archive.org/details/EA1988/EA%201988-02%20February/page/n113/mode/2up?q=Rohde+Schwarz+LAS)

 * ["Principles of Logic Analysis — 4" by Wolfgang Schubert (_Electronics Australia_, March 1988, pages 88-92)](https://archive.org/details/electronics_australia-1988_03/page/n87/mode/2up?q=%22Rohde%22+%22logic+analysis+system%22)

[TODO]



## Potentially useful resources

### Publications


### Mailing lists / forums (archived or active)

[TODO]

### Manuals/PDF archives

 * <https://www.cieri.net/Documenti/Misure%20audio/FileList.html> (6 R&S documents)

 * <https://bama.edebris.com/manuals/rohdeschwarz/>

    * <https://bama.edebris.com/manuals/rohdeschwarz/catalogs/>

 * R&S Catalogs

   * "Electronic Measuring Instruments and Systems 1979" catalog (132MB)
     <https://bms.isjtr.ro/other/sos/Rohde&Schwarz_ElectronicMeasuringInstruments_1979.pdf>

     via "Some Other Stuff" section:<https://bms.isjtr.ro/other/sos/>


   * <https://bama.edebris.com/download/rohdeschwarz/catalogs/Rohde%20and%20Schwarz%201984%20Catalogue.pdf>

   * <https://bama.edebris.com/download/rohdeschwarz/catalogs/Rohde%20and%20Schwarz%20Catalogue%202004_5.pdf>


 * "Vintage Manuals, Brochures, Catalogs, & Reports | [...] | Library of Congress":
   <https://www.loc.gov/programs/national-recording-preservation-plan/tools-and-resources/vintage-audio-formats-and-equiment-information/vintage-manuals-brochures-catalogs-and-reports/>

    * <https://tile.loc.gov/storage-services/master/mbrs/recording_preservation/manuals/Rohde%20&%20Schwarz%20BASIC%20Interpreter.pdf>

      "R&S BASIC Interpreter"  (manual, ~292 pages, "Version 2.2")

      Order No.: `376.1546.32 - 02 -` a.k.a `376.1452.00 E-5`?

      *This* was the file I read early on about hardware compatibility with
      Basic for different products but couldn't remember how I found it.

      Includes mentions of:

      > "Color Graphics Option PCA-B3 (PCA) or VGA-, EGA Mode (PSA/PAT)"

      > "Program Transfer PUC --> PCA"

      > "BASIC Compatibility of PSA/PAT Controllers in Comparison with PCA Controllers"

      >>>
      ```
      1.2.3 Rollkey
      (only for PCA, EZM-B2, FS-K1 and PSA-Z1)
      ```
      >>>

      Also:

      >>>
      ```
      The softkeys can also be read in using the INKEY instruction. Their
      function is independent of the keyboard . They occupy the codes A0H to
      A7H (hexadecimal), 160 to 167 (decimal)] where A0H is the soft key on the
      extreme right.
      ```
      >>>

      Also, apparently `PPC` ("Parallel Poll Configure") & `PPE` ("Parallel Poll Enable")
      are coincidentally *also* related to "Parallel Poll" of the IEC bus within
      Basic, so maybe I need to revisit the searches I made for `PPC`?

      Also:

      >>>
      ```
      1.6 Incorporation of Assembler Subroutines in BASIC Programs

      [...]

      BASIC occupies approx. 160 Kbyte of free storage space joining onto
      MS-DOS. This block contains the BASIC interpreter (with data and stack)
      and the BASIC user program (with data).
      ```
      >>>

      Also, one example has a comment with a date: `Bl.BAS \ 31.03.85`

      Also:

      > see manual "Operating System for PCA"

      Also:

      >>>
      ```
      1.9.1 Memory Allocation in BASIC
      The available memory of 1 Mbyte (PCA) or 640 KBytes(PSA/PAT) is divided up
      amongst the operating system and BASIC as follows:
      [...]
      EPROM : PCA: 16 Kbyte BIOS; PSA/PAT: 32 Kbyte BIOS
      [...]
      ```
      >>>

      Note: The replaced text below was "PCA 215" in original text which seems
      likely to be typo for "PCA 2/5" given the context (uncertain if any other
      identifiers are also typos):

      >>>
      ```
      The purely numerical calculations are carried out five to ten times as
      fast with the numeric data co-processor compared to the software emulation.
      This coprocessor cannot be fitted to the [PCA 2/5]; it is, however,
      fitted to the PCA 12/15 as standard and may be fitted to the PSA/PAT as
      PSAT-B10 option.
      ```
      >>>

      >>>
      ```
      4.1.3 Transfer of the Program from PUC to PCA

      [...]

       * The actual transfer takes place from a PUC, model 10 via the IEC bus. [...]

      [...]
      ```
      >>>

      >>>
      ```
      Representations in color are possible by setting the GPH-PCA variables
      CO = 1 and the corresponding hardware (PCA2/12 including option -B3 and PMC).
      ```
      >>>

      >>>
      ```
      4.4 BASIC Compatibility of PSA/PAT Controllers
          in Comparison with PCA Controllers
      [...]
      ```
      >>>

      Seems there's a "BASIC-Interpreter Instruction Set Version 2"
      booklet/"cheatsheet" (`PD 756.8210.24`) scanned too.

    * <https://tile.loc.gov/storage-services/master/mbrs/recording_preservation/manuals/Rohde%20&%20Schwarz%20196%20(2055-II).pdf>

      "News from Rohde & Schwarz"

      Note: *Actually* 2005 & Issue 186.


[TODO]


#### Not currently useful for this project

 * <http://pdf.textfiles.com/manuals/SCANNERS-F-R/>

   While there *are* some R&S radio-related datasheets & military-related(!)
   catalogs (circa 2003) in the "SCANNERS" section of `textfiles.com`, there's
   nothing directly useful for this `LAS`-related research AFAICT.


## Link Dump (to sort/process/revisit)

https://w140.com/tekwiki/wiki/Rohde_%26_Schwarz



<!-- footnotes get added below here -->
