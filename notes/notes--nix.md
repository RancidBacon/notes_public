## Notes: `nix`

 * Informative notes (from `jvns.ca`) about understanding `nix` as a "non-nix" person:

    * "[Some notes on using nix](https://jvns.ca/blog/2023/02/28/some-notes-on-using-nix/)"

       * Mentions "[binary package cache](https://jvns.ca/blog/2023/02/28/some-notes-on-using-nix/#what-s-interesting-about-nix)".

       * Perhaps the [most useful observation about `nix`](https://jvns.ca/blog/2023/02/28/some-notes-on-using-nix/#some-nix-features-i-m-not-using) for me:

         > *There are a bunch of nix features/tools that I’m not using [...] **I originally thought that you had to use these features to use nix**, because most of the nix tutorials I’ve read talk about them. **But you don’t have to use them.***

         Which (a) I _also_ thought was the case; and, (b) is really useful to know in relation to my "*just give me pre-built binaries that will work regardless of who's deprecated which `libc` version*" desire/goal--without a whole lot of cognitive burden not specifically necessary for reaching the goal.

    * "[How do Nix builds work?](https://jvns.ca/blog/2023/03/03/how-do-nix-builds-work-/)"

 * So, because I'm...apparently me, here's some notes on "*Retrieving pre-built binaries directly from the `nix` binary cache without using `nix` tooling*":
 
    * Unfortunately seems there's no direct browsing of the binary cache server here: <https://cache.nixos.org/>

    * However, there is a wiki section named "[How to check if content is on a binary cache](https://nixos.wiki/wiki/Binary_Cache#How_to_check_if_content_is_on_a_binary_cache)" which is useful for determining how to peform the binary download process "manually".

    * By way of example, consider the package `wezterm` (which I'm currently manually patching with `patchelf` for `libc`-related reasons):

       * Search for `wezterm` via: <https://search.nixos.org/packages>

       * [TODO: Write up additional steps here.] (See `[001]` below.)

       * Then retrieve the associated `.narinfo` file: `https://cache.nixos.org/bi68an831k754lycara7i5dylhmk13i4.narinfo`

         Which has the following content:

            ```
            StorePath: /nix/store/bi68an831k754lycara7i5dylhmk13i4-wezterm-20230408-112425-69ae8472
            URL: nar/0gvfz2n8adgdwh6mnzjy54921b8idi45hiqwkgxxv0fcn7qc6g1m.nar.xz
            Compression: xz
            FileHash: sha256:0gvfz2n8adgdwh6mnzjy54921b8idi45hiqwkgxxv0fcn7qc6g1m
            FileSize: 41325832
            NarHash: sha256:0d5hbh64g7gg4fa7n9l6yd360pwjzdanj0qjmsp6n1wwzb89iac9
            NarSize: 159759224
            References: 050hxdbnylfcf9jkx7rsbn5dcml09lg1-xcb-util-image-0.4.1 0mwnvw81sv5bxs30pcnivi1fld7iqqsx-fontconfig-2.14.0-lib 0wbc29xym943d6vh2x1x8w6qx4m3chc5-libxkbcommon-1.5.0 1nyg1fvhpz8bx3vn3r9f18zhra2rpbx9-glibc-2.37-8 28wb2asziwwv2wxynqi9nxiigycsjjmg-zlib-1.2.13 idzdhlk14zc47h15drwq4q0c18r03vd5-xcb-util-0.4.1 pf1gmm5bhzqr2qv3by264qri0jbi9a5l-wayland-1.21.0 q1gzrms8a43hvqyxzr9zds25ylkidgrj-vulkan-loader-1.3.243.0 s76lla2ah98g5d3ywn878pk1piqqnbdh-libGL-1.6.0 ssw57ksmwz8ijcvyrzd29nl5x49qrpnd-libX11-1.8.4 vmpna0kmb9yqar5snq7wk0lv80a1572r-gcc-12.2.0-lib whqrwcw8s6d74jks5p9xzzcz6jpb4gxm-libxcb-1.14 x5h52r1sk0ad87m5mh6i543x2ryd1618-wezterm-terminfo y5nbmq190bnjvph3xgizzxn7lqmr761s-openssl-3.0.8
            Deriver: 7zhvxl765di6c8ay25cnfp7rclpb06i9-wezterm-20230408-112425-69ae8472.drv
            Sig: cache.nixos.org-1:pL0UhSpbm3Ixnr43SpLDbR7IDBqkmPzcRiIU++AdbWubElVXlDjN7duOR05IFXv7VTL7fgF7snGFe1XXfxm+DQ==
            ```

         My current understanding is the `URL` field value allows one to construct the necessary URL for retrieval of the binary file (as a `xz` compressed "Nix archive" file), in this case:

          * `https://cache.nixos.org/nar/0gvfz2n8adgdwh6mnzjy54921b8idi45hiqwkgxxv0fcn7qc6g1m.nar.xz`

         I'm more hazy on exact details for `StorePath` but I think its where the "final" form of the (extracted? archive?) file(s) is to be placed on the local machine, e.g. in this case:

          * `/nix/store/bi68an831k754lycara7i5dylhmk13i4-wezterm-20230408-112425-69ae8472`


 * via <https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-nar.html>:

   > _`nix nar` provides several subcommands for creating and inspecting Nix Archives (NARs)._

   This page also mentions:

   > _For the definition of the NAR file format, see Figure 5.2 in <https://edolstra.github.io/pubs/phd-thesis.pdf>._

   Which I guess might explain why finding such information seemed to require non-trivial effort?

 * `.nar` manipulation:

    * [`nix-nar-cli`](https://lib.rs/crates/nix-nar-cli) / [`nix-nar`](https://lib.rs/crates/nix-nar) -- "Binary to manipulate Nix Archive (nar) files" / "Library to manipulate Nix Archive (nar) files"

      (Was able to "extract" `wezterm` binary with this but also subsequently ran into some expected complications with the extracted file... [TODO: Go into more detail on this.])

    * From same project:

      * Some other random `nix`-related details: "[`nix-store --export` style archive support?](https://gitlab.com/abstract-binary/nix-nar-rs/-/issues/4#note_1368503175)"


 * Also interesting/relevant:

    * https://nixos.wiki/wiki/Applications -- "the Extended Nix Ecosystem"

    * https://nixos.wiki/wiki/Nix_Hash

    * https://nixos.wiki/wiki/Nix_package_manager#Internals

    * https://nixos.org/manual/nix/stable/command-ref/new-cli/nix3-bundle.html / https://github.com/NixOS/bundlers

    * https://github.com/matthewbauer/nix-bundle

       * Includes "[[Experimental] Create AppImage executables from Nix expressions](https://github.com/matthewbauer/nix-bundle/blob/83eadede9087c2c94a018ea6e240da552355dcc1/README.md#experimental-create-appimage-executables-from-nix-expressions)"!

    * <https://github.com/taktoa/narfuse> -- "FUSE filesystem, written in Haskell, that allows one to mount NAR (Nix archive) files and expose them as a virtual Nix store."

    * <https://github.com/edolstra/dwarffs> -- "FUSE filesystem that allows tools like gdb to look up debug info files via HTTP" // "...fetches DWARF debug info files automatically from cache.nixos.org, based on the build ID embedded in ELF executables and libraries." Nifty! ([See also](https://github.com/edolstra/dwarffs/blob/1f850df9c932acb95da2f31b576a8f6c7c188376/README.md#operation) re: `https://cache.nixos.org/debuginfo/<build-ID>`)


----

`[001]`: [TODO: Finish write-up from these semi-reversed order notes.]

* (via "Help" button on `https://hydra.nixos.org/build/217292666`):

    ```bash
    $ nix-env -i /nix/store/bi68an831k754lycara7i5dylhmk13i4-wezterm-20230408-112425-69ae8472 --option binary-caches https://cache.nixos.org
    ```

* "Build products" section

* "Summary" tab

* via `https://hydra.nixos.org/job/nixos/trunk-combined/nixpkgs.wezterm.x86_64-linux#tabs-status`

  \+ via "Latest successful build" on "links" on `https://hydra.nixos.org/job/nixos/trunk-combined/nixpkgs.wezterm.x86_64-linux#tabs-links`

  \+ via `https://hydra.nixos.org/job/nixos/trunk-combined/nixpkgs.wezterm.x86_64-linux/latest`

  \+ via search for `wezterm` on `https://search.nixos.org/packages?channel=unstable&type=packages&query=wezterm`

  \+ via invisible "▾▾▾ Show more package details ▾▾▾" link under package description

  \+ via "`x86_64-linux`" link under "Platforms" heading leads to `https://hydra.nixos.org/job/nixos/trunk-combined/nixpkgs.wezterm.x86_64-linux`

* TODO: `.nar.xz` a.k.a. "Nix archive (NAR) files"

----

`[002]`:

 * Second example, using `openvdb` (*the* most difficult project I've tried to build in recent memory--especially because I only wanted Nanovdb-related tools):

```
https://hydra.nixos.org/job/nixos/trunk-combined/nixpkgs.openvdb.x86_64-linux

leads to:

https://hydra.nixos.org/job/nixos/trunk-combined/nixpkgs.openvdb.x86_64-linux/latest

which leads to (currently):

https://hydra.nixos.org/build/218287386

which has two "Build Products", being (via "Help" button action):

/nix/store/dplf3jbwd0yx3w1baarxb6zqvhh231n5-openvdb-10.0.1-dev

/nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1


The full raw logs are here: <https://hydra.nixos.org/log/fy9jrcl693rg27mp39kd4h69lz5ssc49-openvdb-10.0.1.drv>

also a "tail" of the log: <https://hydra.nixos.org/build/217626012/nixlog/1/tail>

Where the last steps include:

-- Set runtime path of "/nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1/bin/vdb_print" to "/nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1//nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1/lib"
post-installation fixup
Moving /nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1/lib/cmake to /nix/store/dplf3jbwd0yx3w1baarxb6zqvhh231n5-openvdb-10.0.1-dev/lib/cmake
shrinking RPATHs of ELF executables and libraries in /nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1
shrinking /nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1/lib/libopenvdb.so.10.0.1
shrinking /nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1/bin/vdb_print
checking for references to /build/ in /nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1...
patching script interpreter paths in /nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1
stripping (with command strip and flags -S -p) in  /nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1/lib /nix/store/07294x7raxxqcvjhp7276i75l0rma68y-openvdb-10.0.1/bin
shrinking RPATHs of ELF executables and libraries in /nix/store/dplf3jbwd0yx3w1baarxb6zqvhh231n5-openvdb-10.0.1-dev
checking for references to /build/ in /nix/store/dplf3jbwd0yx3w1baarxb6zqvhh231n5-openvdb-10.0.1-dev...
patching script interpreter paths in /nix/store/dplf3jbwd0yx3w1baarxb6zqvhh231n5-openvdb-10.0.1-dev

which shows the elf patching that nix is doing, some of which I may wish to modify.

Aha, there is apparently a JSON API: <https://github.com/NixOS/hydra/blob/082495e34e094cae1eb49dbfc5648938e23c6355/README.md#json-api>

which links to:

https://github.com/NixOS/hydra/blob/082495e34e094cae1eb49dbfc5648938e23c6355/hydra-api.yaml

https://editor.swagger.io/?url=https://raw.githubusercontent.com/NixOS/hydra/master/hydra-api.yaml

tool: https://github.com/nlewo/hydra-cli


curl -X 'GET' \
  'https://hydra.nixos.org/build/218287386' \
  -H 'accept: application/json'


curl -X 'GET' 'https://hydra.nixos.org/build/218287386' -H 'accept: application/json'


So, I got a Python script as far as downloading the `.nar` files to tmp & "manually" decompressed them & used Rust nar tool to `ls` the contents.

Then I confirmed/noticed that it's seemingly not configured to build any of the nanovdb functionality anyway: <https://github.com/NixOS/nixpkgs/blob/549a132b51de555fed118d56c78580df889f3a12/pkgs/development/libraries/openvdb/default.nix#L21> lol *sigh*


```
