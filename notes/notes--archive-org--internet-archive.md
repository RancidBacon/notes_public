# Notes: Archive.org / Internet Archive

Notes related to using `archive.org`/"Internet Archive" services (e.g. Wayback
Machine) & tools (e.g. APIs, SDKs).

_[Page creation prompted by yet again wanting to download some things from the
Archive "properly" rather than via hacky URL manipulation and now finding myself
buried in yet another tab avalanche in a rabbit hole unrelated to the warren of
rabbit holes I was in already...]_


## Overview

>>>
Note: It is very much *not* obvious when to use *which* tool/API; nor, even, to
know *what* is current; nor, even, *where* to find current documentation or if
documentation even exists. This is probably why I tend to fall back to hacky URL
manipulation...
I mention this not so much as a complaint but to forewarn you. :)
>>>


### Who?

When it comes to Tools/APIs, to a certain degree these are separate
"organizational entities" but at different times they overlap/intersect:

 * Internet Archive

 * Wayback Machine

 * Open Library


### What?

Sometimes the tools or APIs are specific to Internet Archive and other times
they may be standards used for other services also:

 *  International Image Interoperability Framework (IIIF)


### How?

Here are some repositories:

 * <https://github.com/jjjake/internetarchive>

   > "A Python and Command-Line Interface to Archive.org"

   Notes:

    * The "binaries" are not *"native"* binaries.

       * A Python interpreter of a minimum version is required to be installed.

       * The minimum Python version required appears to be not clearly
         documented[^fn-ia-py-min].

          * _Update:_ It's more accurate to say "not clearly documented
            *for the binaries*": <https://archive.org/developers/internetarchive/installation.html#binaries>

            I eventually discovered the prequisitive of "Python 3.9 or later"
            *is* actually listed *twice* on that page but the direct links to
            the binaries section skip where that information is stated--the
            binaries section just says "a supported version of Python 3".

       * Changes in minimum Python version required appear not to be
         announced[^fn-ia-py-min-change] & may change at
         [any release](https://github.com/jjjake/internetarchive/releases).

       * As of early 2025 the minimum version appears to Python v3.9.0.

         (e.g. [`ia` version v5.2.1](https://github.com/jjjake/internetarchive/releases/tag/v5.2.1)
         requires at least Python v3.9.0 to be installed.)

       * People are instructed to directly download the "binary" from:
         <https://archive.org/download/ia-pex/ia>

         The associated "details" page is: <https://archive.org/details/ia-pex/>

         Multiple links on the "details" page are outdated, e.g.:

          * This linked page leads to an "404 Documentation page not found"
            error message (there is a "review" mentioning this):

            > "Documentation for "ia" is available at: `https://internetarchive.readthedocs.org/en/latest/cli.html`"

          * The link to the repo is outdated but *does* seem to automatically redirect:

            > "Github repository: `https://github.com/jjjake/ia-wrapper`"

         The page *does* point to the directory listing of all binaries:

         > "All binaries: [archive.org/download/ia-pex](https://archive.org/download/ia-pex)"

    * Repo states documentation is here: <https://archive.org/developers/internetarchive/>

       * In February 2025 latest release is v5.2.1, documentation site states
         documentation is for `Release v3.6.0.dev1`.

         (Note: **`3.x.y`** _not_ **`5.x.y`**.)

         Issue is presumably related to documentation generation, e.g.:

          * `5.3.0.dev1` set in [`internetarchive/__version__.py`
            ](https://github.com/jjjake/internetarchive/blob/6cee9869158607f6b781454b8e33e676d5c11bb2/internetarchive/__version__.py):

            ```python
            __version__ = '5.3.0.dev1'
            ```

          * `__version__` imported in [`docs/source/conf.py#L19-L25`
            ](https://github.com/jjjake/internetarchive/blob/6cee9869158607f6b781454b8e33e676d5c11bb2/docs/source/conf.py#L19-L25)
            & used in [`docs/source/conf.py#L62-L69`
            ](https://github.com/jjjake/internetarchive/blob/6cee9869158607f6b781454b8e33e676d5c11bb2/docs/source/conf.py#L62-L69):

            ```python
            # [...]

            import internetarchive
            from internetarchive import __version__


            # If extensions (or modules to document with autodoc) are in another directory,
            # add these directories to sys.path here. If the directory is relative to the
            # documentation root, use os.path.abspath to make it absolute, like shown here.
            sys.path.insert(0, os.path.abspath('../../'))

            # [...]

            # The version info for the project you're documenting, acts as replacement for
            # |version| and |release|, also used in various other places throughout the
            # built documents.
            #
            # The short X.Y version.
            version = __version__
            # The full version, including alpha/beta/rc tags.
            release = version

            # [...]
            ```

            Perhaps an out of date, locally installed version of
            `internetarchive` is being imported unintentionally due to the
            import occurring before the system path is modified?


          * `version` used in [`docs/source/index.rst`
             ](https://github.com/jjjake/internetarchive/blob/6cee9869158607f6b781454b8e33e676d5c11bb2/docs/source/index.rst?plain=1#L9):

            ```rst
            Release v\ |version|. (:ref:`Installation <install>`)
            ```

       * The above version issue was *particularly* confusing because the
         navigation path I followed was:

          1. Start from [`README`](https://github.com/jjjake/internetarchive?tab=readme-ov-file#a-python-and-command-line-interface-to-archiveorg).

          2. Click "Python 3" badge to <https://pypi.org/project/internetarchive>.

          3. Observe large "internetarchive 5.2.1" at top of page.

          4. Scroll & see link in "[Documentation
             ](https://pypi.org/project/internetarchive/#documentation)" section

             >>>
             Documentation is available at <https://archive.org/services/docs/api/internetarchive>.
             >>>

          5. Click link to <https://archive.org/services/docs/api/internetarchive>.

          6. Observe "Release v3.6.0.dev1" text at top of page.

          7. Be confused because I hadn't noticed the incorrect old version
             previously & wonder if I'd ended up on another *different*
             documentation site.

          8. Some how *not* notice the "Release v3.6.0.dev1" text again by the
             time I start to write this up and forget how I got to the incorrect
             page due to *not* noticing "**`3.x.y`** is _not_ **`5.x.y`**" above. :D

       * Latest version in "Release History" is v5.2.0:
         <https://archive.org/developers/internetarchive/updates.html#release-history>


[^fn-ia-py-min]:
    There appear to multiple places that some "minimum version" is specified
    which don't appear to all be updated in lock step:

     * <https://github.com/jjjake/internetarchive/blob/6cee9869158607f6b781454b8e33e676d5c11bb2/setup.cfg#L33>

     * <https://github.com/jjjake/internetarchive/blob/6cee9869158607f6b781454b8e33e676d5c11bb2/setup.cfg#L80>

     * <https://github.com/jjjake/internetarchive/blob/6cee9869158607f6b781454b8e33e676d5c11bb2/.github/workflows/tox.yml#L10>

     * <https://github.com/jjjake/internetarchive/blob/6cee9869158607f6b781454b8e33e676d5c11bb2/tox.ini#L2>

     * <https://github.com/jjjake/internetarchive/blob/6cee9869158607f6b781454b8e33e676d5c11bb2/pyproject.toml#L3>


[^fn-ia-py-min-change]:
    e.g.:

     * <https://github.com/jjjake/internetarchive/pull/671>

     * <https://github.com/jjjake/internetarchive/commit/e55c285f9436944583394b4ecdbbba4154cd2962>


 * <https://github.com/internetarchive/openlibrary-client>

   > "Python Client Library for the Archive.org OpenLibrary API"

 * <https://github.com/internetarchive/openlibrary> (server?)

 * <https://github.com/internetarchive/iiif/>:

   >>>
   iiif.archive.org
   The Internet Archive's official [IIIF 3.0 Image & Presentation API](https://iiif.io/)
   service.
   >>>

   Note:

   > "This service replaces the `iiif.archivelab.org` unofficial labs API."

   See also: [`https://github.com/internetarchive/iiif/blob/main/README.md#testing`](https://github.com/internetarchive/iiif/blob/ec7ebcef6d5ee192d5848902454928af651dd685/README.md#testing)



## Tasks

### Download individual book pages

For my purposes, I wanted to download either a single page or a range of pages
from scans of magazines--which were up to ~300 pages in size--rather than
download the entire file.

An example: <https://archive.org/details/edn-1986_05_15/page/147/mode/1up>


#### via IIIF API



#### via `ia` CLI ("Command-Line Interface") tool

**TL;DR:** You can't. This is not the tool you're looking for.

[TODO]

Related repo issues & forum posts & repos:

 * "Download nested content from zip files · Issue #424 · jjjake/internetarchive":
   <https://github.com/jjjake/internetarchive/issues/424>

   Especially [`internetarchive/issues/424#issuecomment-912047000`
   ](https://github.com/jjjake/internetarchive/issues/424#issuecomment-912047000):

   >>>
   `view-archive.php` was designed to be used in the browser for downloading a file
   or two, and is not really designed to be used programmatically. I could see the
   argument for an API that supported this being useful, but I think for most use
   cases using the browser to download a few files or downloading the whole ZIP is
   sufficient.
   [...]
   >>>

   The comment just before that suggested manually constructing the URL would
   probably be the most pragmatic approach (but wouldn't work if the nested
   filename is unknown), e.g.:

   >>>
   ```
   https://archive.org/download/<identifier>/<file>[/<nested-file>]
   ```
   >>>

   While not *ideal*, the `view_archive.php` table output should at least be
   pretty scraping friendly.


 * "Internet Archive Forums: Re: Poor PDF quality" (Apr 7, 2010):
   <https://archive.org/post/301273/poor-pdf-quality>

   >>>
   _The full-book PDFs are intended to be a convenience more than a high-quality
   record of the item._ In order to obtain a good compression ratio we had to make
   several compromises, and you are exactly correct that those compromises work to
   the detriment of photographs. [...]

   Pages may be viewed individually via the Read Online link (the "BookReader").
   When you zoom to 100%, what you're seeing is the full resolution of our master
   jpeg2000 images. URLs for specific pages and zoom levels can be bookmarked or
   otherwise saved. You can also extract the images themselves, without the
   BookReader skin, via URLs like:

   `http://{server}.us.archive.org/BookReader/BookReaderImages.php?zip={path to jp2 zip}&file={file path within the jp2 zip}&scale={n}`

   [...]
   >>>

   I'll note the statement "what you're seeing is the full resolution of our
   master jpeg2000 images" _doesn't_ state the image is of the same *quality*...


_[Aside: In the back of my mind I've thought about investigating how often PDFs
on archive.org are "Optimized"--particularly if generated by archive.org as a
derived item._

```bash
$ pdfinfo /<path>/edn-1986_05_15.pdf
Title:          edn-1986_05_15
Subject:        electronics
Keywords:       https://archive.org/details/edn-1986_05_15
Creator:        Internet Archive
Producer:       Internet Archive PDF 1.4.16; including mupdf and pymupdf/skimage
CreationDate:   Fri Aug 12 04:36:44 2022 NZST
[...]
Tagged:         yes
[...]
Pages:          284
[...]
File size:      164913742 bytes
Optimized:      no
PDF version:    1.5
```

_Alas, not promising with a sample size of one... :D :/_

_But perhaps not a huge deal since I could presumably re-create what they do to
construct the derived searchable text PDFs by just downloading the individual
images. Nothing like re-inventing the wheel! :p ]_

_[Aside: Oh, hey, look at that... :D ]_

 * "internetarchive/archive-pdf-tools: Fast PDF generation and compression. Deals with millions of pages daily.":
   <https://github.com/internetarchive/archive-pdf-tools>


 * "Downloading a single page image only · Issue #188 · internetarchive/openlibrary-client":
   <https://github.com/internetarchive/openlibrary-client/issues/188>

   Note: In *`openlibrary-client`* repo.


 * <https://archive.org/developers/ias3.html#how-this-is-different-from-normal-s3>:

   >>>
   **How this is different from normal S3**
   [...]
   > * HTTP 1.1 Range headers are ignored (also copy range headers for multipart).
   [...]
   >>>

   :/

   Also:

    * <https://archive.org/developers/ias3.html#fast-get-downloads>


 * _Update:_ Okay, I *really* need to stop believing what I read on the
   internet... *especially* when it's archive.org documentation!?!? :D :/

   Why? Cos, you know, oh, _**no particular reason**_:

   ```bash
   $ curl --head 'https://archive.org/download/edn-1986_05_15/edn-1986_05_15.cbr'
   HTTP/2 302
   server: nginx/1.24.0
   date: Mon, 03 Mar 2025 06:00:52 GMT
   content-type: text/html; charset=UTF-8
   location: https://dn720001.ca.archive.org/0/items/edn-1986_05_15/edn-1986_05_15.cbr
   accept-ranges: bytes
   [...]
   ```

   _\*zoom enhance\*_

   ```
   accept-ranges: bytes
   ```

   Certainly not because of _**range requests**_; or, like, anything like that:

   ```bash
   $ curl --location --range 0-3 'https://archive.org/download/edn-1986_05_15/edn-1986_05_15.cbr' | xxd

     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
     0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
   100     4  100     4    0     0      1      0  0:00:04  0:00:02  0:00:02     6

   00000000: 5261 7221                                Rar!
   ```

   Certainly nothing that would make a person go "Rar!" or anything... Well,
   apart from _that_, I _guess_.

   Hey, does a `.cbr` file go "Rar!" at its _tail_ too..?

   ```bash
   $ curl --location --range 16427117800- 'https://archive.org/download/edn-1986_05_15/edn-1986_05_15.cbr' | xxd | head

     % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                    Dload  Upload   Total   Spent    Left  Speed
     0     0    0     0    0     0      0      0 --:--:--  0:00:01 --:--:--     0
   100    22  100    22    0     0      4      0  0:00:05  0:00:05 --:--:--     6

   00000000: f88a d023 3316 4fe1 89e9 17c5 d691 1d77  ...#3.O........w
   00000010: 5651 0305 0400                           VQ....
   ```

   Oh, apparently not. Disappointing, seems a bit of a waste of a palindrome... :p

   What do you mean, `view_archive.php`? Never heard of it!

   But have you wanted to download like a single `.jp2` file from an archive.org
   `.zip` file? Or, like even a few at once?

   Nah, pretty weird use case, right?

   But... if by chance you _did_ want to do such a thing, you could _totally_ use
   ~~[Template Downloader for Godot](https://rancidbacon.itch.io/template-downloader-for-godot)~~,
   umm, I mean, use **[Zip Carver](https://rancidbacon.itch.io/zip-carver)** to do that!

   (Okay, so it doesn't _currently_ automatically handle the redirect from the
   permalink URL to the non-permalink URL but it _does_ work when supplied with
   the non-permalink URL.)

   Alternatively you can probably use [`zipdump.py`](https://github.com/nlitsme/zipdump)[^fn-zipdump-webdump]
   (untested for this specific use case) which is what I used for downloading
   partial `.zip`-format files before I made "Zip Carver"/"Template Downloader
   for Godot". :)

   [TODO: Insert screenshot. :) ]

   _\*sigh\*_

   But how?!

   BitTorrent(TM)[^fn-bittorrent-tm], of course!

   Well, indirectly, at least...

   [TODO]


   Also, just BTW:

    * On the topic of reading `.cbr` & RAR[^fn-cbr-rar]. (See footnote.)

    * On topic of downloading single file from multi-file
      torrent[^fn-torrent-single-file]. (See footnote.)

    * On topic of downloading single file from an *archive* (e.g.
      `.rar`/`.cbr`/`.zip`/`.cbz`) nested within a multi-file
      torrent[^fn-torrent-single-file-nested]. (See footnote.)


[^fn-bittorrent-tm]:
    See:

     * "Archive BitTorrents – Internet Archive Help Center":
       <https://help.archive.org/help/archive-bittorrents/>


[^fn-zipdump-webdump]:
    Hadn't noticed and/or forgot the existence of companion
    [`webdump.py`](https://github.com/nlitsme/zipdump/blob/master/webdump.py)
    tool which is for "hexdumping a section of a large online file" (i.e.
    instead of `curl` range request + `xxd` combo).

    e.g. _\*cough\*_:

    ```bash
    $ python3 /<path>/zipdump/webdump.py -o 0 -l 128 'https://archive.org/download/edn-1986_05_15/edn-1986_05_15.cbr'
    00000000: 52 61 72 21 1a 07 01 00 b2 ff cd 43 19 01 05 15   Rar!.......C....
    00000010: 0c 14 01 03 fd e6 bb c0 b7 80 80 80 00 f9 d8 bd   ................
    00000020: c0 b7 80 80 80 00 1a 0b 15 1f 5c 02 03 2e 99 84   ..........\.....
    00000030: e9 9b 80 00 00 94 d1 e0 aa 80 00 ff 83 02 80 53   ...............S
    00000040: 01 17 65 64 6e 2d 31 39 38 36 5f 30 35 5f 31 35   ..edn-1986_05_15
    00000050: 2e 30 30 31 2e 74 69 66 66 22 02 00 09 b8 59 55   .001.tiff"....YU
    00000060: 95 b5 16 c6 34 00 7a 6a 83 8f 91 1f 1f 3e 82 39   ....4.zj.....>.9
    00000070: 33 55 2d 98 35 02 a4 9d 05 ae 3d 1a 0a 03 13 9f   3U-.5.....=.....
    ```

    (Although, on further use, it seems that using `webdump.py` is _significantly_
    slower than using `curl`.)

    Also, realising `webdump.py` was slower _also_ lead me to discover[^fn-https-slower]
    that using `https` rather than `http` was _also_ _significantly_ slower! e.g.

    ```bash
    $ curl --location --range 0-128 'https://archive.org/download/edn-1986_05_15/edn-1986_05_15.cbr' | xxd

      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
      0     0    0     0    0     0      0      0 --:--:--  0:00:04 --:--:--     0
    100   129  100   129    0     0     16      0  0:00:08  0:00:07  0:00:01    70

    [...]

    $ curl --location --range 0-128 'http://archive.org/download/edn-1986_05_15/edn-1986_05_15.cbr' | xxd

      % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                     Dload  Upload   Total   Spent    Left  Speed
      0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0
    100   162  100   162    0     0    202      0 --:--:-- --:--:-- --:--:--   202
    100   129  100   129    0     0     73      0  0:00:01  0:00:01 --:--:--   247

    [...]
    ```

[^fn-https-slower]:
    The comparison was largely prompted by a `astreamfs` docs comment about
    `httpfs`[^fn-httpfs]:

    >>>
    [...] doing a range request for each read request received.
    That won't fly, especially with https!
    >>>

    So, if your "security posture" can accommodate such insecurity use of `http`
    may provide signfiicant performance uplift. :)


[^fn-cbr-rar]:
    Links related to reading RAR file format (which `.cbr` files are):

    * <https://en.wikipedia.org/wiki/Comic_book_archive>

    * <https://github.com/libarchive/libarchive/blob/master/libarchive/archive_read_support_format_rar.c>

       * <https://github.com/libarchive/libarchive/wiki/Examples#list-contents-of-archive-with-custom-read-functions>

       * <https://github.com/libarchive/libarchive/wiki/Examples#a-note-about-the-skip-callback>

       * <https://github.com/libarchive/libarchive/wiki/Examples#a-note-about-the-seek-callback>

    * <https://formats.kaitai.io/rar/>


[^fn-torrent-single-file]:
    Links related to downloading single file from multi-file torrent:

     * In theory many--if not most--BitTorrent clients support downloading a single file
       from a multi-file torrent.

       However, it appears there are at least two aspects of `archive.org` hosted
       `.torrent` files that seem to cause issues with this functionality:

        1. "Web seeds"

            * "WebSeed - HTTP/FTP Seeding (GetRight style)":
              <https://www.bittorrent.org/beps/bep_0019.html>

            * "Support for GetRight-style webseeding, eg on archive.org?":
              <https://forum.kde.org/viewtopic.php%3Ff=235&t=108448.html#p252786>

        2. "Padding files"

            * "BEP-0047, padding files and broken webseeds - Help and Support - Tixati":
              <https://forum.tixati.com/support/7212>

        3. Potentially also:

           "Re-generation of `.torrent` files" and/or "Redirection" and/or "IP Banning".

            * <https://archive.org/post/395011/torrent-webseeding-the-bencode-editor>

            * "Archive.org torrents stucks and not reach 100% ·
              Issue #19747 · qbittorrent/qBittorrent":
              <https://github.com/qbittorrent/qBittorrent/issues/19747>

               * "fails to follow redirect to webseed file ·
                 Issue #7682 · arvidn/libtorrent":
                 <https://github.com/arvidn/libtorrent/issues/7682>

            * "Not completing HTTP seeded torrents from archive.org ·
              Issue #15193 · qbittorrent/qBittorrent":
              <https://github.com/qbittorrent/qBittorrent/issues/15193#issuecomment-880849692>

               * "Make smart ban optional · Issue #22076 · qbittorrent/qBittorrent":
                 <https://github.com/qbittorrent/qBittorrent/issues/22076>

     * Other links:

        * <https://stackoverflow.com/questions/39997621/why-my-get-request-to-a-torrent-tracker-doesnt-work>

     * [TODO]


[^fn-torrent-single-file-nested]:
    Links related to downloading single file from an *archive* (e.g.
    `.rar`/`.cbr`/`.zip`/`.cbz`) nested within a multi-file torrent:

     * From a brief search I didn't immediately find a BitTorrent client that
       seemed to mention being able to "intelligently" download only a single
       file from *within* an archive file that was within a torrent (e.g. a
       single page image `edn-1986_05_15.001.tiff` within `edn-1986_05_15.cbr`
       within the multi-file torrent described by `https://archive.org/download/edn-1986_05_15/edn-1986_05_15_archive.torrent`).

     * I _did_ learn about the existence of a project called `httpfs`[^fn-httpfs]
       which might be interesting to investigate further in relation to this for
       torrents that also provide "web seeds".

     * Other:

        * <https://stackoverflow.com/questions/22099468/getting-zip-rar-structure-without-full-downloading#22112707>

          Commenter mentions developing related functionality with a Usenet backend:

          >>>
          [...] That and other RAR related code you can find in the [`pyReScene`
          ](https://bitbucket.org/Gfy/pyrescene) project.
          Doing the same from HTTP will be a lot easier because you can ignore
          yEnc encoding stuff and can be more precise in selecting byte ranges.
          >>>

           * <https://bitbucket.org/Gfy/pyrescene> -- of course, no longer exists.

             Wayback archives:

              * <https://web.archive.org/web/20190118053832/https://bitbucket.org/Gfy/pyrescene/src/>

              * <https://web.archive.org/web/20190118060106/https://bitbucket.org/Gfy/pyrescene>

            PyPi: <https://pypi.org/project/pyReScene/>

            Perhaps this is its new home:

             * <https://github.com/srrDB/pyrescene>

             * See also: [`/pyrescene/rerar`](https://github.com/srrDB/pyrescene/tree/1b6d931abc28b7d0c0aefaed5f0e6c698664fe5f/rerar)

                * Particularly: [`/pyrescene/rerar/rarfile.py`](https://github.com/srrDB/pyrescene/blob/1b6d931abc28b7d0c0aefaed5f0e6c698664fe5f/rerar/rarfile.py)

             * Maybe _don't_ read these: [`/pyrescene/dev-docs/rar`](https://github.com/srrDB/pyrescene/tree/1b6d931abc28b7d0c0aefaed5f0e6c698664fe5f/dev-docs/rar) :)



[^fn-httpfs]:
    I learned of the existence of `httpfs` (an "HTTP-backed virtual filesystem")
    from this [answer to "Is it possible to download just part of a ZIP archive (e.g. one file)?"](https://stackoverflow.com/questions/8543214/is-it-possible-to-download-just-part-of-a-zip-archive-e-g-one-file#15321699).

    (Which, if it still works, would be have been useful to have known about
    earlier... :D For both this & other projects.)

     * `httpfs` website: <https://httpfs.sourceforge.net/> (as of
       "(26. of August 2006)" :) )

       Mentions [via](https://httpfs.sourceforge.net/techinfo.htm):

       >>>
       The simplest version of httpfs doesn't follow redirections or allows basic
       authorization. It also doesn't exploit keep alive connections.
       >>>

       Related links:

        * <https://manpages.ubuntu.com/manpages/bionic/man1/httpfs2.1.html>
          <https://launchpad.net/ubuntu/bionic/+package/httpfs2>

        * <https://manpages.ubuntu.com/manpages/noble/en/man1/httpfs2.1.html>
          <https://launchpad.net/ubuntu/noble/+package/httpfs2>

        * "jbd/httpfs2: Unofficial httpfs repository for http://httpfs.sourceforge.net/":
          <https://github.com/jbd/httpfs2>


    Other potential FUSE-based alternatives
    ([via](https://github.com/libfuse/libfuse/wiki/Filesystems)):

     * `astreamfs`: <https://gitlab.com/BylonAkila/astreamfs>

       >>>
       [...]
       There was already an httpfs implementation, but it was very naive:
       doing a range request for each read request received.
       That won't fly, especially with https! It was also not following location,
       or not doing clever things like keeping locations for a given time.
       [...]
       >>>

       Related links:

        * <https://launchpad.net/~alainb06/+archive/ubuntu/astreamfs/+packages>

     * `fuse-bgzip`: <https://github.com/sahlberg/fuse-bgzip>

       >>>
       [..] read-only overlay filesystem to transparently uncompress
       indexed GZIP files. [...]
       >>>

       (Because I've noticed other magazine scans which have their original TIFF[^fn-tiff-decryption]
       scans in `.tar.gz` files which don't normally have any file index--although
       reading further "indexed GZIP files" seem to require a non-standard tool
       to generate a separate index file & not work with `.tar.gz`-style archives
       anyway?)

     * `ratarmount`: <https://github.com/mxmlnkn/ratarmount>

       I've actually used `ratarmount` previously for something else.

       Related links:

        * <https://github.com/mxmlnkn/ratarmount#remote-files>

          Shows `.rar` syntax example:

          ```
          http[s]://hostname[:port]/path-to/archive.rar
          ```

          States `.rar` support is via `fsspec`: <https://github.com/fsspec/filesystem_spec>

           * <https://filesystem-spec.readthedocs.io/en/latest/api.html#built-in-implementations>

           * See also:

             * <https://filesystem-spec.readthedocs.io/en/latest/api.html#other-known-implementations>

             * <https://github.com/moradology/httpfs-sync>



[^fn-tiff-decryption]:
    Oh, hey, I think I just found the helpful person who uploaded *both* the
    "Electronics Australia" *and* "EDN" magazine collections wherein I found the
    [R&S "Logic Analysis System LAS"](notes--rohde-schwarz--vintage--logic-analyzer.md)
    references! :D

    And:

     a. Wow, it's quite the operation!

     b. The person has _also_ taken the time to document over time the process
        used including some reasoning behind various decisions involved:

         * "[Scanning Workflow Notes for Summer 2021/2022 - Scanning - decryption.net.au
            ](https://blog.decryption.net.au/t/scanning-workflow-notes-for-summer-2021-2022/70)"

         * "[My 2022 Scanning Setup and Workflow - Scanning - decryption.net.au
            ](https://blog.decryption.net.au/t/my-2022-scanning-setup-and-workflow/76)"

         * "[How to download 2400 items off the Internet Archive - decryption.net.au
            ](https://blog.decryption.net.au/t/how-to-download-2400-items-off-the-internet-archive/99)"

    **Thanks [@decryption](https://decryption.net.au/)!** :)

    Somewhat amusingly, it was the "[How to download 2400 items off the Internet Archive
    ](https://blog.decryption.net.au/t/how-to-download-2400-items-off-the-internet-archive/99)"
    article I initially encountered on the site--presumably because my search
    was for some combination of "`.cbr`" & "torrent":

    >>>
    All I want are the original scanned images I uploaded. For this item, that
    is `dr_dobbs_journal-1987_08.cbr` - a Comic Book RAR file (view [this post
    ](https://blog.decryption.net.au/t/scanning-workflow-notes-for-summer-2021-2022/70)
    as to why I upload in Comic Book RAR/ZIP format). Some of my uploads
    are CBR, some are CBZ and some are GZIP files, from before I realised I
    should have been uploading CBR/CBZ.
    >>>

    (The "this post" link seems broken/missing on the actual page but in the
    above quote I've linked it to the post I found that seems to include the
    relevant information.)

    So, now I know more of the context about the formats used in the uploads! :)
    My vote is certainly for `.cbz` over `.cbr` for ease of access to individual
    original TIFF files but I'm not sure if there's a significant difference in
    compression between the two variants with the settings used--and *I'm* not
    the person having to upload or store _all_ the data! :D

    (I suspect my recent discovery that `archive.org` `http` URLs seem to be
    _significantly_ faster might have been useful to know when trying to download
    those 2400 files! _\*ouch\*_ :/ )



#### via hacky URL manipulation

##### with BookReader URL

 1. Right-click on page image and choose e.g. "Open Image in New Tab"
    (in Firefox) from context menu.

 2. This (currently) results in tab with a URL of this form:

    ```
    https://ia801500.us.archive.org/BookReader/BookReaderImages.php?zip=/9/items/edn-1986_05_15/edn-1986_05_15_jp2.zip&file=edn-1986_05_15_jp2/edn-1986_05_15_0148.jp2&id=edn-1986_05_15&scale=4&rotate=0
    ```

    (Note: These URLs are *not* permalinks--due to inclusion of the
    server-specific `ia000000`-style subdomain.)

 3. If you want the non-scaled image version you can either zoom in within the
    BookReader viewer before opening the image in the tab or change the value
    of `scale` in the URL to `0` (actually, using a value of `1` or just removing
    `&scale=0` seem equivalent):

    ```
    https://ia801500.us.archive.org/BookReader/BookReaderImages.php?zip=/9/items/edn-1986_05_15/edn-1986_05_15_jp2.zip&file=edn-1986_05_15_jp2/edn-1986_05_15_0148.jp2&id=edn-1986_05_15&scale=0&rotate=0
    ```

 4. If you want to download a different page you can edit the page number
    referenced in the `.jp2` file name. You'll note that there may be a
    difference between the page number displayed in the BookReader viewer UI/URL
    & the number in the actual filename, e.g. page 147 in the viewer is stored
    in a file with 148 in the file name--this is because Computer Science.

    Also note that the file returned is `.jpg` not `.jp2`.

 5. You can either save the image within the browser or use a CLI tool like
    `wget` to retrieve the file--note that in latter case you'll want to quote
    the filename so that the `&` characters don't confuse your command shell,
    e.g.:

    ```bash
    wget --content-disposition 'https://ia801500.us.archive.org/BookReader/BookReaderImages.php?zip=/9/items/edn-1986_05_15/edn-1986_05_15_jp2.zip&file=edn-1986_05_15_jp2/edn-1986_05_15_0148.jp2&id=edn-1986_05_15&scale=0&rotate=0'
    ```

    The addition of the `--content-disposition` option to the `wget` invocation
    means you'll get a more useful filename than the default (e.g. `edn-1986_05_15_0148.jpg`
    rather than `BookReaderImages.php?zip=%2F9%2Fitems%2Fedn-1986_05_15%2Fedn-1986_05_15_jp2.zip&file=edn-1986_05_15_jp2%2Fedn-1986_05_15_0148.jp2&id=edn-1986_05_15&scale=4&rotate=0`.
    :D )


##### with Download URL

An alternative approach (which will work e.g. if the BookReader viewer can't
display the item; or, if you want to download the `.jp2` file format) is to
use/manipulate an item's "Download" URL instead:

 1. On the item's "Details" page (e.g. `https://archive.org/details/edn-1986_05_15`),
    within the right-hand side "DOWNLOAD OPTIONS" section, click the "SHOW ALL"
    link to open the associated "Download"/"directory listing" overview, e.g.
    `https://archive.org/download/edn-1986_05_15`.

 2. On the `/download/` page files list you'll see a Zip archive file named with
    a suffix & extension of `_jp2.zip`:

    ```
    [...]
    edn-1986_05_15_jp2.zip (View Contents)  11-Aug-2022 13:05 1.3G
    [...]
    ```

    There will be a clickable link with the text `View Contents` next to the
    file name, opening the `View Contents` link will lead to a (again,
    *non-permalink*) URL of this form:

    ```
    https://ia801500.us.archive.org/view_archive.php?archive=/9/items/edn-1986_05_15/edn-1986_05_15_jp2.zip
    ```

    _(Side note: In *this* particular case there is *also* a `.cbr` archive file
    ("Comic Book RAR") in the list which also has a "View Contents" link--you
    could use that link if downloading the ~90MB per page TIFF files is more
    your style: :D_

    ```
    [...]
    edn-1986_05_15.cbr (View Contents)  11-Aug-2022 11:11  15.3G
    [...]
    ```

    _Unfortunately, when I tried to retrieve a TIFF file I had repeated status
    errors of this form:_

    ```
    $ wget --content-disposition 'https://archive.org/download/edn-1986_05_15/edn-1986_05_15.cbr/edn-1986_05_15.148.tiff'
    [...]
    504 Gateway Time-out
    [...]
    ```

    _(I *suspect* the time-out errors are because it's attempting to extract the
    TIFF file from the ~~1.5GB~~ **15.3GB** `.cbr` file in a very inefficient
    manner... :D :/ )_

    _Yeah, I tried again but requesting the *1st* page instead of the *148th* and
    it "suceeded":_

    ```bash
    $ wget --content-disposition 'https://archive.org/download/edn-1986_05_15/edn-1986_05_15.cbr/edn-1986_05_15.001.tiff'
    ```
    _So... I hope *you* don't need the last page of anything... :D )_

    This "View Archive" URL will display a listing of all the files within the
    Zip archive similar to this:

    ```
    listing of edn-1986_05_15_jp2.zip
    file	as jpg	timestamp	size
    edn-1986_05_15_jp2/		2022-08-11 13:04
    edn-1986_05_15_jp2/edn-1986_05_15_0000.jp2	jpg	2022-08-11 12:40	6568877
    [...]
    edn-1986_05_15_jp2/edn-1986_05_15_0148.jp2	jpg	2022-08-11 12:53	5646011
    [...]
    ```

    Note that each line of the file listing has *two* links within it (which, I
    just noticed, *are* permalinks because that don't include a server-specific
    subdomain):

    1. `edn-1986_05_15_jp2/edn-1986_05_15_0148.jp2`: Link text with a file path
       ending in `.jp2` that will lead to the `.jp2` format file being downloaded:

       ```
       https://archive.org/download/edn-1986_05_15/edn-1986_05_15_jp2.zip/edn-1986_05_15_jp2%2Fedn-1986_05_15_0148.jp2
       ```

       This link can be downloaded via the browser or a CLI tool like `wget`:

       ```bash
        wget --content-disposition 'https://archive.org/download/edn-1986_05_15/edn-1986_05_15_jp2.zip/edn-1986_05_15_jp2%2Fedn-1986_05_15_0148.jp2'
       ```

    2. `jpg`: Link text with *only* `jpg`, that will lead to a `.jpg` format
       file being downloaded:

       ```
       https://archive.org/download/edn-1986_05_15/edn-1986_05_15_jp2.zip/edn-1986_05_15_jp2%2Fedn-1986_05_15_0148.jp2&ext=jpg
       ```
       This link can be downloaded via the browser or a CLI tool like `wget`:

       ```bash
       wget --content-disposition 'https://archive.org/download/edn-1986_05_15/edn-1986_05_15_jp2.zip/edn-1986_05_15_jp2%2Fedn-1986_05_15_0148.jp2&ext=jpg'
       ```

       _NOTE: I've just noticed that `.jpg` image downloaded via this method is
       *not* the same size as the one downloaded via BookReader URL
       manipulation--it seems to download the equivalent of `scale=2` rather
       than `scale=1`. (I wonder if that's intentional or a bug?)_
       _(But it *also* seems to be a smaller file size too?)_

       _Update: The smaller file size appears to be due to use of a different
       "Quality" level setting: 75 vs 90._

       _(I suspected it might be quality setting-related but determining for
       sure required trying an unusually large number of CLI tools until I found
       one that would actually *display* that information. Use of `identify`
       command from `ImageMagick` with `-verbose` option was required,
       specifically[^fn-fd-redir]:_

       ```bash
       $ diff -u <(identify -verbose edn-1986_05_15_0148.jpg.1) <(identify -verbose edn-1986_05_15_0148.jpg.3)
       --- /dev/fd/63  2025-03-03 07:10:33.935916971 +1300
       +++ /dev/fd/62  2025-03-03 07:10:33.939916946 +1300
       @@ -1,4 +1,4 @@
       -Image: edn-1986_05_15_0148.jpg.1
       +Image: edn-1986_05_15_0148.jpg.3
          Format: JPEG (Joint Photographic Experts Group JFIF format)
          Mime type: image/jpeg
          Class: DirectClass
       [...]
          Dispose: Undefined
          Iterations: 0
          Compression: JPEG
       -  Quality: 90
       +  Quality: 75
          Orientation: Undefined
       [...]
          Artifacts:
       -    filename: edn-1986_05_15_0148.jpg.1
       +    filename: edn-1986_05_15_0148.jpg.3
            verbose: true
          Tainted: False
       -  Filesize: 2.783MB
       +  Filesize: 1.617MB
          Number pixels: 7.639M
       [...]
       ```

       _Where:_

        * `edn-1986_05_15_0148.jpg.1` = `https://archive.org/download/edn-1986_05_15/edn-1986_05_15_jp2.zip/edn-1986_05_15_jp2%2Fedn-1986_05_15_0148.jp2&ext=jpg`

        * `edn-1986_05_15_0148.jpg.3` = `https://ia801500.us.archive.org/BookReader/BookReaderImages.php?zip=/9/items/edn-1986_05_15/edn-1986_05_15_jp2.zip&file=edn-1986_05_15_jp2/edn-1986_05_15_0148.jp2&id=edn-1986_05_15&scale=2&rotate=0`

       _That's a kinda less than ideal combination of interactions... :/_

       _(Also, I'm starting to have a sense of déjà vu about this...again.))_

    Now, where were we? :D

    Oh, yeah, so, at least with this method, if you're using a browser you've
    got a *list* of file page number-based URLs you can save/open directly if
    you prefer that to something on the CLI.



[^fn-fd-redir]: Remembered "file descriptor redirection" incantation without looking it up! :D



## Resources

### Tools

#### Unsorted

 * "internetarchive/archive-hocr-tools: Efficient hOCR tooling":
   <https://github.com/internetarchive/archive-hocr-tools>

 * "internetarchive/jpg241: Author & serve single progressive JPEG image which can be served as two different qualities "Two for One" :)":
   <https://github.com/internetarchive/jpg241>

 * "internetarchive/tocky: [WIP] Extract structured table of contents data from digitized books":
   <https://github.com/internetarchive/tocky>

 * "internetarchive/infogami":
   <https://github.com/internetarchive/infogami>


### Documentation

 * IAS3:

    * https://github.com/vmbrasseur/IAS3API/blob/master/downloading.md

    * https://github.com/vmbrasseur/IAS3API/blob/master/specialfiles.md

    * https://github.com/vmbrasseur/IAS3API/blob/master/appendices/othertools.md

    * https://archive.org/developers/ias3.html


 * <https://archive.org/developers/items.html#archival-urls>
