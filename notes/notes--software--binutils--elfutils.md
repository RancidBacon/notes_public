# Notes: Software : `binutils` & `elfutils`

_(see also: [Notes: ld-linux & Dynamic Linker execution](notes--ld-linux--dynamic-linker.md))_


## Which tool to use: `nm`, `objdump` or `readelf`?

Various tools in the `binutils` package have significant overlap in
functionality and it's not always immediately obvious which tool to
choose for any particular task, so, here's some suggestions from the
related docs in terms of the differences & intended target audience:

 * [`nm`](https://sourceware.org/binutils/docs/binutils/nm.html)

   > _`nm` lists the symbols from object files [...]_

 * [`objdump`](https://sourceware.org/binutils/docs/binutils/objdump.html)

   > _`objdump` displays information about one or more object
   > files. [...] **This information is mostly useful to programmers
   > who are working on the compilation tools, as opposed to
   > programmers who just want their program to compile and work**._

 * [`readelf`](https://sourceware.org/binutils/docs/binutils/readelf.html)

   > _This program performs a similar function to `objdump` but it
   > goes into more detail and it exists independently of the BFD
   > library, so if there is a bug in BFD then readelf will not be
   > affected._

In addition, the [`elfutils`](https://sourceware.org/elfutils/)
package includes some "reimplementations" of `binutils` tools,
including:

 * `eu-nm`

 * `eu-objdump`

 * `eu-readelf`

Differences between the various tools may include defaults and/or the
format of output.

If I'm reaching for one of these tools it's generally for some low
level "spelunking" of executables/libraries for some reason or other
so I tend toward reaching for one of the `readelf` variants first.


## Useful CLI options

One aspect of all these tools that can be frustrating is remembering &
supplying all the CLI options/flags needed to get the level of detail
desired.
