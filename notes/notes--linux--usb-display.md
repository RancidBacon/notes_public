# Notes: Linux USB Display

Items related to either USB Displays (either implementating or
supporting) and/or other forms of implementing a "virtual" display
that will be recognized as an actual physical display for use by X11
etc.

(Semi-related to "DisplayLink" displays and related module that IIUC
was never merged due to... reasons.

See also: <https://github.com/DisplayLink/evdi>

(Even less related: <https://starkeblog.com/efi/video/2022/07/11/displaylink-usb-video-for-efi.html>))


## Generic USB Display (GUD)

 * <https://github.com/notro/gud/wiki>

    * Note: "27.01.2025: GUD is now without a maintainer ([ref](https://gitlab.freedesktop.org/drm/misc/kernel/-/commit/e2a81c0cd7de6cb063058be304b18f200c64802b))" :(

    * "Linux Host Driver" merged as of Linux v5.13, further updated v5.16.

       * <https://elixir.bootlin.com/linux/v6.13.1/source/drivers/gpu/drm/gud>

    * <https://github.com/notro/gud/wiki/Linux-Gadget-Driver>

    * <https://github.com/notro/gud/wiki/GUD-Protocol>

    * <https://github.com/notro/gud/wiki/Gadget-Implementations>:

       * Userspace Linux / via USB FunctionFS ("theoretical"): <https://elixir.bootlin.com/linux/latest/source/drivers/usb/gadget/function/f_fs.c>

          * "Rust implementation (WIP"): <https://github.com/samcday/gud-gadget/>

            Related:

             * <https://github.com/samcday/gud-gadget/tree/main/gadget>

             * <https://github.com/samcday/gud-gadget/tree/main/drm>

             * <https://crates.io/crates/usb-gadget>

       * Android: "It would have been really cool to see old tablets and phones reused as GUD touch displays!"

       * "Implementations not updated to the final protocol": [STM32F769I-DISCO](https://github.com/notro/gud/tree/cf690e44b36582fa0b77a63fca9d383c125ac66c/STM32F769I-DISCO)

         See also: <https://github.com/hackerspace/libopencm3-gf32v-examples/tree/lr/gd32v/examples/gd32v/f103/polos-alef/usb-display>

       * "Use old phone as a second display: USB GUD with postmarketOS": <https://gist.github.com/ZenithalHourlyRate/220186818bae434386e80c62ee1bc7bc>

    * <https://github.com/notro/gud/wiki/Raspberry-Pi>:

      ```
      There are two images for the Raspberry Pi both supporting Pi Zero and Pi 4:

      gud-raspberry-lite
      Fast booting read-only image with no access to the SD card

      gud-raspberry
      Writeable filesystem, USB over IP and some debugging tools
      ```

    * <https://github.com/notro/gud/wiki/Tips&Tricks>

    * See also: <https://github.com/notro/gud/wiki/Other-USB-Display-Adapters>
