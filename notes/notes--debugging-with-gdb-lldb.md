## Notes: Debugging with `gdb` and/or `lldb`

### `gdb`

#### `<optimized out>` & `<value optimized out>`

I either forgot or hadn't realised that `gdb`'s message(s) about something being "`optimized out`" are (or may be) *location* dependent! So, it's not necessarily that there's no way to view the value, just that it's not possible *at the current point of execution* (e.g. maybe because the register where the value was stored has been overwritten).

So, for example, if you want to see the value of an function argument partway through the execution of the function, it may not be possible *but* (assuming restarting the process is acceptable) instead you could, e.g. set a breakpoint at the start of the function & get access to it there.

Related:

 * > "_The message that a value was optimized out doesn't mean that a variable_
   > _got optimized away. **It means that the information needed to determine the**_
   > _**variable's value is no longer available**, possibly because it was overwritten_
   > _or reused to hold a different variable._"
   >
   > -- via <https://stackoverflow.com/questions/72830042/how-does-gdb-know-optimized-out-local-variable-values#72835466>

 * > "_...if a **variable is optimized to be stored only in a register rather than the stack, and then the register it was in gets overwritten**, then it shows as `<optimized out>`..._
   >
   > _...it tends to happen that at **the start of the function** you can see the variable value, but **then at the end** it becomes `<optimized out>`._
   >
   > _One typical case which we often are interested in of this is that of the function arguments themselves..._"
   >
   > -- via <https://stackoverflow.com/questions/5497855/what-does-value-optimized-out-mean-in-gdb#60552533>

   (The above link has both more detailed explanation & complete example worth reading.)


[TODO: Is the difference in wording significant? (i.e. "`<optimized out>`" vs "`<value optimized out>`", I think is what I meant?)]


### `lldb`


### `rr`

 * <https://github.com/rr-debugger/rr/wiki/Using-rr-in-an-IDE>


### Function tracing / Program execution flow

#### With `rr`


----


## Uncategorized

 * "[Printing .lto_priv symbols in GDB](https://www.tablix.org/~avian/blog/archives/2020/02/printing_lto_priv_symbols_in_gdb/)"

   The key lesson being using quotes to avoid gdb interpreting the `.` incorrectly.

   However in my case the numeric suffix didn't seem to be shown (may be related to them being listed as "Non-debugging symbols" (which apparently means they're from the library's exported symbol table?).

   In my case appending `.0` worked but I needed to also cast it to the correct type--which I guess makes sense if it's from the export symbol table...

   Anyway, here's something that worked:

    ```gdb
    (rr) info variables ungot_char*
    All variables matching regular expression "ungot_char*":

    File src/libespeak-ng/readclause.c:
    61:     static int ungot_char;
    59:     static int ungot_char2;

    File src/libespeak-ng/tr_languages.c:
    61:     static int ungot_char;
    59:     static int ungot_char2;

    Non-debugging symbols:
    0x00007f894f8c8244  ungot_char2.lto_priv
    0x00007f894f8c8248  ungot_char.lto_priv
    (rr) print 'ungot_char.lto_priv.0'
    'ungot_char.lto_priv' has unknown type; cast it to its declared type
    (rr) print (int) 'ungot_char.lto_priv.0'
    $1 = 32
    ```

    Unfortunately that didn't appear to work with `dprintf` so I had to refer to the memory addresses directly:

    ```gdb
    (rr) dprintf text_decoder_getc, ">> ungots: '%c' '%c'\n", (int) 0x00007f894f8c8248, (int) *0x00007f894f8c8244
    ```

    Also, speaking of `dprintf`, see above for example use. :D

    Related:

     * "[[PATCH]: 2eb3980b66 rtos: Support looking up .lto_priv.0 appended to symbol name | OpenOCD - Open On-Chip Debugger](https://sourceforge.net/p/openocd/mailman/message/37703887/)"

     * "[rtos: Support looking up .lto_priv.0 appended to symbol name (I03580b45) · Gerrit Code Review](https://review.openocd.org/c/openocd/+/7179)

     * "[GDB: GNU debugger — Victor Stinner's Notes 1.0 documentation](https://vstinner.readthedocs.io/gdb.html)" -- describes some `gcc` symbol suffixes, e.g.:

       * "`.constprop.0` stands for 'constant propagation'..."

       * "`isra.3` ... 'interprocedural scalar replacement' ..."

     * "[What does the GCC function suffix `.constprop` mean?](https://stackoverflow.com/questions/14796686/what-does-the-gcc-function-suffix-constprop-mean)"

     * "[What does the GCC function suffix `isra` mean?](https://stackoverflow.com/questions/13963150/what-does-the-gcc-function-suffix-isra-mean)"

 * "[`iovisor/bcc`: BCC - Tools for BPF-based Linux IO analysis, networking, monitoring, and more](https://github.com/iovisor/bcc)"

 * "[Compiler considerations — The Linux Kernel 5.7.0+ documentation](https://people.redhat.com/~jolawren/klp-compiler-notes/livepatch/compiler-considerations.html#gcc-function-suffixes)" -- includes descriptions & more details about further suffixes (e.g. `.code`, `.cold`, `.part` (partial inlining), `.constprop`) & other "Interprocedural optimization (IPA)"-related information.
