## Notes: Procedural Generation

### Text

 * <https://bml-lang.org/> -- "Blur Markup Language" / "Write text that changes"

    * <https://bml-lang.org/docs/reference/js-library/>

    * <https://bml-lang.org/docs/tools/cli/>


## AI/ML Related

### Music

#### Links to potentially read

 * "Generating Music Tracks with Unified Representation and Diffusion Framework (ai-muzic.github.io)": <https://news.ycombinator.com/item?id=36038945>
