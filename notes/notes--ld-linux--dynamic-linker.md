# Notes: `ld-linux` & Dynamic Linker execution

_(see also: [Notes: A `ld-linux`, `RPATH` & `RUNPATH` saga.](notes--ld-linux--rpath--runpath--saga.md))_

## ELF File Format

It was not immediately clear to me why the ELF file format seems to
have overlapping/duplicate/extraneous information within it nor
when/why each is used.

But when I (eventually `:D`) read the specification it nicely
summarises the intent is to provide "parallel views" of the file's
content (from the perspective of the code/programmer currently
consuming the file's content) depending on whether it is being
_linked_ or _loaded_.

See "File Format" section in
["Tool Interface Standard (TIS) Executable and Linking Format (ELF) Specification Version 1.2" (PDF)](https://refspecs.linuxfoundation.org/elf/elf.pdf)
(emphasis mine):

>
> _Object files participate in **program linking (building a program)** and
> **program execution (running a program)**. For convenience and
> efficiency, **the object file format provides parallel views of a
> file's contents**, reflecting the differing needs of these
> activities. Figure 1-1 shows an object file's organization._
>
> ![Comparison of "Linking View" vs "Execution View" of an ELF file.](assets/figure-1-1-object-file-format--tis-elf-spec.png)
>

For completeness, here is a somewhat lossy[^lossy-table] [^other-links] version of
the above figure in text table form (HTML copied from
"[System V Application Binary Interface - DRAFT - 24 April 2001](https://refspecs.linuxbase.org/elf/gabi4+/ch4.intro.html#file_format)"):

[^lossy-table]:
    The text table is "lossy" in the sense that it doesn't preserve
    the nuance of the original figure which (subtly) showed that the
    "sections" (of the linking view) are contained within the
    "segments" of the execution view. (The "parallel"-ness/equivalence
    of the _contents_ of the sections/segments is I think a key point
    to understand.)

[^other-links]:
    The [Wikipedia "Executable and Linkable Format" page](https://en.wikipedia.org/wiki/Executable_and_Linkable_Format)
    _does_ have a custom SVG figure which preserves some of the nuance
    of "parallel"-ness between the section and segment concepts but
    IMO the diagram is harder to "read" (in part because it doesn't
    explicitly show the two "views" side-by-side nor does it
    explicitly label the sections and segments as such):
    [`Elf-layout--en.svg`](https://commons.wikimedia.org/wiki/File:Elf-layout--en.svg)

    Another (directly linkable) source for the figure in image form is
    the ["File Format" section of the "Oracle Solaris 11.4 Linkers and
    Libraries Guide"](https://docs.oracle.com/en/operating-systems/solaris/oracle-solaris/11.4/linkers-libraries/file-format.html).

----

>>>

<b>Figure 4-1: Object File Format</b>
<p>
<table>
<tr><td width="250">
<table border=1 cellspacing=0>
<caption align=bottom><b>Linking View</b></caption>
<tr><td align=center>ELF Header</td></tr>
<tr><td align=center>Program header table<br><i>optional</i></td></tr>
<tr><td align=center>Section 1</td></tr>
<tr><td align=center>...</td></tr>
<tr><td align=center>Section n</td></tr>
<tr><td align=center>...</td></tr>
<tr><td align=center>Section header table<br><i>required</i></td></tr>
</table>
</td>
<td>
<table border=1 cellspacing=0>
<caption align=bottom><b>Execution View</b></caption>
<tr><td align=center>ELF Header</td></tr>
<tr><td align=center>Program header table<br><i>required</i></td></tr>
<tr><td align=center>Segment 1<br></td></tr>
<tr><td align=center>Segment 2<br></td></tr>
<tr><td align=center>Segment 3<br></td></tr>
<tr><td align=center>...</td></tr>
<tr><td align=center>Section header table<br><i>optional</i></td></tr>
</table>
</td>
</tr>
</table>

>>>

----

It occurred to me that one of the aspects that leads to a lack of
clarity/"obviousness" when understanding the ELF format is that while
there's an "obvious" connection between the concepts of "sections" and
the "*section* header table" (i..e. it's in the name :) ) there isn't
an "obvious" connection between the concepts of "segments" and the
"program header table" (because it's not called the "*segment* header
table" or the "program *segment* header table" or the "program header
& loadable *segments* table" or the "program header & *segment* table"
or the "*segment* & program header table" or...--presumably for
reasons of semantic "accuracy" and/or "catchiness" :D ).

----

More extracts from <https://refspecs.linuxfoundation.org/elf/elf.pdf>:

>
> * `.bss`
>
>    * _This section holds uninitialized data that contribute to the
>      program's memory image. By definition, **the system initializes
>      the data with zeros when the program begins to run.** The
>      section occupies no file space, as indicated by the section
>      type, `SHT_NOBITS`._
>
> [...]
>
> * `.data` and `.data1`
>
>    * _These sections hold **initialized data** that contribute to
>      the program's memory image._
>
> [...]
>
> * `.rodata` and `.rodata1`
>
>    * _These sections hold read-only data that typically contribute
>      to a non-writable segment in the process image._
>


>
> **_Program Header_**
>
> _An executable or shared object file's program header table is an
> array of structures, each describing a segment or other information
> the system needs to prepare the program for execution. **An object
> file segment contains one or more sections.** Program headers are
> meaningful only for executable and shared object files._
>

>
> * `PT_LOAD`
>
>    * _The array element specifies a loadable segment, described by
>      `p_filesz` and `p_memsz`. The bytes from the file are mapped to
>      the beginning of the memory segment. **If the segment's memory
>      size (`p_memsz`) is larger than the file size (`p_filesz`), the
>      "extra" bytes are defined to hold the value 0** and to follow
>      the segment's initialized area. The file size may not be larger
>      than the memory size.  Loadable segment entries in the program
>      header table appear in ascending order, sorted on the `p_vaddr`
>      member._
>


>
> **Segment Contents**
>
> _An object file segment comprises one or more sections, though this
> fact is transparent to the program header. Whether the file segment
> holds one or many sections also is immaterial to program
> loading. [...]_
>


>
> _The end of the data segment requires special handling for
> uninitialized data, which the system defines to begin with zero
> values. Thus if a file's last data page includes information not in
> the logical memory page, the extraneous data must be set to zero,
> not the unknown contents of the executable file. "Impurities'' in
> the other three pages are not logically part of the process image;
> whether the system expunges them is unspecified._
>

----

The more up-to-date ELF-related specification pages include:

 * [Section _"II. Executable And Linking Format (ELF)"_
    of _"Linux Standard Base Core Specification, Generic Part"_ (LSB v5.0)
   ](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/elf-generic.html)

    * [Section "10.2. [ELF] Sections" (lol)
       of "Chapter 10. Object Format"
      ](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/sections.html)

      (The listed section types are as defined in the _"System V ABI"_
      and the _"System V ABI Update"_ documents.)

      I note that while it states this in relation to `SHT_DYNSYM`:

      > _This section holds a minimal set of symbols adequate for
      > dynamic linking. See also `SHT_SYMTAB`. Currently, an object
      > file may have either a section of `SHT_SYMTAB` type or a
      > section of `SHT_DYNSYM` type, but not both. This restriction
      > may be relaxed in the future._

      and this in relation to `SHT_SYMTAB`:

      > _Currently, an object file may have either a section of
      > `SHT_SYMTAB` type or a section of `SHT_DYNSYM` type, but not
      > both. This restriction may be relaxed in the
      > future. Typically, SHT_SYMTAB provides symbols for link
      > editing, though it may also be used for dynamic linking. As a
      > complete symbol table, it may contain many symbols unnecessary
      > for dynamic linking._

      ...that the binary executable for a common FLOSS project that
      I'm currently looking at does, in fact, have both. (I haven't
      investigated why this is the case.)

    * [Section "10.3. Special [ELF] Sections"
       of "Chapter 10. Object Format"
      ](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/specialsections.html)

      (The listed special section types are as defined in the _"System
      V ABI"_ and the _"System V ABI Update"_ documents.)

      These include:

      > * `.bss`
      >
      >   _This section holds data that contributes to the program's memory
      >   image. The program may treat this data as uninitialized. However,
      >   **the system shall initialize this data with zeroes** when the
      >   program begins to run. The section occupies no file space, as
      >   indicated by the section type, `SHT_NOBITS`._

      > * `.data`
      >
      >   _This section holds initialized data that contribute to the
      >   program's memory image._

      > * `.interp`
      >
      >   _This section holds the path name of a program
      >   interpreter. **If the file has a loadable segment that
      >   includes relocation, the sections' attributes will include
      >   the `SHF_ALLOC` bit;** otherwise, that bit will be off. See
      >   Chapter 5 of System V ABI Update for more information._

      > * `.rodata`
      >
      >   _This section holds read-only data that typically contribute
      >   to a non-writable segment in the process image. See `Program
      >   Header' in Chapter 5 of System V ABI Update for more
      >   information._

      > * `.tbss`
      >
      >   _This section holds uninitialized thread-local data that
      >   contribute to the program's memory image. By definition, the
      >   system initializes the data with zeros when the data is
      >   instantiated for each new execution flow. The section
      >   occupies no file space, as indicated by the section type,
      >   `SHT_NOBITS`. [...]_

      > * `.tdata`
      >
      >   _This section holds initialized thread-local data that
      >   contributes to the program's memory image. A copy of its
      >   contents is instantiated by the system for each new
      >   execution flow. [...]_

      > * `.text`
      >
      >   _This section holds the `text', or executable instructions,
      >   of a program._

      With regard to the thread-related sections, as of `glibc` v2.35,
      `ld-linux.so` does *not* contain either section but `libc.so.6`
      contains both `.tbss` & `.tdata`.

      (I assume this is related to `ld-linux.so` starting up single
      threaded, at least? Need to verify if situation has changed with
      later releases.)

      The LSB also has sub-section "[10.3.1.2. Additional Special [ELF]
      Sections](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/specialsections.html#AEN953)"
      which lists optional "additional special sections" none of which
      appear to need any special `SHT_NOBITS`-like handling.

      One of the additional sections may receive some "special
      handling" but it's only a concern for the dynamic linker not the
      initial "process image" creation by the system/kernel (which is
      my main area of interest currently):

      > * `.data.rel.ro`
      >
      >   _This section holds initialized data that contribute to the
      >   program's memory image. This section may be made read-only
      >   after relocations have been applied._

      This additional section type may also receive the same special
      handling:

      > * `.got.plt`
      >
      >   _This section holds the read-only portion of the GLobal
      >   Offset Table. This section may be made read-only after
      >   relocations have been applied._

      These three additional section types relate to versioned symbol
      handling (described in [section _"10.7. Symbol Versioning"_](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/symversion.html)):

      > * `.gnu.version`
      >
      >   _This section contains the Symbol Version Table.
      >   See [Section 10.7.2](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/symversion.html#SYMVERTBL)._
      >
      > * `.gnu.version_d`
      >
      >   _This section contains the Version Definitions.
      >   See [Section 10.7.3](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/symversion.html#SYMVERDEFS)._
      >
      > * `.gnu.version_r`
      >
      >   _This section contains the Version Requirements.
      >   See [Section 10.7.4](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/symversion.html#SYMVERRQMTS)._

      One detail to note (for "reasons"--in part related to use of `@`
      vs. `@@`[^current-symbol-versions]) is:

      > _The special section `.gnu.version` which has a section type
      > of `SHT_GNU_versym` shall contain the Symbol Version
      > Table. **This section shall have the same number of entries as
      > the Dynamic Symbol Table in the `.dynsym` section**._

      (Also, semi-related thought...[^aside-table-mods])

      This additional section type is ABI-compatibility related:

      > * `.note.ABI-tag`
      >
      >   _Specify ABI details. See Section 10.8._


[^current-symbol-versions]:

    With regard to use of `@` vs. `@@` when a version is specified,
    see `VER_NEED_CURRENT` & `VER_DEF_CURRENT` in `elf.h` which, IIUC
    (and I very much may not UC :D as I've not yet read the relevant
    spec, so this is more just trying to note the observed output of
    various ELF-parsing related tools, in order to follow-up on
    it...), *may* indicate that:

     * if the "version number" is `1` then it means use the "current"
       version (i.e. the "most recent" symbol version available); or,

     * if the "version number" is `2` use the specific symbol version
       named by the "version string" contained in the related
       `vda_name` field]

    It's somewhat non-obvious, so, by way of example, for `libc.so.6`:

    ```bash
    $ eu-readelf --all /<path>/libc.so.6 # (from glibc v2.35 on Ubuntu)
    [...]

    Dynamic segment contains 29 entries:
     Addr: 0x0000000000218bc0  Offset: 0x217bc0  Link to section: [ 6] '.dynstr'
      Type              Value
      NEEDED            Shared library: [ld-linux-x86-64.so.2]
      SONAME            Library soname: [libc.so.6]
      INIT_ARRAY        0x0000000000215900
    [...]
      VERDEF            0x000000000001fd08
      VERDEFNUM         37
      FLAGS             STATIC_TLS
      VERNEED           0x0000000000020230
      VERNEEDNUM        1
      VERSYM            0x000000000001e566
      RELACOUNT         1197
      NULL
    [...]

    Symbol table [ 5] '.dynsym' contains 3024 entries:
     1 local symbol  String table: [ 6] '.dynstr'
      Num:            Value   Size Type    Bind   Vis          Ndx Name
    [...]
        4: 0000000000000000      0 OBJECT  GLOBAL DEFAULT    UNDEF __libc_enable_secure@GLIBC_PRIVATE (38)
        5: 0000000000000000      0 FUNC    GLOBAL DEFAULT    UNDEF _dl_deallocate_tls@GLIBC_PRIVATE (38)
        6: 0000000000000000      0 FUNC    GLOBAL DEFAULT    UNDEF __tls_get_addr@GLIBC_2.3 (39)
        7: 0000000000000000      0 OBJECT  GLOBAL DEFAULT    UNDEF __libc_stack_end@GLIBC_2.2.5 (40)
        8: 0000000000000000      0 OBJECT  GLOBAL DEFAULT    UNDEF _rtld_global_ro@GLIBC_PRIVATE (38)
    [...]
       18: 000000000009b800    293 FUNC    GLOBAL DEFAULT       15 pthread_setname_np@GLIBC_2.12
       19: 000000000009d9b0     94 FUNC    GLOBAL DEFAULT       15 thrd_create@@GLIBC_2.34
       20: 000000000009b800    293 FUNC    GLOBAL DEFAULT       15 pthread_setname_np@@GLIBC_2.34
       21: 000000000009d9b0     94 FUNC    GLOBAL DEFAULT       15 thrd_create@GLIBC_2.28
    [...]
      498: 0000000000152000     74 FUNC    GLOBAL DEFAULT       15 gai_cancel@GLIBC_2.2.5
      499: 0000000000127ba0    205 FUNC    WEAK   DEFAULT       15 sendto@@GLIBC_2.2.5
      500: 00000000002219e8      8 OBJECT  GLOBAL DEFAULT       35 argp_program_version_hook@@GLIBC_2.2.5
      501: 000000000011ec20     37 FUNC    WEAK   DEFAULT       15 munmap@@GLIBC_2.2.5
      502: 0000000000127cb0     37 FUNC    WEAK   DEFAULT       15 shutdown@@GLIBC_2.2.5
    [...]
      695: 0000000000029f40     12 FUNC    WEAK   DEFAULT       15 gnu_get_libc_version@@GLIBC_2.2.5
    [...]

    Version symbols section [ 7] '.gnu.version' contains 3024 entries:
     Addr: 0x000000000001e566  Offset: 0x01e566  Link to section: [ 5] '.dynsym'
        0:   0 *local*                       38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)
        2:  38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)   38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)
        4:  38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)   38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)
        6:  39 GLIBC_2.3(ld-linux-x86-64.so.2)   40 GLIBC_2.2.5(ld-linux-x86-64.so.2)
        8:  38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)   38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)
       10:  38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)   38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)
       12:  38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)   38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)
       14:  38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)   38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)
       16:  38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)   38 GLIBC_PRIVATE(ld-linux-x86-64.so.2)
       18:  16hGLIBC_2.12                    35 GLIBC_2.34
       20:  35 GLIBC_2.34                    29hGLIBC_2.28
       22:   2 GLIBC_2.2.5                    2 GLIBC_2.2.5
       24:  35 GLIBC_2.34                    37 GLIBC_PRIVATE
       26:   2hGLIBC_2.2.5                   37 GLIBC_PRIVATE
       28:  35 GLIBC_2.34                     2 GLIBC_2.2.5
       30:  24 GLIBC_2.23                     2hGLIBC_2.2.5
       32:  37 GLIBC_PRIVATE                  2 GLIBC_2.2.5
       34:  37 GLIBC_PRIVATE                  2 GLIBC_2.2.5
       36:   2 GLIBC_2.2.5                    2 GLIBC_2.2.5
       38:   2 GLIBC_2.2.5                    2 GLIBC_2.2.5
       40:   2hGLIBC_2.2.5                   28 GLIBC_2.27
    [...]

    Version definition section [ 8] '.gnu.version_d' contains 37 entries:
     Addr: 0x000000000001fd08  Offset: 0x01fd08  Link to section: [ 6] '.dynstr'
      000000: Version: 1  Flags: BASE   Index: 1  Cnt: 1  Name: libc.so.6
      0x001c: Version: 1  Flags: none  Index: 2  Cnt: 1  Name: GLIBC_2.2.5
      0x0038: Version: 1  Flags: none  Index: 3  Cnt: 2  Name: GLIBC_2.2.6
      0x0054: Parent 1: GLIBC_2.2.5
      0x005c: Version: 1  Flags: none  Index: 4  Cnt: 2  Name: GLIBC_2.3
      0x0078: Parent 1: GLIBC_2.2.6
      0x0080: Version: 1  Flags: none  Index: 5  Cnt: 2  Name: GLIBC_2.3.2
    [...]
      0x04dc: Version: 1  Flags: none  Index: 36  Cnt: 2  Name: GLIBC_2.35
      0x04f8: Parent 1: GLIBC_2.34
      0x0500: Version: 1  Flags: none  Index: 37  Cnt: 2  Name: GLIBC_PRIVATE
      0x051c: Parent 1: GLIBC_2.35

    Version needs section [ 9] '.gnu.version_r' contains 1 entry:
     Addr: 0x0000000000020230  Offset: 0x020230  Link to section: [ 6] '.dynstr'
      000000: Version: 1  File: ld-linux-x86-64.so.2  Cnt: 3
      0x0010: Name: GLIBC_2.2.5  Flags: none  Version: 40
      0x0020: Name: GLIBC_2.3  Flags: none  Version: 39
      0x0030: Name: GLIBC_PRIVATE  Flags: none  Version: 38

    String section [6] '.dynstr' contains 32533 bytes at offset 0x16650:
      [     0]
      [     1]  __pthread_mutex_destroy
    [...]
      [  7d61]  readdir
      [  7d69]  ld-linux-x86-64.so.2
      [  7d7e]  libc.so.6
      [  7d88]  GLIBC_2.2.5
      [  7d94]  GLIBC_2.2.6
    [...]
      [  7ef1]  GLIBC_2.34
      [  7efc]  GLIBC_2.35
      [  7f07]  GLIBC_PRIVATE

    [...]
    ```

    And, for `ld-linux-x86-64.so.2`:

    ```bash
    $ eu-readelf --all /<path>/ld-linux-x86-64.so.2 # (from glibc v2.35 on Ubuntu)
    [...]

    Dynamic segment contains 23 entries:
     Addr: 0x0000000000039e80  Offset: 0x038e80  Link to section: [ 6] '.dynstr'
      Type              Value
      SONAME            Library soname: [ld-linux-x86-64.so.2]
      HASH              0x00000000000002f0
    [...]
      RELAENT           24 (bytes)
      VERDEF            0x0000000000000c68
      VERDEFNUM         7
      VERSYM            0x0000000000000c12
      RELACOUNT         142
      NULL
    [...]

    Symbol table [ 5] '.dynsym' contains 40 entries:
     1 local symbol  String table: [ 6] '.dynstr'
      Num:            Value   Size Type    Bind   Vis          Ndx Name
        0: 0000000000000000      0 NOTYPE  LOCAL  DEFAULT    UNDEF
        1: 000000000000af60    723 FUNC    GLOBAL DEFAULT       13 _dl_rtld_di_serinfo@@GLIBC_PRIVATE
        2: 000000000001b750    449 FUNC    GLOBAL DEFAULT       13 _dl_audit_symbind_alt@@GLIBC_PRIVATE
        3: 000000000003b1d8      1 OBJECT  GLOBAL DEFAULT       23 __nptl_initial_report_events@@GLIBC_PRIVATE
        4: 0000000000000000      0 OBJECT  GLOBAL DEFAULT      ABS GLIBC_2.2.5@@GLIBC_2.2.5
        5: 0000000000039ae0    928 OBJECT  GLOBAL DEFAULT       18 _rtld_global_ro@@GLIBC_PRIVATE
        6: 00000000000147a0    711 FUNC    GLOBAL DEFAULT       13 _dl_allocate_tls@@GLIBC_PRIVATE
        7: 0000000000032fb0      4 OBJECT  GLOBAL DEFAULT       14 __rseq_flags@@GLIBC_2.35
        8: 000000000003a040   4304 OBJECT  GLOBAL DEFAULT       22 _rtld_global@@GLIBC_PRIVATE
        9: 00000000000144f0    673 FUNC    GLOBAL DEFAULT       13 _dl_allocate_tls_init@@GLIBC_PRIVATE
       10: 0000000000039a90      8 OBJECT  GLOBAL DEFAULT       18 __libc_stack_end@@GLIBC_2.2.5
    [...]
       23: 0000000000000000      0 OBJECT  GLOBAL DEFAULT      ABS GLIBC_PRIVATE@@GLIBC_PRIVATE
       24: 0000000000038ae8      8 OBJECT  GLOBAL DEFAULT       18 __rseq_offset@@GLIBC_2.35
       25: 000000000002b330      5 FUNC    GLOBAL DEFAULT       13 __rtld_version_placeholder@GLIBC_2.34
       26: 000000000000ff20    169 FUNC    GLOBAL DEFAULT       13 _dl_fatal_printf@@GLIBC_PRIVATE
       27: 0000000000000000      0 OBJECT  GLOBAL DEFAULT      ABS GLIBC_2.3@@GLIBC_2.3
       28: 0000000000000000      0 OBJECT  GLOBAL DEFAULT      ABS GLIBC_2.4@@GLIBC_2.4
       29: 0000000000038af0      4 OBJECT  GLOBAL DEFAULT       18 __rseq_size@@GLIBC_2.35
    [...]
       37: 000000000001b680    205 FUNC    GLOBAL DEFAULT       13 _dl_audit_preinit@@GLIBC_PRIVATE
       38: 0000000000000000      0 OBJECT  GLOBAL DEFAULT      ABS GLIBC_2.34@@GLIBC_2.34
       39: 0000000000000000      0 OBJECT  GLOBAL DEFAULT      ABS GLIBC_2.35@@GLIBC_2.35

    Version symbols section [ 7] '.gnu.version' contains 40 entries:
     Addr: 0x0000000000000c12  Offset: 0x000c12  Link to section: [ 5] '.dynsym'
        0:   0 *local*                        7 GLIBC_PRIVATE
        2:   7 GLIBC_PRIVATE                  7 GLIBC_PRIVATE
        4:   2 GLIBC_2.2.5                    7 GLIBC_PRIVATE
        6:   7 GLIBC_PRIVATE                  6 GLIBC_2.35
        8:   7 GLIBC_PRIVATE                  7 GLIBC_PRIVATE
       10:   2 GLIBC_2.2.5                    7 GLIBC_PRIVATE
       12:   7 GLIBC_PRIVATE                  7 GLIBC_PRIVATE
       14:   7 GLIBC_PRIVATE                  7 GLIBC_PRIVATE
       16:   2 GLIBC_2.2.5                    7 GLIBC_PRIVATE
       18:   3 GLIBC_2.3                      7 GLIBC_PRIVATE
       20:   7 GLIBC_PRIVATE                  7 GLIBC_PRIVATE
       22:   7 GLIBC_PRIVATE                  7 GLIBC_PRIVATE
       24:   6 GLIBC_2.35                     5hGLIBC_2.34
       26:   7 GLIBC_PRIVATE                  3 GLIBC_2.3
       28:   4 GLIBC_2.4                      6 GLIBC_2.35
       30:   7 GLIBC_PRIVATE                  7 GLIBC_PRIVATE
       32:   7 GLIBC_PRIVATE                  7 GLIBC_PRIVATE
       34:   2 GLIBC_2.2.5                    7 GLIBC_PRIVATE
       36:   7 GLIBC_PRIVATE                  7 GLIBC_PRIVATE
       38:   5 GLIBC_2.34                     6 GLIBC_2.35

    Version definition section [ 8] '.gnu.version_d' contains 7 entries:
     Addr: 0x0000000000000c68  Offset: 0x000c68  Link to section: [ 6] '.dynstr'
      000000: Version: 1  Flags: BASE   Index: 1  Cnt: 1  Name: ld-linux-x86-64.so.2
      0x001c: Version: 1  Flags: none  Index: 2  Cnt: 1  Name: GLIBC_2.2.5
      0x0038: Version: 1  Flags: none  Index: 3  Cnt: 2  Name: GLIBC_2.3
      0x0054: Parent 1: GLIBC_2.2.5
      0x005c: Version: 1  Flags: none  Index: 4  Cnt: 2  Name: GLIBC_2.4
      0x0078: Parent 1: GLIBC_2.3
      0x0080: Version: 1  Flags: none  Index: 5  Cnt: 2  Name: GLIBC_2.34
      0x009c: Parent 1: GLIBC_2.4
      0x00a4: Version: 1  Flags: none  Index: 6  Cnt: 2  Name: GLIBC_2.35
      0x00c0: Parent 1: GLIBC_2.34
      0x00c8: Version: 1  Flags: none  Index: 7  Cnt: 2  Name: GLIBC_PRIVATE
      0x00e4: Parent 1: GLIBC_2.35

    String section [6] '.dynstr' contains 705 bytes at offset 0x950:
      [     0]
      [     1]  _rtld_global
      [     e]  _dl_audit_preinit
    [...]
      [   258]  _dl_catch_error
      [   268]  ld-linux-x86-64.so.2
      [   27d]  GLIBC_2.2.5
      [   289]  GLIBC_2.3
      [   293]  GLIBC_2.4
      [   29d]  GLIBC_2.34
      [   2a8]  GLIBC_2.35
      [   2b3]  GLIBC_PRIVATE

    [...]
    ```

    Okay, I dunno... :D Back to read the spec, I guess. :)


[^aside-table-mods]:

    _[The thought turns out actually to be more an aside, so now it's
    a footnote. :D ]_

    A thought in relation to `RPATH`, `RUNPATH`, `NODEFLIB` &
    content+relative order of `NEEDED` entries, is that while the
    `.dynamic` section is loaded into memory it also can only(?)
    point to strings contained within `.dynstr` (also loaded into
    memory)...

    ...but, from looking at the results of `patchelf` moving
    sections around (e.g. into a new/last segment) within the section
    headers, it seems potentially do-able to...

    _\*pause\*_

    ...--because, IIUIC, then...

    _\*pause\*_

    ...no, actually, the existence of `dl_iterate_phdr()` & `PT_PHDR`
    presumably means the program headers need manipulation
    too/instead--because they may/do end up loaded in memory &
    accessed by the dynamic linker, at least...

    Anyway, where were we..? :D


----

via [_"How To Write Shared Libraries" a.k.a `dsohowto` (Version 4.1.2)_
by Ulrich Drepper](https://akkadia.org/drepper/dsohowto.pdf) (emphasis mine):

> _The size in the file can be smaller than the address space it takes
> up in memory. **The first `p_filesz` bytes of the memory region are
> initialized from the data of the segment in the file**, the
> difference is initialized with zero. This can be used to handle BSS
> sections, sections for uninitialized variables which are according
> to the C standard initialized with zero. Handling uninitialized
> variables this way has the advantage that the file size can be
> reduced since no initialization value has to be stored, no data has
> to be copied from disc to memory, and the memory provided by the OS
> via the `mmap` interface is already initialized with zero._

[TODO: Move this quote to more appropriate location in the text?]


Also from the same document with regard to the apparent reason for the
existence of the auxiliary vector (emphasis mine):

> _This auxiliary vector contains [...] values **which allow the
> dynamic linker to avoid several system calls**._


Also:

> _The two main differences are: it is possible to have more than one
> definition of a given symbol (the associated version must differ)
> and the application or DSO linked with the versioned DSO contains
> not only a list of the required version, but also records for each
> symbol which symbol version was used and from which DSO the
> definition came. At runtime this information can then be used to
> pick the right version from all the different versions of the same
> interface. The only requirement is that the API (headers, DSO used
> for linking, and documentation) is consistent. Every versioned DSO
> has at most one version of every API which can be used at
> link-time._


See also section "3.5 Handling Compatible Changes (GNU)", which is
probably the "best" explanation of the versioning situation.


Also (emphasis mine):

> _What remains to be explained is the use of `@` and `@@`. The symbol
> defined using `@@` is the default definition. There must be at most
> one. It is the version of the symbol used in all linker runs
> involving the DSO. **No symbol defined using `@` are ever considered
> by the linker.** These are the compatibility symbols which are
> considered only by the dynamic linker._

(This choice seems to be the underlying cause of the issue with "new
binaries on old systems".)


Also:

> _[...] The only requirement is that the interface the caller saw at
> compile time, also is the interface the linker finds when handling
> the relocatable object file. Since the relocatable object file does
> *not* contain the versioning information it is not possible to keep
> object files around and hope the right interface is picked by the
> linker. Symbol versioning only works for DSOs and executables. If it
> is necessary to reuse relocatable object files later, it is
> necessary to recreate the link environment the linker would have
> seen when the code was compiled. The header files (for C, and
> whatever other interface specification exists for other languages)
> and the DSOs which are used for linking together form the API. It is
> not possible to separate the two steps, compiling and linking. For
> this reason packaging systems, which distinguish between runtime and
> development packages, put the headers and linkable DSOs in one file,
> while the files needed at runtime are in another._


Also:

> _The `DT_RPATH` value is used first, before any other path,
> specifically before the path defined in the `LD_LIBRARY_PATH`
> environment variable. This is problematic since it does not allow
> the user to overwrite the value. Therefore `DT_RPATH` is
> deprecated. The introduction of the new variant, `DT_RUNPATH`,
> corrects this oversight by requiring the value is used after the
> path in `LD_LIBRARY_PATH`._

Too bad if it was the *user* who set the `DT_RPATH` value though, eh?


Also, this is publication date mentioned in references:

> [4] Ulrich Drepper, Red Hat, Inc., ELF Symbol Versioning, http://people.redhat.com/drepper/symbol-versioning, 1999.

----



### Versioned symbols

It turns out it's actually rather non-trivial to understand how
versioned symbols work, not least because of a comment[^vna_other-comment]
added to the file `elf/elf.h` in a nearly 30 year-old commit[^vna_other-commit]
that hasn't been updated in subsequent years and is no longer accurate
but has since propagated all over. :D

[_Update:_

It *also* turns out that the inaccurate `vna_other` comment
may have existed from some point *during* the development of the
standard and has been incorrect during the entire lifetime of the
standard. :D

Specifically, the "original" source of the structure definition ([via](https://github.com/freebsd/freebsd-src/commit/0eb88f20298d056bf09b52ec2d84d3662b8fd152)[^freebsd-commit-msg])
appears to be a document entitled "ELF Symbol Versioning" by Ulrich
Drepper, originally hosted at
`http://people.redhat.com/~drepper/symbol-versioning`
([2003 archived version
](https://web.archive.org/web/20030811054032/http://people.redhat.com/~drepper/symbol-versioning))
which now (2024) redirects to
<https://www.akkadia.org/drepper/symbol-versioning>
([2024 archived version
](https://web.archive.org/web/20241122153015/https://www.akkadia.org/drepper/symbol-versioning)).


[^freebsd-commit-msg]:

    With the commit message:

    ```
    Implement ELF symbol versioning using GNU semantics. This code aims

    to be compatible with symbol versioning support as implemented by
    GNU libc and documented by http://people.redhat.com/~drepper/symbol-versioning
    and LSB 3.0.

    Implement dlvsym() function to allow lookups for a specific version of
    a given symbol.
    ```


However, the _earliest_ version of this (ironically, _unversioned_ :D )
document that I found was in [an email entitled "ELF symbol
versioning with glibc 2.1 and later"
](https://lists.debian.org/lsb-spec/1999/12/msg00017.html)
([archived](https://web.archive.org/web/20240912163753/https://lists.debian.org/lsb-spec/1999/12/msg00017.html))
that seemingly "quotes" correspondence from Ulrich Drepper who
prefaced the "ELF Symbol Versioning" document with this remark[^ia-64-wg]:

> ```
>
> from Ulrich Drepper <drepper@cygnus.com> ...
>
> -----------------------------------------------------------------------
>
> This is the most complete description on symbol versioning I have.
> It's the same the ia64 working group got submitted by me.
>
> ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
> ELF Symbol Versioning
>
> [...]
>
> ```

And, sure enough, in the relevant section it states:

```
[...]

  The entries referenced to by the vn_aux elements are of this type:

typedef struct
{
  Elfxx_Word	vna_hash;		/* Hash value of dependency name */
  Elfxx_Half	vna_flags;		/* Dependency specific information */
  Elfxx_Half	vna_other;		/* Unused */
  Elfxx_Word	vna_name;		/* Dependency name string offset */
  Elfxx_Word	vna_next;		/* Offset in bytes to next vernaux
					   entry */
} Elfxx_Vernaux;

[...]

  vna_other
    Contains version index unique for the file which is used in the
    version symbol table.  If the highest bit (bit 15) is set this
    is a hidden symbol which cannot be referenced from outside the
    object.

[...]

```

So, it seems the contradiction has existed "from the beginning"!

Well, _that_ was an adventure...[^vna_other-more] [^symver-blurb] [^symver-solaris] :)


[^ia-64-wg]: FWIW more search results with: `"ia-64" "working group"`


[^vna_other-more]:

    Some other `vna_other` code references encountered along the way
    that aren't just copypasta (but also aren't informative):

     * <https://github.com/illumos/illumos-gate/blob/45744051679350ee063cdc366b66bee5223a11ea/usr/src/uts/common/sys/link.h#L409>

     * <https://github.com/illumos/illumos-gate/blob/45744051679350ee063cdc366b66bee5223a11ea/usr/src/boot/sys/sys/elf64.h#L248>

     * <https://github.com/freebsd/freebsd-src/commit/0eb88f20298d056bf09b52ec2d84d3662b8fd152#diff-5c7472e4df0cc5ecd2071bfa22a877c97a4c7fd6d2cc3d7cf02f6931e9fdc44bR203>

    Symbol version related `#defines` (not specifically `gnu`-related):

     * <https://github.com/illumos/illumos-gate/blob/45744051679350ee063cdc366b66bee5223a11ea/usr/src/boot/sys/sys/elf_common.h#L790-L807>

    This documentation/explanation about `#define` values with a
    `_NUM` suffix would be beneficial for the `glibc` include file
    also ([via](https://github.com/illumos/illumos-gate/blob/45744051679350ee063cdc366b66bee5223a11ea/usr/src/uts/common/sys/elf.h#L59-L61)):

    > ```
    >  *    "Enumerations" below use ...NUM as the number of
    >  *    values in the list.  It should be 1 greater than the
    >  *    highest "real" value.
    > ```


[^symver-blurb]:

    The intro "blurb" on <https://www.akkadia.org/drepper/> for the
    "Symbol Versioning" document mentions some additional historical
    context:

    > _For the glibc 2.1 release Eric Youngdale and I developed a powerful
    > symbol versioning scheme based on Sun's own versioning. Sun does not
    > have what makes this scheme so powerful: versioning on symbol level
    > with automatically selecting the right version. This short document
    > explains the basics._


[^symver-solaris]:

    Some semi-relevant Solaris-related links encountered include:

     * [`vd_version`](https://docs.oracle.com/en/operating-systems/solaris/oracle-solaris/11.4/linkers-libraries/version-definition-section.html#GUID-5B5F79BA-307F-4D31-B603-00237042F3D2__GUID-F222A295-B138-490F-9A48-970303EB6459)
       specifies `VER_DEF_NONE` (`0`) as "Invalid version" and
       `VER_DEF_CURRENT` (**`>=1`**) as "Current version" with a
       clarifying comment "The value of `VER_DEF_CURRENT` changes as
       necessary to reflect the current version number."

       The description is similar for [`vn_version`
       ](https://docs.oracle.com/en/operating-systems/solaris/oracle-solaris/11.4/linkers-libraries/version-dependency-section.html#GUID-B6FC9352-7C83-4F0C-9DFF-7E932302717A__GUID-ECBB74E2-1A41-410E-92F7-E8CAACE56167)
       with `VER_NEED_NONE` & `VER_NEED_CURRENT` except those
       `#define` names are even *more* confusing and easily
       misunderstood (as mentioned previously). (Not to mention the
       struct field names differ by only one character in their
       prefix.)

     * [`vna_other`](https://docs.oracle.com/en/operating-systems/solaris/oracle-solaris/11.4/linkers-libraries/version-dependency-section.html#GUID-B6FC9352-7C83-4F0C-9DFF-7E932302717A__GUID-209F29E1-9378-4D0A-AD1C-0B4525AEF9F0):

       > _If non-zero, the version index assigned to this dependency
       > version. This index is used within the `SHT_SUNW_versym` to
       > assign global symbol references to this version._
       >
       > _Versions of Solaris up to and including the Oracle
       > Solaris 10 release, did not assign version symbol indexes to
       > dependency versions. In these objects, the value of
       > `vna_other` is `0`._

     * While the ["ELF Version Dependency Indexes" table of the
       "Version Symbol [ELF] Section"
       ](https://docs.oracle.com/en/operating-systems/solaris/oracle-solaris/11.4/linkers-libraries/version-symbol-section.html#GUID-DC6EBD6B-50B1-4197-A00B-A3CAE73FC9E4__CHAPTER6-TBL-32)
       and subsequent remarks are in relation to `SUNW`
       versions, they do seem relevant to the `gnu` versions and
       explicitly state details I don't think I've seen elsewhere (emphasis mine):

        * `VER_NDX_GLOBAL` (`1`) means _"Symbol has global scope **and
          is assigned to the base version definition**."_

        * Confusingly (and incorrectly) it then goes on to imply a
          *different* value for the `VER_NDX_GLOBAL` `#define` but
          what it really seems to mean is if the value of the version
          section/table _entry_ is `>1` (i.e. neither `0`
          a.k.a. `VER_NDX_LOCAL`, nor `1`) then "Symbol has global scope
          and is assigned to a user-defined version definition,
          `SHT_SUNW_verdef`, or a version dependency,
          `SHT_SUNW_verneed`"_.

        * While the first paragraph below may not be new information,
          the second paragraph does seem particularly relevant in
          terms of the "fake"/internal version numbers (discussed
          elsewhere on this page):

          > _Versions defined by an object are assigned version indexes starting
          > at 1 and incremented by 1 for each version. Index 1 is reserved for
          > the first global version. If the object does not have a
          > `SHT_SUNW_verdef` version definition section, then all the global
          > symbols defined by the object receive index 1. If the object does
          > have a version definition section, then `VER_NDX_GLOBAL` simply
          > refers to the first such version._
          >
          > _Versions required by the object from other `SHT_SUNW_verneed`
          > dependencies, **are assigned version indexes that start 1 past the
          > final version definition index.** These indexes are also incremented
          > by 1 for each version. Since index `1` is always reserved for
          > `VER_NDX_GLOBAL`, **the first possible index for a dependency version
          > is `2`**._

          I haven't verified if the details match exactly but the
          point about the starting value, at least, seems to match
          `gnu` behaviour.

]



[^vna_other-comment]:

    [Link to comment as it exists in "current" code
    ](https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/elf.h;h=33aea7f743b885c5d74736276e55ef21756293ee;hb=refs/heads/release/2.40/master#l1152).


[^vna_other-commit]:

    [Link to the related file diff in original commit
    ](https://sourceware.org/git/?p=glibc.git;a=blobdiff;f=elf/elf.h;h=f6779ba16ab8a51558d2211d1dbff0cef71c9a29;hp=05eeb3664f1e96562e271d2da53823e66f48bef2;hb=c84142e8fe3cbcce43ae35ac957a74ac8216d11d;hpb=7434ccadbb6897d366d2377f84efe1e2cd61b02b)
    ([via](https://sourceware.org/git/?p=glibc.git;a=blame;f=elf/elf.h;h=33aea7f743b885c5d74736276e55ef21756293ee;hb=refs/heads/release/2.40/master#l1152)).

    [Alternate file diff link highlighting relevant line
    ](https://github.com/bminor/glibc/commit/c84142e8fe3cbcce43ae35ac957a74ac8216d11d#diff-4726090c318860886c153ce96e26da3e54a78a13054a0cc2bbbf1e89b3e1a4b1R597)


Specifically, the comment (highlighted below) that the `vna_other`
field of the `Elf64_Vernaux`[^elf32-vernaux] struct is "unused" is
_not_ accurate[^vna_other-users] (or, at the least, _no longer_ accurate):

```C
typedef struct
{
  Elf64_Word	vna_hash;		/* Hash value of dependency name */
  Elf64_Half	vna_flags;		/* Dependency specific information */
  Elf64_Half	vna_other;		/* Unused */   [/!\  Outdated Info!  /!\]
  Elf64_Word	vna_name;		/* Dependency name string offset */
  Elf64_Word	vna_next;		/* Offset in bytes to next vernaux
					   entry */
} Elf64_Vernaux;
```

However, it turns out there _does_ appear to be an updated/corrected
version of this structure/field in at least one place,
[`elfcpp/elfcpp_internal.h`](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/elfcpp/elfcpp_internal.h#L271-L274),
where `vna_other` is described as _"Version index used in
`SHT_GNU_versym` entries"_:

```C++
// An auxiliary entry in a SHT_GNU_verneed section.  This structure is
// the same in 32-bit and 64-bit ELF files.

struct Vernaux_data
{
  // Hash of dependency name.
  Elf_Word vna_hash;
  // Bit flags (VER_FLG_*).
  Elf_Half vna_flags;
  // Version index used in SHT_GNU_versym entries.
  Elf_Half vna_other;
  // Offset in string table of version name.
  Elf_Word vna_name;
  // Byte offset to next Vernaux entry.
  Elf_Word vna_next;
};
```

Another potentially helpful resource when trying to understand
versioned symbols and use of `vna_other` is the source code of
`eu-readelf`[^vna_other-eu-readelf] (the `elfutils` reimplementation
of `readelf`).


[^elf32-vernaux]:

    (And perhaps `Elf32_Vernaux` also?)


[^vna_other-users]:

    Example uses of the `vna_other` field include:

     * <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/gold/dynobj.cc#L663-L665>

     * <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/gold/dynobj.cc#L1424-L1426>

       (Aside: There is a nearby [comment in relation to the "base
       version of a shared library"
       ](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/gold/dynobj.cc#L1482-L1492).)

     * <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elflink.c#L2454-L2456>

     * <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elflink.c#L2330-L2332>

     * <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elf.c#L9406-L9409>

       See also use of the related variables `freeidx`, `maxidx` &
       `default_imported_symver`, particularly around here where
       "Create a default version based on the soname" occurs:

        * <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elf.c#L9667-L9693>

     * <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elf.c#L1674-L1676>

     * <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elf.c#L2299-L2301>

       The above occurrence is within the `_bfd_elf_get_symbol_version_string()`
       implementation which has this comment:

       > ```C
       > /* Get version name.  If BASE_P is TRUE, return "Base" for VER_FLG_BASE
       > and return symbol version for symbol version itself.   */
       > ```

       It also [includes this check referencing `cverdefs`
       ](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elf.c#L2276):

       ```C
             else if (vernum <= elf_tdata (abfd)->cverdefs)
       ```

       And, [elsewhere, `cverdefs` is described
       ](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elf-bfd.h#L2130-L2130):

       ```C
       /* Number of symbol version definitions we are about to emit.  */
       unsigned int cverdefs;
       ```

       Which (I assume) relates to the `vna_other` "index" values
       continuing in sequence after the index of the highest symbol
       defined within the library, as this code is executed after the
       the above `cverdefs` test ([via
       ](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elf.c#L2300-L2305)):

       ```C
       if (a->vna_other == vernum)
         {
           *hidden = true;
           version_string = a->vna_nodename;
           break;
         }
       ```

       (See also: `_bfd_elf_slurp_version_tables()`.)

     * <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elflink.c#L5198-L5200>

       The above occurrence is within the _extremely_ long
       [`elf_link_add_object_symbols()` implementation
       ](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elflink.c#L4345-L4348)
       and is preface by this comment:

       ```C
       /* We cannot simply test for the number of
          entries in the VERNEED section since the
          numbers for the needed versions do not start
          at 0.  */
       ```

       There's also references to `VERSYM_HIDDEN`/hidden symbols,
       `SHN_UNDEF`/undefined symbols &
       `default_imported_symver`/default versions, which relate to
       aspects of versioned symbols data.
       (e.g. <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elflink.c#L5147-L5170>)

       And, also ([via](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elflink.c#L5231-L5237)):

       ```C
       *p++ = ELF_VER_CHR;
       /* If this is a defined non-hidden version symbol,
          we add another @ to the name.  This indicates the
          default version of the symbol.  */
       if ((iver.vs_vers & VERSYM_HIDDEN) == 0
           && isym->st_shndx != SHN_UNDEF)
         *p++ = ELF_VER_CHR;
       ```

       Also additional version related code
       ([via](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elflink.c#L11097-L11122)):

       ```C
       if (!h->def_regular && !ELF_COMMON_DEF_P (h))
         {
           if (h->verinfo.verdef == NULL
               || (elf_dyn_lib_class (h->verinfo.verdef->vd_bfd)
                   & (DYN_AS_NEEDED | DYN_DT_NEEDED | DYN_NO_NEEDED)))
             iversym.vs_vers = 1;
           else
             iversym.vs_vers = h->verinfo.verdef->vd_exp_refno + 1;
         }
       else
         {
           if (h->verinfo.vertree == NULL)
             iversym.vs_vers = 1;
           else
             iversym.vs_vers = h->verinfo.vertree->vernum + 1;
           if (flinfo->info->create_default_symver)
             iversym.vs_vers++;
         }

       /* Turn on VERSYM_HIDDEN only if the hidden versioned symbol is
          defined locally.  */
       if (h->versioned == versioned_hidden && h->def_regular)
         iversym.vs_vers |= VERSYM_HIDDEN;

       eversym = (Elf_External_Versym *) flinfo->symver_sec->contents;
       eversym += h->dynindx;
       ```

     * `Elf_External_Vernaux` exists & also has `vna_other`:
       <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/include/elf/external.h#L284-L292>

     * `Elf_Internal_Vernaux` exists & also has `vna_other` (and
       inaccurate comment): <https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/include/elf/internal.h#L222-L235>

     * Also, `Elf_Internal_Versym` is used instead of `Elf32_Half`/`Elf64_Half`
       ([via](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/include/elf/internal.h#L237-L242):

       ```C
       /* This structure appears in a SHT_GNU_versym section.  This is not a
          standard ELF structure; ELF just uses Elf32_Half.  */

       typedef struct elf_internal_versym {
         unsigned short vs_vers;
       } Elf_Internal_Versym;
       ```

     * Note: Both `cverdefs` *and* `cverrefs` are variables that
       exist ([via](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elf-bfd.h#L2129-L2139)):

       ```C
       /* Number of symbol version definitions we are about to emit.  */
       unsigned int cverdefs;

       /* Number of symbol version references we are about to emit.  */
       unsigned int cverrefs;

       /* Symbol version definitions in external objects.  */
       Elf_Internal_Verdef *verdef;

       /* Symbol version references to external objects.  */
       Elf_Internal_Verneed *verref;
       ```
     * `version` field of `elf_symbol_type` (note: *not* a custom type)
       ([via](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/bfd/elf-bfd.h#L85-L88)):

       ```C
       /* Version information.  This is from an Elf_Internal_Versym
          structure in a SHT_GNU_versym section.  It is zero if there is no
          version information.  */
       unsigned short version;
       ```

     * An explanation of a "hidden" symbol as one that _"[...] can
       only be seen when referenced using an explicit version number"_
       ([via](https://github.com/bminor/binutils-gdb/blob/dbe717effbdf31236088837f4686fd5ad5e71893/elfcpp/elfcpp.h#L654-L663)):

       ```C
       // A SHT_GNU_versym section holds 16-bit words.  This bit is set if
       // the symbol is hidden and can only be seen when referenced using an
       // explicit version number.  This is a GNU extension.

       const int VERSYM_HIDDEN = 0x8000;

       // This is the mask for the rest of the data in a word read from a
       // SHT_GNU_versym section.

       const int VERSYM_VERSION = 0x7fff;
       ```

     * lol ([via](https://github.com/bminor/binutils-gdb/blob/dbe717effbdf31236088837f4686fd5ad5e71893/gold/dynobj.cc#L420-L424)):

       ```C
       // The GNU linker clears the VERSYM_HIDDEN bit.  I'm not
       // sure why.

       if (vd_ndx > maxver)
         maxver = vd_ndx;
       ```

       In the most recent source it now states
       ([via](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/gold/dynobj.cc#L564-L569)):

       ```C
       // The GNU linker clears the VERSYM_HIDDEN bit.  I'm not
       // sure why.

       // The first Verdaux holds the name of this version.  Subsequent
       // ones are versions that this one depends upon, which we don't
       // care about here.
       ```

       Which is "new" information, I think.

     * in `gold/symtab.cc` ~([via](https://sourcegraph.com/github.com/bminor/binutils-gdb/-/commit/dbe717effbdf31236088837f4686fd5ad5e71893)):

       ```C
       // In an object file, an '@' in the name separates the symbol
       // name from the version name.  If there are two '@' characters,
       // this is the default version.
       ```

       ```C
       // If this is an absolute symbol, and the version name and
       // symbol name are the same, then this is the version definition
       // symbol.  These symbols exist to support using -u to pull in
       // particular versions.  We do not want to record a version for
       // them.
       ```

     * ([via](https://github.com/bminor/glibc/blob/37214df5f103f4075cf0a79a227e70f3e064701c/elf/dl-version.c#L336-L344)):

       ```C
       if ((ent->vd_flags & VER_FLG_BASE) == 0)
         {
           /* The name of the base version should not be
              available for matching a versioned symbol.  */
           ElfW(Half) ndx = ent->vd_ndx & 0x7fff;
           map->l_versions[ndx].hash = ent->vd_hash;
           map->l_versions[ndx].name = &strtab[aux->vda_name];
           map->l_versions[ndx].filename = NULL;
         }
       ```

     * Also, just in general:

        * <https://github.com/bminor/glibc/blob/37214df5f103f4075cf0a79a227e70f3e064701c/elf/dl-version.c>

        * <https://github.com/bminor/glibc/blob/37214df5f103f4075cf0a79a227e70f3e064701c/elf/get-dynamic-info.h>

          This provides `elf_get_dynamic_info()` and mentions:

          ```C
          /* Read the dynamic section at DYN and fill in INFO with indices DT_*.
          ```

          It uses [`l_ld` which is available in the public `link_map`
          ](https://sourcegraph.com/github.com/bminor/glibc@1228ed5cd520342af906f07eb1b21be82d0b40d0/-/blob/elf/link.h?L91)
          and uses the value of `d_tag` plus a relatively "convoluted"
          lookup calculation to set the content of a non-public
          `info[]` (which is a large array) array entry:

           * <https://github.com/bminor/glibc/blob/37214df5f103f4075cf0a79a227e70f3e064701c/elf/get-dynamic-info.h#L49-L68>

          It uses various macros like `DT_VERSIONTAGIDX()` to
          calculate the index into the array, this is "explained"
          somewhat here:

           * <https://github.com/bminor/glibc/blob/37214df5f103f4075cf0a79a227e70f3e064701c/include/link.h#L118-L134>

          But it was helpful to find out that the "under documented"
          symbols like `DT_VERSIONTAGIDX()` here aren't actually
          anything to do the spec, just an internal detail of `glibc`
          implementation:

           * <https://sourcegraph.com/github.com/bminor/glibc@release/2.35/master/-/blob/elf/elf.h?L953>


[^vna_other-eu-readelf]:

    e.g.:

     * [`handle_dynamic_symtab()`](https://sourceware.org/git/?p=elfutils.git;a=blob;f=src/readelf.c;h=3e97b64cb493c8c45efa6f9535521b03e4403118;hb=HEAD#l2894)

     * `vna_other` use: <https://sourceware.org/git/?p=elfutils.git;a=blob;f=src/readelf.c;h=3e97b64cb493c8c45efa6f9535521b03e4403118;hb=HEAD#l2707>

     * `vna_other` use & `@` output: <https://sourceware.org/git/?p=elfutils.git;a=blob;f=src/readelf.c;h=3e97b64cb493c8c45efa6f9535521b03e4403118;hb=HEAD#l2740>

       This is where output like this (with a reference to the
       "fake"/internal version number--here it's `38`--a.k.a the
       `vna_other` value) is generated from the `.dynsym` symbol table
       data:

       ```
       [...] _dl_exception_create@GLIBC_PRIVATE (38)
       ```

     * <https://sourceware.org/git/?p=elfutils.git;a=blob;f=src/readelf.c;h=3e97b64cb493c8c45efa6f9535521b03e4403118;hb=HEAD#l2787>

       This is where either `@` or `@@` is output (AIUI based on
       "default"/"hidden" symbol version status):

       ```C
       printf ((*versym & 0x8000) ? "@%s" : "@@%s", // [...]
       ```

     * Detection of `BASE`/`VER_FLG_BASE` version flag state:
       <https://sourceware.org/git/?p=elfutils.git;a=blob;f=src/readelf.c;h=3e97b64cb493c8c45efa6f9535521b03e4403118;hb=HEAD#l3104>

     * <https://sourceware.org/git/?p=elfutils.git;a=blob;f=src/readelf.c;h=3e97b64cb493c8c45efa6f9535521b03e4403118;hb=HEAD#l3187>

       The above is where output like this is generated (this is the
       other place with a reference to the "fake"/internal version
       number--here it's `38`--a.k.a the `vna_other` value) from the
       `.gnu.version_r` a.k.a. "version needs section" data:

       ```
       0x0030: Name: GLIBC_PRIVATE  Flags: none  Version: 38
       ```

       **NOTE:**
       It's both important to realise and non-obvious that the other
       `Version` value printed in the output of the
       `.gnu.version_r`/"version needs section" data represents
       [the value of the `vn_version` field
       ](https://sourceware.org/git/?p=elfutils.git;a=blob;f=src/readelf.c;h=3e97b64cb493c8c45efa6f9535521b03e4403118;hb=HEAD#l3170),
       which refers to the **revision/"version" of the `Elf64_Verneed`
       `struct` _itself_** _not_ anything related to a version of a
       symbol:

       ```
       Version needs section [ 9] '.gnu.version_r' contains 1 entry:
       [...]
       000000: Version: 1  File: ld-linux-x86-64.so.2  Cnt: 3
       [...]
       ```

       It's an unfortunate conflict/conflation of terms (that
       primarily only occurs when a structure itself is related to the
       concept of _symbol versions_) due to the spec using a
       `_version` suffix in field names containing the
       "version"/"revision" of a structure (exarcerbated by also
       inconsistently & imprecisely describing the field, e.g. as
       "Version of structure" or "Version revision").

       IMO it would be helpful if tools explicitly referred to the
       value as a "structure revision" or similar to more clearly
       delineate it (or maybe even only display the value if it's
       *not* `1`--which according to the spec is currently the only
       valid value[^valid-version-field-value] for the field.

       Here is the complete output showing the two different senses of
       the `Version` term in context:

       ```
       Version needs section [ 9] '.gnu.version_r' contains 1 entry:
       Addr: 0x0000000000020230  Offset: 0x020230  Link to section: [ 6] '.dynstr'
        000000: Version: 1  File: ld-linux-x86-64.so.2  Cnt: 3
        0x0010: Name: GLIBC_2.2.5  Flags: none  Version: 40
        0x0020: Name: GLIBC_2.3  Flags: none  Version: 39
        0x0030: Name: GLIBC_PRIVATE  Flags: none  Version: 38
       ```

[^valid-version-field-value]: [TODO]



Even after becoming aware that the above comment was outdated & not
accurate, the situation is still complicated as defining/processing
versioned symbol information involves:

 * the content of at least three different sections, one of which has
   subtly different "version"-related behaviour depending on whether a
   symbol is undefined or not;

 * multiple similarly named structs/fields/defines;

 * a term[^term-object-file-version-identifier] used in the specification but not elsewhere AFAICT; and,

 * what appears to be a "convention" that I've not explicitly seen
   spelled out anywhere.

It's also not straight-forward to figure out how to get tools like
`objdump`/`readelf` to show the relevant data.

(Side note: It'd be useful if such tools implemented a `--struct-dump`
option in the style of existing `--hex-dump` and `--string-dump`
options but functioning at a slightly higher level, where the raw
values are shown as interpreted by the applicable `structs`, similar
to how they would appear under `gdb` with relevant debug symbols
supplied.)

However, the following invocation does seem "sufficient" to make the
required hex/string values visible, in order to explain more:

```
$ readelf --all --hex-dump=.dynsym --hex-dump=.gnu.version --hex-dump=.gnu.version_r --string-dump=.dynstr --wide /<path>/libc.so.6 |& less
```

```
[...]

Symbol table '.dynsym' contains 3024 entries:
   Num:    Value          Size Type    Bind   Vis      Ndx Name
     0: 0000000000000000     0 NOTYPE  LOCAL  DEFAULT  UND
     1: 0000000000000000     0 FUNC    GLOBAL DEFAULT  UND _dl_exception_create@GLIBC_PRIVATE (38)
[...]

Version symbols section '.gnu.version' contains 3024 entries:
 Addr: 000000000001e566  Offset: 0x01e566  Link: 5 (.dynsym)
  000:   0 (*local*)      26 (GLIBC_PRIVATE)  26 (GLIBC_PRIVATE)  26 (GLIBC_PRIVATE)
[...]

Version definition section '.gnu.version_d' contains 37 entries:
  Addr: 0x000000000001fd08  Offset: 0x01fd08  Link: 6 (.dynstr)
  000000: Rev: 1  Flags: BASE  Index: 1  Cnt: 1  Name: libc.so.6
[...]
  0x0500: Rev: 1  Flags: none  Index: 37  Cnt: 2  Name: GLIBC_PRIVATE
  0x051c: Parent 1: GLIBC_2.35

Version needs section '.gnu.version_r' contains 1 entry:
 Addr: 0x0000000000020230  Offset: 0x020230  Link: 6 (.dynstr)
  000000: Version: 1  File: ld-linux-x86-64.so.2  Cnt: 3
  0x0010:   Name: GLIBC_2.2.5  Flags: none  Version: 40
  0x0020:   Name: GLIBC_2.3  Flags: none  Version: 39
  0x0030:   Name: GLIBC_PRIVATE  Flags: none  Version: 38

Hex dump of section '.dynsym':
  0x00004ad0 00000000 00000000 00000000 00000000 ................
  0x00004ae0 00000000 00000000 093b0000 12000000 .........;......
  0x00004af0 00000000 00000000 00000000 00000000 ................
[...]

String dump of section '.dynstr':
  [     1]  __pthread_mutex_destroy
[...]
  [  7efc]  GLIBC_2.35
  [  7f07]  GLIBC_PRIVATE


Hex dump of section '.gnu.version':
  0x0001e566 00002600 26002600 26002600 27002800 ..&.&.&.&.&.'.(.
  0x0001e576 26002600 26002600 26002600 26002600 &.&.&.&.&.&.&.&.
[...]

Hex dump of section '.gnu.version_r':
  0x00020230 01000300 697d0000 10000000 00000000 ....i}..........
  0x00020240 751a6909 00002800 887d0000 10000000 u.i...(..}......
  0x00020250 1369690d 00002700 a07d0000 10000000 .ii...'..}......
  0x00020260 85cf6309 00002600 077f0000 00000000 ..c...&.........

[...]
```

[TODO: Explain more? :D ]


[^term-object-file-version-identifier]:

    The [LSB 5.0 Core generic specification
    ](https://refspecs.linuxfoundation.org/LSB_5.0.0/LSB-Core-generic/LSB-Core-generic/symversion.html)
    uses the term "Object file version identifier" to describe the
    value of `vna_other`, (which is what I've referred to elsewhere on
    this page as the "fake"/internal version number) but the original
    ["Symbol Versioning" document](https://www.akkadia.org/drepper/symbol-versioning)
    uses a different term (no less lacking in clarity) in its
    description:

    > _Contains **version index unique for the file** which is used in
    > the version symbol table._

    The [`elfcpp/elfcpp_internal.h`
    ](https://github.com/bminor/binutils-gdb/blob/87480dcfb62faa25b9fc6e46ae58a090ac2e43dc/elfcpp/elfcpp_internal.h#L271-L274)
    file mentioned elsewhere on this page at least uses part of the
    same phrase but with no additional clarificaiton:

    > ```C++
    > // Version index used in SHT_GNU_versym entries.
    > ```

    And, of course, as previously established, `elf.h` is particularly
    unhelpful in this regard, its own unique way. :D

    (The `SUNW` versioning docs use the term/phrase _"the version
    index assigned to this dependency version"_.)

----


## Related links

### `glibc`/`ld-linux`-specific

 * Some `.abilist` files from `glibc` which provide information about
   which release versions modified (e.g. added) particular symbols;
   and, a `Versions` file, which can be used to determine information
   about symbols which are assigned the `GLIBC_PRIVATE` "version"
   (note: the sort order of versions listed is not strictly numeric):

    * [`ld.abilist`](https://sourceware.org/git/?p=glibc.git;a=blob;f=sysdeps/unix/sysv/linux/x86_64/64/ld.abilist;hb=refs/heads/release/2.40/master)
      ([mirror](https://github.com/bminor/glibc/blob/glibc-2.40/sysdeps/unix/sysv/linux/x86_64/64/ld.abilist))

    * [`libc.abilist`](https://sourceware.org/git/?p=glibc.git;a=blob;f=sysdeps/unix/sysv/linux/x86_64/64/libc.abilist;hb=refs/heads/release/2.40/master)
      ([mirror](https://github.com/bminor/glibc/blob/glibc-2.40/sysdeps/unix/sysv/linux/x86_64/64/libc.abilist))

    * [`elf/Versions`](https://sourceware.org/git/?p=glibc.git;a=blob;f=elf/Versions;hb=refs/heads/release/2.40/master)
      ([mirror](https://github.com/bminor/glibc/blob/glibc-2.40/elf/Versions))

 * Commits to `glibc` source containing comments/explanations/info
   related to aspects of various internal
   implementation/versioning-related details involving
   `ld`/`ld-linux`/`libc`/`rtld` et al which I have previously
   encountered and/or anticipate potentially encountering in the
   future:

    * See [this commit](https://sourceware.org/git/?p=glibc.git;a=commitdiff;h=ff6d62e9edb5dce537a6dd4a237d6053f331f09c)
      which mentions:

      > "_When new leaves are added, `_rtld_global_ro` offsets will
      > change [...]_"

    * The file [`include/libc-symbols.h`](https://sourceware.org/git/?p=glibc.git;a=blob;f=include/libc-symbols.h;hb=refs/heads/release/2.40/master)
      contains a bunch of informative comments about
      `rtld`/`ld.so`/`libc` symbol-related... "hijinks".

    * Oh, so, *that's* what caused that message
      ([via](https://github.com/bminor/glibc/commit/f0b2132b35248c1f4a80f62a2c38cddcc802aa8c#diff-7ee66c4f1536ac84dc5bbff1b8312e2eef24b974b3e48a5c5c2bcfdf2eb8f3ce)):

      >
      > * _The dynamic linker no longer refuses to load objects which
      >   reference versioned symbols whose implementation has moved
      >   to a different soname since the object has been linked.  The
      >   old error message, symbol FUNCTION-NAME, version
      >   SYMBOL-VERSION not defined in file DSO-NAME with link time
      >   reference, is gone._
      >

    * Description of `_rtld_global_ro` et al
      ([via](https://github.com/bminor/glibc/commit/20c37dfde1a836a139f6269e8617260f2b90bf52)):

      > _"[...] Define new structure `rtld_global_ro`. Declare
      > `_rtld_global_ro` and `_rtld_local_ro`.  Move members of
      > `rtld_global` structure into the new one if they are modified
      > only at startup time.  Define `GLRO` to access
      > `_rtld_global_ro`."_

    * Discussion about issues related to "`__libc_early_init` suffix
      changes", static `dlopen()` use and a (~reverted?) change
      intended to "Detect ld.so and libc.so version inconsistency
      during startup"
      ([via](https://github.com/bminor/glibc/commit/89baed0b93639180fd7d0ba922873b003649c7af)
      & [via](https://github.com/bminor/glibc/commit/6f85dbf102ad7982409ba0fe96886caeb6389fef)):

      > **Revert "Detect ld.so and libc.so version inconsistency during startup"**
      >
      > _This reverts commit 6f85dbf._
      >
      > _Once this change hits the release branches, it will require
      > relinking of all statically linked applications before static
      > dlopen works again, for the majority of updates on release
      > branches: The NEWS file is regularly updated with bug
      > references, so the `__libc_early_init` suffix changes, and
      > static dlopen cannot find the function anymore._
      >
      > While this ABI check is still technically correct (we do
      > require rebuilding & relinking after glibc updates to keep
      > static dlopen working), it is too drastic for stable release
      > branches.

      If left unreverted an error message would be generated that "the
      version of libc.so it loaded comes from a different build of
      glibc":

      > _"ld.so/libc.so mismatch detected (upgrade in progress?)" if
      > it detects that the version of libc.so it loaded comes from a
      > different build of glibc."_

      Also mentioned is that the "actual" name of `__libc_early_init`
      can change between versions:

      > ```
      > # libc_early_init_name.h provides the actual name of the
      > # __libc_early_init function.  It is used as a protocol version marker
      > # between ld.so and libc.so
      > ```

      Although it seems that may only be the case if the change is
      unreverted ([via](https://github.com/bminor/glibc/commit/6f85dbf102ad7982409ba0fe96886caeb6389fef#diff-2c49b626c34be17b28bc870eb1d1bea84dfc46bde33f32a1b285472d3cafb301)):

      > ```
      > # A pattern is needed here because the suffix is dynamically generated.
      > __libc_early_init_*;
      > ```

      From a brief look at the `NEWS` file the commit doesn't seem to
      have been "unreverted" at any point between 2.37 & 2.40.

    * Discussion about issues related to use of "`EI_ABIVERSION` field
      of the ELF header [...] to indicate the minimum ABI requirement
      on the dynamic linker" (about which I only learned earlier today
      via Wikipedia) and an explanation of the reason for
      `__placeholder_only_for_empty_version_map`
      ([via](https://github.com/bminor/glibc/commit/57292f574156f817b7cbeb33ea6278c6eab22bcc)):

      > **Add GLIBC_ABI_DT_RELR for DT_RELR support**
      >
      > _The EI_ABIVERSION field of the ELF header in executables and
      > shared libraries can be bumped to indicate the minimum ABI
      > requirement on the dynamic linker.  However, EI_ABIVERSION in
      > executables isn't checked by the Linux kernel ELF loader nor
      > the existing dynamic linker.  Executables will crash
      > mysteriously if the dynamic linker doesn't support the ABI
      > features required by the EI_ABIVERSION field.  The dynamic
      > linker should be changed to check EI_ABIVERSION in
      > executables._
      >
      > _Add a glibc version, GLIBC_ABI_DT_RELR, to indicate DT_RELR
      > support so that the existing dynamic linkers will issue an
      > error on executables with GLIBC_ABI_DT_RELR dependency.  When
      > there is a DT_VERNEED entry with libc.so on DT_NEEDED, issue
      > an error if there is a DT_RELR entry without GLIBC_ABI_DT_RELR
      > dependency._
      >
      > _Support `__placeholder_only_for_empty_version_map` as the
      > placeholder symbol used only for empty version map to generate
      > `GLIBC_ABI_DT_RELR` without any symbols._

    * Remark about "substantial" `dlopen()` ABI-related changes &
      static linking issues in glibc v2.34
      ([via](https://github.com/bminor/glibc/commit/466c1ea15f461edb8e3ffaf5d86d708876343bbf)):

      > **dlfcn: Rework static dlopen hooks**
      >
      > _Consolidate all hooks structures into a single one.  There
      > are no static dlopen ABI concerns because glibc 2.34 already
      > comes with substantial ABI-incompatible changes in this area.
      > (Static dlopen requires the exact same dynamic glibc version
      > that was used for static linking.)_
      >
      > _The new approach uses a pointer to the hooks structure into
      > `_rtld_global_ro` and initalizes it in `__rtld_static_init`. [...]_

    * Description of `__libc_early_init` & dynamic loader interactions
      ([via](https://github.com/bminor/glibc/commit/ec935dea6332cb22f9881cd1162bad156173f4b0):

      > **elf: Implement __libc_early_init**
      >
      > _This function is defined in libc.so, and the dynamic loader
      > calls right after relocation has been finished, before any ELF
      > constructors or the preinit function is invoked.  It is also
      > used in the static build for initializing parts of the static
      > libc. [...]_

    * "[elf: Support dlvsym within libc.so](https://github.com/bminor/glibc/commit/82eef55f8fad3e00c53050de5d6ebea08df488b3)"

    * "Unrelated" ([via](https://www.qualys.com/2023/10/03/cve-2023-4911/looney-tunables-local-privilege-escalation-glibc-ld-so.txt)):

      > _"[...] because for all intents and purposes
      > `__minimal_malloc()` always returns a clean chunk of
      > `mmap()`ed memory, which is guaranteed to be initialized to
      > zero by the kernel. [...]"_

      Followed by:

      > _"[...] We then realized that many more pointers in the
      > `link_map` structure are not explicitly initialized to `NULL`;
      > in particular, the pointers to `Elf64_Dyn` structures in the
      > `l_info[]` array of pointers. Among these, `l_info[DT_RPATH]`,
      > the "Library search path", immediately stood out: if we
      > overwrite this pointer and control where and what it points
      > to, then we can force `ld.so` to trust a directory that we
      > own, and therefore to load our own `libc.so.6` or `LD_PRELOAD`
      > library from this directory, and execute arbitrary code (as
      > root, if we run `ld.so` through a SUID-root program). [...]"_

   For, you know, "reasons". :D


### Alternate dynamic linker and/or process execution implementations

 * [`sunfishcode/origin`](https://github.com/sunfishcode/origin) -- "Program startup and thread support written in Rust"

    * [`origin-start-dynamic-linker`](https://github.com/sunfishcode/origin/tree/main/example-crates/origin-start-dynamic-linker)
      ([permalink](https://github.com/sunfishcode/origin/tree/160185d77c9c2300611b0fafb5bf93d1497e4ccd/example-crates/origin-start-dynamic-linker))
      -- an example demonstrating dynamic linker related support

    * [`relocate.rs`](https://github.com/sunfishcode/origin/blob/main/src/relocate.rs)
      -- relocation related code

      A comment about being a [_"dynamic linker compiled with `-Bsymbolic`"_](https://github.com/sunfishcode/origin/blob/160185d77c9c2300611b0fafb5bf93d1497e4ccd/src/relocate.rs#L159C31-L159C73)
      led me to these:

       * "[The Use of `-Bsymbolic`](https://docs.oracle.com/cd/E19957-01/806-0641/chapter4-16/index.html)"
         which mentions:

         > _"This option is somewhat historic, in that it was
         >  primarily designed for use in building the runtime linker
         >  itself."_

         Also, "[Runtime Linker (Linker and Libraries Guide)](https://docs.oracle.com/cd/E19957-01/806-0641/chapter6-35405/index.html)"
         has an interesting overview of tasks intended to be performed
         (though note the document in question relates to Solaris not
         Linux but there are significant overlaps):

         > _"`exec(2)` and the runtime linker cooperate to create the
         > process image for the program, which entails the following
         > actions:"_

         Interestingly, there are also mentions on the page about
         "alternative configurations" associated with an application
         indicated by dynamic tags/flags, described as (for
         `DT_CONFIG`):

         > _"The configuration file may provide search path, directory
         > cache or alternative objects, typically unique to the
         > application specifing the `DT_CONFIG` entry."_

         and (for `DF_1_CONFALT`):

         > _"This state triggers the runtime linker to search for a
         > configuration file $ORIGIN/ld.config.app-name."_

         Which seems like a useful feature which AFAIK doesn't exist
         for Linux?

       * "[ELF interposition and `-Bsymbolic` | MaskRay](https://maskray.me/blog/2021-05-16-elf-interposition-and-bsymbolic)"

 * Also related:

    * FreeBSD [`libexec/rtld-elf`](https://git.quacker.org/d/freebsd-nq/src/commit/73b2958b9422da744c9e7ae6291874c03a2b4325/libexec/rtld-elf)

      Includes this readable example of loading/mapping segments:
      <https://git.quacker.org/d/freebsd-nq/src/commit/73b2958b9422da744c9e7ae6291874c03a2b4325/libexec/rtld-elf/map_object.c#L170-L222>

    * "binary-compatible replacement of /lib/ld-linux.so.2": <https://code.nsnam.org/thehajime/elf-loader/file>

       * More recently updated fork: <https://github.com/robgjansen/elf-loader>

         (Which I discovered due to its inclusion of related reference
         documents including <https://github.com/robgjansen/elf-loader/blob/master/doc/references/symbol-versioning>.)

 * [`nix-community/nix-ld`](https://github.com/nix-community/nix-ld) -- "Run unpatched dynamic binaries on NixOS" ([via](https://news.ycombinator.com/item?id=42279894))

   (Original implementation was replaced by: <https://github.com/nix-community/nix-ld-rs>)

   Also: <https://blog.thalheim.io/2022/12/31/nix-ld-a-clean-solution-for-issues-with-pre-compiled-executables-on-nixos/>



### Versioned Symbols

 * "[Peeter Joot's Blog » An example of Linux/glibc symbol versioning.](https://peeterjoot.com/2019/09/20/an-example-of-linux-glibc-symbol-versioning/)"

 * ["Symbol Versioning" slide from LCA2006 presentation "An introduction to building and using shared libraries" by Michael Kerrisk
   ](https://www.man7.org/conf/lca2006/shared_libraries/slide19a.html)

   Lol, small world, perhaps I should've gone to and/or paid more
   attention to that talk at the time? :D

 * <https://gcc.gnu.org/wiki/SymbolVersioning>

 * <https://sourceware.org/binutils/docs/ld/VERSION.html>

 * <https://sourceware.org/binutils/docs/as/Symver.html#Symver>

 * "The memcpy vs. memmove saga": <https://aeb.win.tue.nl/linux/misc/gcc-semibug.html> :D
   ([archived](https://web.archive.org/web/20241127064758/https://aeb.win.tue.nl/linux/misc/gcc-semibug.html))

   a.k.a. `memcpy@GLIBC_2.14`

   Something something "glibc backwards compatibility".

 * "[All about symbol versioning | MaskRay](https://maskray.me/blog/2020-11-26-all-about-symbol-versioning)"

   Blog post by maintainer(?) of LLVM's `ld.lld` linker implementation.

   This post is one of the more comprehensive overviews of symbol
   versioning, especially in relation to how the functionality is
   actually _implemented_ by various linkers/loaders.

   It's not exactly a "guided" overview of symbol versioning as it's
   somewhat terse/"notes" level detail in places but has some useful
   specifics/historical details.

   Makes reference to other sources:

    * "[Combining Versions – Airs – Ian Lance Taylor](https://www.airs.com/blog/archives/220)"

    * Also:

       * "[When to prevent execution of new binaries with old glibc](https://sourceware.org/pipermail/libc-alpha/2021-October/132168.html)"

          * <https://sourceware.org/pipermail/libc-alpha/2021-October/132293.html>

       * "[[PATCH v2] Support C2X printf %b, %B](https://sourceware.org/pipermail/libc-alpha/2021-October/132150.html)"

          * <https://sourceware.org/pipermail/libc-alpha/2021-October/132153.html>

    * "[Symbol-versioning in ncurses](https://invisible-island.net/ncurses/ncurses-mapsyms.html)"

   Semi-related:

    * <https://maskray.me/blog/2015-09-29-specify-dynamic-linker-to-use-higher-version-gcc> (Non-English version only)



### Linker/"link editor" implementations

Note: Not _dynamic_ linker or _loader_ implementations. (See above for those.)

 * `ld`

 * `gold`

 * `ld.lld`

 * `mold`/`sold`

 * [`wild`](https://github.com/davidlattimore/wild) -- Written in Rust.



### Linux kernel binary execution

#### `/proc/self/exe` & `prctl(PR_SET_MM, PR_SET_MM_EXE_FILE, ...) ` related

 * `criu` (Checkpoint/Restore in Userspace): <https://criu.org/>

    * Current repo: <https://github.com/checkpoint-restore/criu/>


[NOTE TO SELF: Maybe avoid this rabbit hole next time..?]


### Other

 * "Generic System V Application Binary Interface" discussion group: <https://groups.google.com/g/generic-abi>

 * ["Full compatibility. How Static Executables Work on Linux." (archived)
   ](https://web.archive.org/web/20201127141851/http://sysdvd.com/full-compatibility-how-static-executables-work-on-linux-the-hacker/)

 * [`make-portable`](https://github.com/natanael-b/make-portable/tree/master)
   -- "The easiest way to make a glibc executable portable on Linux"

   Includes: <https://github.com/natanael-b/make-portable/blob/master/preloads/libexec.c>

 * "[PRoot — chroot, mount --bind, and binfmt_misc without privilege/setup](https://proot-me.github.io/)"

   Also:

    * "[CARE — Comprehensive Archiver for Reproducible Execution](https://proot-me.github.io/care/)"

    * <https://nixos.wiki/wiki/Nix_Installation_Guide#PRoot>

      Also:

       * <https://nixos.wiki/wiki/Nix_Installation_Guide#Installing_without_root_permissions>

       * <https://nixos.wiki/wiki/Nix_Installation_Guide#nix_2.0.27s_native_method>

    * <https://github.com/proot-me/proot-rs>

    * "[Translation of `execve()`](https://github.com/proot-me/proot-rs/wiki/Translation-of-execve())"

 * Also:

    * "[LKML: Christian Brauner: [PATCH v2 2/2] binfmt_misc: enable sandboxed mounts](https://lkml.org/lkml/2021/12/16/407)"

      "...kernel from 6.7 onwards..." ([via](https://discuss.linuxcontainers.org/t/binfmt-misc-permission-denied-in-unprivileged-container/19813))

    * "[Does allowing binfmt_misc significantly increase the attack
       surface for unprivileged users that already can launch - native
       - binaries?
       ](https://security.stackexchange.com/questions/271786/does-allowing-binfmt-misc-significantly-increase-the-attack-surface-for-unprivil)"

    * <https://docs.kernel.org/admin-guide/syscall-user-dispatch.html>

 * Also:

    * <https://github.com/johannst/elfload> -- "`no_std` elf loader library"

    * <https://github.com/johannst/dynld> -- "Dynamic linker studies."

    * <https://johannst.github.io/notes/development/ld.so.html>

    * <https://johannst.github.io/notes/development/symbolver.html>

    * <https://johannst.github.io/notes/arch/x86_64.html>

 * Also:

    * [`bediger4000/userlandexec`: "userland exec for Linux x86_64"
      ](https://github.com/bediger4000/userlandexec)

    * [`Cloaked9000/ElfLoader`: "ELF loader capable of manually loading ELF executables directly from memory into a new process, without the use of exec."
      ](https://github.com/Cloaked9000/ElfLoader)

    * [`io12/userland-execve-rust`: "implementation of execve() in user space"
      ](https://github.com/io12/userland-execve-rust)

    * <https://github.com/io12/pwninit>
      -- "...Download a linker (`ld-linux.so.*`) that can segfaultlessly load the provided libc..."

       * "[glibc >= 2.34 support](https://github.com/io12/pwninit/pull/282)"

       * Also: <https://github.com/rapid7/mettle/blob/master/libreflect/src/map_elf.c>

    * <https://github.com/rapid7/mettle/tree/master/libreflect>

    * <https://github.com/vfsfitvnm/intruducer> -- "Rust crate to load a shared library into a Linux process without using ptrace."

    * <https://github.com/ohchase/ptrace-do> -- "...library for interacting with unix processes through ptrace..."

      Also: <https://github.com/ohchase/yaui>


### Unrelated

Other "interesting" links encountered during related research that are
not really related to page topic but yet to be filed elsewhere:

 * <https://chromium.googlesource.com/chromium/src/tools/clang/+/refs/heads/main/scripts/update.py>:

   > ```
   > This script is used to download prebuilt clang binaries. [...]
   > It can also be run stand-alone as a convenient way of installing a well-tested
   > near-tip-of-tree clang version:
   > ```

   ([via](https://maskray.me/blog/2021-08-22-freebsd-src-browsing-on-linux-and-my-rtld-contribution#get-prebuilt-clang-and-lld))
