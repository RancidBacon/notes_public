# Notes: `flatpak`

## Uncategorized

 * List which apps are dependent on specific runtimes:

   ```
   flatpak list --app-runtime=org.freedesktop.Platform
   ```

   ```
   flatpak list --app-runtime=org.gnome.Sdk/x86_64/3.38
   ```

   (in part, via: <https://github.com/flatpak/flatpak/issues/3531#issuecomment-1059963576>)

 * Apparently *SDK* "runtimes"/"platforms" aren't automatically
   deleted (or considered "unused") when they (no longer) have no
   dependencies because they might be being used for development(!):

   >> INTERNET: Yeah those are SDK's - those can stick around on their
   >> own because maybe you're a dev that's using them, even if none
   >> of your packages depend on them.

   (via <https://github.com/flatpak/flatpak/issues/3531#issuecomment-1445065264>)

   (Although after deleting the KDE SDK you might find your notes that
   remind you that you installed the SDK in order for Kate to have
   access to `git`--at least that's what happened to me. :D)

 * Some one-liners I've used while "spring-cleaning" (i.e. wanting to remove runtimes):

   ```
   # "On disk size" of runtimes/apps. (Note: includes multiple symlinks.)
   du -h --max-depth=2 --total ~/.local/share/flatpak/runtime/* | sort --human-numeric-sort --reverse | head -n 30
   du -h --max-depth=2 --total ~/.local/share/flatpak/app/* | sort --human-numeric-sort --reverse | head -n 30

   flatpak list --app --columns=runtime,application,size | sort

   flatpak list --runtime

   flatpak uninstall --unused # (Although sometimes it doesn't detect dependencies correctly? e.g. "Error: Can't remove org.kde.Platform/x86_64/5.15, it is needed for: org.kde.kate/x86_64/stable")

   flatpak remote-info --log flathub com.github.fabiocolacio.marker


   # Available updates displayed with related runtime
   flatpak remote-ls --updates --columns=runtime,application,download-size,installed-size,application,version | sort | less --chop-long-lines
   flatpak remote-ls --cached --updates --columns=runtime,application,download-size,installed-size,application,version | sort | less --chop-long-lines
   ```
