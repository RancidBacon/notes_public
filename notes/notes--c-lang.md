## Notes: C (Programming Language)

### `libc`-related

 * "[Comparison of C/POSIX standard library implementations for Linux](https://www.etalabs.net/compare_libcs.html)"

 * [`tugger-binary-analysis`](https://github.com/indygreg/PyOxidizer/tree/main/tugger-binary-analysis) / <https://lib.rs/crates/tugger-binary-analysis> -- "library crate implementing functionality for analyzing platform native binaries"

    * "Defines mappings of gcc and glibc versions to Linux distributions." :o

      See: [`GLIBC_VERSIONS_BY_DISTRO` in `tugger_binary_analysis/linux_distro_versions.rs`](https://docs.rs/tugger-binary-analysis/0.7.0/src/tugger_binary_analysis/linux_distro_versions.rs.html#9-94)

    * Also "Obtain shared library dependencies" / "Find unresolved symbols" / "Analyze for machine portability".

 * See also:

    * <https://github.com/indygreg/linux-packaging-rs> -- "Rust projects related to Linux packaging"

      Includes:

       * <https://github.com/indygreg/linux-packaging-rs/tree/main/debian-repo-tool>

       * <https://github.com/indygreg/linux-packaging-rs/tree/main/linux-package-analyzer>

    * <https://github.com/indygreg/toolchain-tools> -- "Projects related to packaging and language toolchain support"

       * <https://github.com/indygreg/toolchain-tools/tree/main/binary-portability>



### Dynamic Linking

 * "[Linkers and Loaders](https://www.iecc.com/linker/)" / <https://linker.iecc.com/>

 * <https://www.akkadia.org/drepper/dsohowto.pdf> -- "How to Write Shared Libraries" (PDF)


#### Symbol versioning

 * "summary of [the/a ncurses developer's] experience with symbol versioning":
   <https://invisible-island.net/ncurses/ncurses-mapsyms.html>

   Particularly:

    * <https://invisible-island.net/ncurses/ncurses-mapsyms.html#bk_followup>

    * <https://invisible-island.net/ncurses/ncurses-mapsyms.html#problems>

   It is, at times, a somewhat... delightful(?) read. :)

   Also, via <https://invisible-island.net/ncurses/ncurses-mapsyms.html#proc_report>, this:

   ```
   objdump -axhT foo
   ```

   apparently produces output along the lines of:

   ```
   [...snip...]
   Version References:
     [...snip...]
     required from libc.so.6:
       0x0d696913 0x00 16 GLIBC_2.3
       0x0d696914 0x00 08 GLIBC_2.4
       0x09691a75 0x00 03 GLIBC_2.2.5
       0x09691974 0x00 02 GLIBC_2.3.4
   [...snip...]
   ```

 * See (via above): <https://sourceware.org/binutils/docs/ld/VERSION.html>

   Which, amongst other things,  mentions:

   > "[...] symbol versioning [...] The fundamental problem that is
   >  being addressed here is that typically references to external
   >  functions are bound on an as-needed basis, and are not all bound
   >  when the application starts up. If a shared library is out of
   >  date, a required interface may be missing; when the application
   >  tries to use that interface, it may suddenly and unexpectedly
   >  fail."

   Well, if *that's* the problem that's trying to be solved, based on
   my experience with libc & symbol versioning, in some ways the
   "cure" seems worse than the "disease":

   "Hey, instead of a program having 99-100% of its functionality
   working due to a glibc version change (which might even only be
   related to the existence of a `#define`), how about we just *stop*
   100% of the functionality from working?"

   Although, maybe this *wasn't* the outcome originally *intended*? In
   light of the next sentence:

   > "With symbol versioning, the user will get a warning when they
   >  start their program if the libraries being used with the
   >  application are too old."

   You say "a warning", system says "execution halting *error*", same
   diff, I guess..?

 * *See also:* `libc` section above.


#### Tools

 * *See also:* `libc` section above.


----

## Uncategorized

 * "[Driving Compilers](https://fabiensanglard.net/dc/index.php)"

 * "[Driving Compilers](https://fabiensanglard.net/dc/cpp.php)"

 * "[[clang-cl] Add -emit-ast to clang-cl driver · llvm/llvm-project@3f0578d · GitHub](https://github.com/llvm/llvm-project/commit/3f0578dd87ee5539eccae507b6a77cfe3354d705#diff-b96d6afe9c11eebd46a3db540b201d765aeca1577e857bef8f2c34f80adb97a4R29)"

 * "[c++ - What are canonical types in Clang? - Stack Overflow](https://stackoverflow.com/questions/25231080/what-are-canonical-types-in-clang)"

 * "[clang: include/clang/AST/Type.h Source File](https://clang.llvm.org/doxygen/Type_8h_source.html#l02774)"

 * "[GitHub - chadbrewbaker/awesome-ast: Tools for AST hacking](https://github.com/chadbrewbaker/awesome-ast)"

 * "[GitHub - rsms/llvmbox: Self contained, fully static llvm tools & libs](https://github.com/rsms/llvmbox)"
