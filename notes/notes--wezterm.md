# Notes: WezTerm Terminal Emulator

_Project home: <https://wezfurlong.org/wezterm/>_

## Related links

### My related projects

 * "Lua Cheatsheet for WezTermites" (WIP): <https://gitlab.com/RancidBacon/floss-various-contribs/-/blob/main/wezterm/wezterm-lua-cheatsheet-docs/notes--wezterm-lua-cheatsheet.md>


## Usage notes (general interest)

 * The debug overlay REPL history gets stored/loaded via a file named
   `repl-history` in the "runtime dir", which for me was
   `~/.local/share/wezterm/repl-history`. ([via](https://github.com/wez/wezterm/commit/d41ed68b1ac10462e22fe48b2a43125a10d392dc))

 * This will start a new gui process without needing to mess around with job control:

   ```terminal
   $ systemd-run --collect --user /<path>/wezterm start
   ```

   As `--always-new-process` still semms to require something like this:

   ```terminal
    nohup /<path>/wezterm start --always-new-process &
   ```

   Either seems sufficient to prevent a debug overlay REPL Lua being
   stuck in an infinite loop in one process from taking the other
   process with it.

   (They live in separate worlds though, so primarily best for testing
   during WezTerm Lua-related development.)

 * `--position 'main:30%,30%'` ([via](https://github.com/wez/wezterm/issues/1794#issuecomment-1086969663))


### Changing content of right-click "New Tab"/"+"/"Launch" menu

```lua

wezterm.on('new-tab-button-click', function(window, pane, button, default_action)

  if button == 'Right' then

    if true then
      --
      -- This variant prevents the large number of default launch menu
      -- items from being added and instead only includes the items
      -- from custom `launch_menu` config.
      --
      -- For other values available to control the combination of
      -- "sets"/"groups"/"types" of items added see `flags` docs
      -- here: <https://wezfurlong.org/wezterm/config/lua/keyassignment/ShowLauncherArgs.html>
      --
      window:perform_action(act.ShowLauncherArgs{flags='LAUNCH_MENU_ITEMS'}, pane)
    end

    return false

  end

end)
```



## Usage notes (possibly of niche interest :) )

 * This mentions using "Using raw syntax for backwards compatibility"
   for `wezterm.action.*` items:

    * <https://github.com/wez/wezterm/issues/3454#issuecomment-1499173746>

   While a proper solution for the "detect if action is available"
   situation has since been implemented (via
   `wezterm.has_action('PromptInputLine')`), perhaps the raw syntax
   might still be useful in some other situation?



## Kitty Image Protocol related (semi-related)

 * <https://github.com/dsanson/termpdf.py>

   Though `termpdf.py` seems to have "issues" with WezTerm currently, according to reports:

    * <https://github.com/dsanson/termpdf.py/issues/41>

    * <https://github.com/dsanson/termpdf.py/pull/44>



## General terminal-related notes

### Tools

 * `infocmp` will output terminal info like so:

   ```terminal
   $ infocmp
   #       Reconstructed via infocmp from file: /lib/terminfo/x/xterm-256color
   xterm-256color|xterm with 256 colors,
           am, bce, ccc, km, mc5i, mir, msgr, npc, xenl,
           colors#0x100, cols#80, it#8, lines#24, pairs#0x7fff,
   [...snip...]
   ```

 * `man terminfo` includes a section "Predefined Capabilities" which
   lists capabilities, their names and a very brief description, e.g.:

   ```
     Variable                     Cap-                                    TCap                Description
     String                       name                                    Code
   ----------------------------|----------------------------|----------------------------|----------------------------|
     cursor_home                  home                                     ho               home cursor (if no cup)
   ```

 * `tput` will retrieve and output the appropriate characters for a
   given named capability, e.g.:

   ```terminal
   $ tput home >> /tmp/tput-home.txt
   ```

   (If not re-directed it will output to the current terminal which
   may or may not be desired--depending on what capability you're
   testing with. :) )

   Redirecting to a file means you can view the characters output
   and/or cat the file to cause the related action to occur, e.g.:

   ```terminal
   $ xxd /tmp/tput-home.txt
   00000000: 1b5b 48                                  .[H
   ```

   ```terminal
   $ cat /tmp/tput-home.txt
   [ cursor moves to top left corner of terminal]
   ```

   Or even string multiple actions together, e.g.:

   ```terminal
   $ tput il1 >> /tmp/tput-il1.txt # "open a new blank line before the line where the cursor is"
   $ cat /tmp/tput-home.txt /tmp/tput-il1.txt /tmp/tput-il1.txt
   [ cursor moves to top left corner of terminal and then moves up two lines]
   ```

   Which, helpfully, demonstrates that what I wanted to try does indeed appear to work... :)

   (Although apparently `ri` might be "better" than `il1`...)

 * See also: `stty`.
