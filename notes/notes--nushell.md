## Notes: `nushell`

[TOC]

### Links

 * "[Command Reference | Nushell](https://www.nushell.sh/commands/)"

### Using `nushell` structured data pipes from other shells (e.g. `bash`)

(Tested with `nu-0.84.0`.)

 * Example use of `to/from nuon` to use bash for the pipes but preserve (most) data types:

    ```bash
    $ nu --stdin --commands 'ls | to nuon' | nu --stdin --commands 'from nuon | where size > 100MiB'
    ```

 * Specific external `ls` binary example options (with basic parsing of output based on ["Parse external ls command and combine columns for datetime" example](https://www.nushell.sh/commands/docs/detect_columns.html#examples)):

    ```bash
    # Option 1 (use `^ls`)
    $ nu --commands '^ls -lb | detect columns --no-headers --skip 1 --combine-columns 5..7 | select column4 column8 | rename size name | update size {|it| $it.size | into filesize} | last 5 | where size > 10KiB'
    ```

    ```bash
    # Option 2 (use `run-external`)
    $ nu --commands 'run-external --redirect-stdout "ls" "-lb" | detect columns --no-headers --skip 1 --combine-columns 5..7 | select column4 column8 | rename size name | update size {|it| $it.size | into filesize} | last 5 | where size > 10KiB'
    ```

    ```bash
    # Option 3 (use `--stdin`)
    $ ls -lb | nu --stdin --commands 'detect columns --no-headers --skip 1 --combine-columns 5..7 | select column4 column8 | rename size name | update size {|it| $it.size | into filesize} | last 5 | to nuon' | nu --stdin --commands 'from nuon | where size > 10KiB'
    ```

#### (Semi-related) Links

 * "[scripts can't operate as part of a pipeline · Issue #9373 · nushell/nushell](https://github.com/nushell/nushell/issues/9373#issuecomment-1635919687)"

 * "[Expose some commands to external world · Issue #6554 · nushell/nushell](https://github.com/nushell/nushell/issues/6554#issuecomment-1672012053)"

   * > "We want to focus on getting the experience inside nushell right and probably won't be able to dedicate design time to get the interface of native Nu commands with an outside POSIX shell right and stable."

   * "Helper to call nushell nuon/json/yaml commands from bash/fish/zsh": [`https://github.com/cruel-intentions/devshell-files/blob/master/examples/nushell/nuon.nu`](https://github.com/cruel-intentions/devshell-files/blob/fad6ca5fe4bf0a4689e229cee26be39c008d3c3a/examples/nushell/nuon.nu)

 * "[Is there a method that can execute commands in a variable? · nushell/nushell · Discussion #5413](https://github.com/nushell/nushell/discussions/5413#discussioncomment-2683978)"

 * `jc` -- "converts output of popular command-line tools [...] to JSON [...]"

     - <https://kellyjonbrazil.github.io/jc/docs/parsers/ls>

     - The `ls` output parsing implementation: [`https://github.com/kellyjonbrazil/jc/blob/master/jc/parsers/ls.py#L187`](https://github.com/kellyjonbrazil/jc/blob/4cd721be8595db52b620cc26cd455d95bf56b85b/jc/parsers/ls.py#L187)


### More notes on `nu` use

#### April 2024

(Tested with `nu-0.92.2` ("full" version).)

After some time away from using `nu` I returned in order to make use
of its JSON-munging abilities to investigate an issue I'd
encountered.

While I performed the actual investigation with the already installed
0.84.0 version (I was *already* too many rabbit holes deep to risk
installing a later version :D ) after a successful experience I
decided to try out the most recent version (v0.92.2). (Which from the
intervening versions' release notes seemed to have had some
improvements for various QoL/UX/papercut issues I remembered having
previously.)

 * Two particular aspects (separate from the UX-related ones referred
   to above) I found frustating during my issue investigation were:

    1. Seemingly inconsistent/unexpected behaviour that caused nushell
       to attempt (and fail) to run an external program when I was
       trying to e.g. refer to a variable or use an expression or some
       internal nu functionality.

    2. Extremely unintuitive/confusing behaviour in relation to
       (especially) boolean logical expressions (particularly `and` &
       `not`) used with the `where` command.

    3. (Bonus :) ) Variable naming & access + built-in vs "by
       convention".

As I almost immediately encountered the first issue again when I
started using v0.92.2 I thought I'd at least take some notes about
these to remind me & maybe serve as the basis for a future issue
report (lol, yeah, that's gonna happen. :/ ).

(Also I did get some hints about what might be happening from reading
the latest online documentation.)


##### 1. Unexpected `Error: nu::shell::external_command`/`External command failed` error.

The output from `help sys` includes the following examples:

```
  Show the os system name with get
  > (sys).host | get name

  Show the os system name
  > (sys).host.name
```

Two questions:

 1. What's the difference between two variants (i.e. `get name` vs `.name`)?

 2. Why are the parentheses required in this situation?

My current (partial) understanding of the answers to these questions
is:

 1. With the `get` variant it's possible to retrieve the values from
    more than one field/column at the same time, e.g.:

    ```
    (sys).host | get name hostname
    ```

 2. This variant which uses parentheses around the `sys` command
    (which produces a "record" as output) returns the expected result:

    ```
    nu> (sys).host.hostname
    ```

    But if the parentheses are *not* used, the following *very*
    unexpected error occurs:

    ```
    nu> sys.host.hostname
    Error: nu::shell::external_command

      × External command failed
       ╭─[entry #1:1:1]
     1 │ sys.host.hostname
       · ────────┬────────
       ·         ╰── executable was not found
       ╰────
      help: No such file or directory (os error 2)
    ```

    What? Who was trying to run an external (i.e. a "system" command
    rather than an internal nushell command) command?

    Why is the `host.hostname` part okay without parentheses but
    `sys.host` part isn't?

    I ran into this issue a *lot* when I was trying figure out the
    correct use of parentheses while trying to understand the negated
    boolean logic expressions issue mentioned (but not yet described)
    previously.

    I'd recognized that parentheses were important/required in some
    situations and were (presumably) required for the "subexpressions"
    that seemed to be required in some situations (for things to be
    interpreted as desired) but I didn't understand the logic
    behind when it was required/worked & when not.

    The `help` command also shows this as a variant as an example:

    ```
    sys | get host
    ```

    And, indeed, this variant doesn't require use of parentheses either:

    ```
    sys | get host.name
    ```

    So, what's the deal?

    (Aside: `columns` is handy to see available columns for output,
            e.g. `ps | columns` or `ps --long | columns`.)

    Side note:

     * This works:

       ```
       ( sys )
       ```

     * This does *not* work (and, indeed, is highlighted as an error when
       entered on the command line):

       ```
       sys .host
       ```

       It generates this error:

       ```
       Error: nu::parser::extra_positional

         × Extra positional argument.

       [...]
       ```


    ###### `ast` command

    One potentially helpful tool is the `ast` command (which I think
    was added since I last used nushell and seems to be a nice way to
    learn/understand nushell-specific terminology in a different way
    than is "traditionally" available in such situations--kind of like
    a "what *is* this thing?" button :) ).

    Let's start with:

    ```
    ast "sys"
    ```

    and:

    ```
    ast "(sys)"
    ```

    Now, this was already educational from seeing the output but even
    more so because I wanted to be able compare the output
    side-by-side, which lead to so interesting discoveries while
    trying to figure out how to do that. :)

    (Actually, what I *really* want to do is _diff_ the output but I
    don't think there's an internal command for that and I'd have to
    *think* to try to do it with an external `diff` command and
    *actual* rabbits don't dig as many holes as I'm currently in. :D )

    (Aside: _\*cough\*_

    Oh, apparently `diff` has these options for comparing multiple
    files (or directories) to the same single file (or directory)
    which I don't think I was previously aware of (and vaguely recall
    possibly having need of at some point in "recent" history--oh,
    yeah, I remember now: it was when I was trying to find the
    "latest" variant of a GDScript "utility" class I'd written a
    couple of variants of):

    ```
           --from-file=FILE1
              compare FILE1 to all operands; FILE1 can be a directory

       --to-file=FILE2
              compare all operands to FILE2; FILE2 can be a directory
    ```

    Err, anyway... :) )

    (Aside: Oh, hey, `rediff`/`editdiff` exist to _"fix offsets and
            counts of a hand-edited diff"_. Not that *I* would've ever
            attempted to hand edit a diff and run into problems before.. :D )

    (Aside: `espdiff` [lol](https://stackoverflow.com/questions/44728690/espdiff-what-is-it-and-how-can-i-use-it#47211709).)

    (Aside: [TODO] Re-visit `git diff` `--anchored=`,
            `--break-rewrites`, `--inter-hunk-context=`/
            `diff.interHunkContext`, `-O`/`diff.orderFile`.)

    (Aside: `gitdiffcore` man page is somewhat informative,
            and also includes an "orderfile" example which
            more immediately communicates why it might be
            useful.)

    (Aside: [TODO] Re-visit `hexdiff`, `mcdiff` (+ maybe internal
            binary editor), `msidiff`, `ptardiff`, `vbindiff`.)

    (Side note: Python's [`difflib`](https://docs.python.org/3/library/difflib.html) exists.)

    (Aside: Now, where was I? :D )


    ###### `ast` command (too much magic interlude - part 1)

    First, in my attempt to display the `ast` output side-by-side, I
    tried this form (as "described" by `describe`:)

    ```
    > [ast "sys", ast "(sys)"] | describe
    list<string>
    ```

    Okay, so, this is an example of nushell that I find
    *frustrating*. Essentially, it seems to have more
    "surprising"/"magic" behaviour than... whatever I'm used to. :)

    Now, this could be difference of POV/mindset/philosophy but I find
    that it adds a lot of friction to the "iterative construction"
    approach that I tend to use for learning/creating things.

    And perhaps it's related to doing this inside the interactive
    shell rather than within a script but I thought that was half the
    point of nushell? :)

    Because, of course, the *previous*/first step had been:

    ```
    > ast "sys"
    [...snip...]
    ```

    And, to me, it's "unexpected" when some (unquoted) text resulted
    in a command being executed in one place but then "magically"
    turns into a string when placed into an array/list.

    It makes me think I'm missing/misunderstanding something fairly
    fundamental. e.g. Is there an additional "implicit" step being
    performed when I enter text at the nushell prompt? Or is there
    some concept that I'm missing that when taken into account would
    mean the behaviour can be considered "consistent"?


    ###### `ast` command (too much magic interlude - part 2)

    I'm aware there is *some* "magic" behaviour with nushell due to
    comments about not having to use commas in lists or something...

    Which *does* seem to be a contributing factor in at least some of
    what I'm seeing here, consider these examples (assuming the
    default table theme box characters aren't broken :) ):

    ```
    > [ast sys]
    ╭───┬─────╮
    │ 0 │ ast │
    │ 1 │ sys │
    ╰───┴─────╯

    > [ast sys,]
    ╭───┬─────╮
    │ 0 │ ast │
    │ 1 │ sys │
    ╰───┴─────╯

    > [ast "sys", ast "(sys)"]
    ╭───┬───────╮
    │ 0 │ ast   │
    │ 1 │ sys   │
    │ 2 │ ast   │
    │ 3 │ (sys) │
    ╰───┴───────╯
    ```

    To me, this behaviour at least *borders* on "broken".

    And, being able to "avoid" having to use commas to separate list
    items certainly isn't worth it to me--particularly when the
    presence of commas & quotes are basicly ignored rather than
    being used as indicator of separation and/or quoted.

    (Side note: The default right-side timestamp output really
    interferes with copy & paste of demonstration command input &
    output from my terminal--although it may be possible to
    automatically exclude it via OSC prompt indicators with some
    effort.)

    We can see here (via my fancy new `ast-compare-inner` command :) )
    that the first item is parsed as a `Call` but the second as a
    `FullCellPath`:

    ```
    > ["sys", "(sys)"] | ast-compare-inner | get expr | columns
    ╭───┬──────────────╮
    │ 0 │ Call         │
    │ 1 │ FullCellPath │
    ╰───┴──────────────╯
    ```

    And we find these both produce `FullCellPath`s also:

    ```
    > ["[sys]", "[(sys)]"] | ast-compare-inner | get expr
    [...snip...]
    ```

    In the first case the result is a `List` containing `String` and
    in the second a `List` containing a `FullCellPath` with
    `Subexpression`.

    And it just seems unintuitive to me that `[a (sys)]` would result
    in a list with one string and the output of a subexpression which
    called a command:

    ```
    > ['[a (sys)]'] | ast-compare-inner | get expr | first
    [...snip...]
    ```

    *This* selection of behaviours just seems *entirely* buggy: :D

    ```
    # Example (1)
    > [a (echo "hello")]
    ╭───┬───────╮
    │ 0 │ a     │
    │ 1 │ hello │
    ╰───┴───────╯
    ```

    ```
    # Example (2)
    > [a \(echo "hello"\)]
    Error:   × Invalid literal
    ╭─[entry #1:1:19]
    1 │ [a \(echo "hello"\)]
    ·                   ▲
    ·                   ╰── unrecognized escape after '\' in string
    ╰────
    ```

    ```
    # Example (3)
    > [a \(echo hello)]
    ╭───┬────────╮
    │ 0 │ a      │
    │ 1 │ \hello │
    ╰───┴────────╯
    ```

    ```
    # Example (4)
    > [a \(echo hello\)]
    ╭───┬─────────╮
    │ 0 │ a       │
    │ 1 │ \hello\ │
    ╰───┴─────────╯
    ```

    Oh, I hadn't even tried this intentionally, but:

    ```
    > ['a'] | ast-compare-inner | get expr | first
    ╭──────────────┬────────────────────────────────────────────────────╮
    │              │ ╭───┬────────────────────────────────────────────╮ │
    │ ExternalCall │ │ 0 │ ╭───────────────────┬────────────────────╮ │ │
    │              │ │   │ │                   │ ╭────────┬───╮     │ │ │
    │              │ │   │ │ expr              │ │ String │ a │     │ │ │
    │              │ │   │ │                   │ ╰────────┴───╯     │ │ │
    │              │ │   │ │                   │ ╭───────┬────────╮ │ │ │
    │              │ │   │ │ span              │ │ start │ 119615 │ │ │ │
    │              │ │   │ │                   │ │ end   │ 119616 │ │ │ │
    │              │ │   │ │                   │ ╰───────┴────────╯ │ │ │
    │              │ │   │ │ ty                │ String             │ │ │
    │              │ │   │ │ custom_completion │                    │ │ │
    │              │ │   │ ╰───────────────────┴────────────────────╯ │ │
    │              │ │ 1 │ [list 0 items]                             │ │
    │              │ ╰───┴────────────────────────────────────────────╯ │
    ╰──────────────┴────────────────────────────────────────────────────╯
    ```

    And *this* is apparently parsed as having a `StringInterpolation`(!?)
    (as the second item in the list) containing a `\` string and the
    remainder as `FullCellPath`/`Subexpression` combo:

    ```
    > ['[a \(echo "hello")]'] | ast-compare-inner | get expr.0.FullCellPath.head.expr
    [...snip...]
    ```

    So, yeah, *definitely* feels like "too much magic" to/for me... :/

    [TODO: change my `ast-compare-inner` command to also display an
           error message when one is present. :) ]

    Did I answer whatever the question was yet?! :D

    \*looks back\*

    No, no, I don't think I did...

    **See also:**

     * _"Appendix A"_ below for `ast-compare` & `ast-compare-inner` code
       and related development code.


    ###### `ast` command (continued)

    [TODO]


    ###### RTFM: String Formats

    Probably relevant: :)

     * <https://www.nushell.sh/book/working_with_strings.html#string-formats-at-a-glance>

     * See especially:

        * "Double-quoted string"

        * "Bare string"

           * "Can only contain 'word' characters"

           * "Cannot be used in command position" (seems *very*
             relevant :p )

        * "Single-quoted interpolation" & "Double-quoted interpolation".

    Well, yes, *okay* (via <https://www.nushell.sh/book/working_with_strings.html#bare-strings>):

    > But be careful - if you use a bare word plainly on the command
    > line (that is, **not inside a data structure or used as a command
    > parameter**) or *inside round brackets* `(` `)`, it will be
    > interpreted as an external command: [...snip...]

    Indeed!

    The problem being that it's not obvious that the text you've typed
    *is* a "special" type of string.

    At a minimum I think it could be helpful if the `Error:
    nu::shell::external_command` "help" section had a comment that if
    you *weren't* trying to run a program then maybe you're running
    into an issue because you're "using a 'bare word' format string in
    a 'command position'.

    And that *still* doesn't seem to explain *all* the weirdness I was
    (and *am* with some quick experiments) seeing.

    Overall it *still* seems like a pretty cost to pay for "luxury" of
    having such a thing as a "bare string".

    *Especially* when (!):

    > "Also, **many bare words have special meaning in nu**, and **so will
    >  not be interpreted as a string**"

    But also, it gets *better* because:

    ```
    > 1==1
    Error: nu::parser::unknown_command

      × Unknown command.
       ╭─[entry #1:1:1]
     1 │ 1==1
       · ──┬─
       ·   ╰── unknown command
       ╰────
    ```

    Other quotes from the page:

    * > "By wrapping expressions in `()`, we can run them to
      >  completion and use the results to help build the string."


    Other related docs quotes:

     * When the "[Creating lists](https://www.nushell.sh/book/working_with_lists.html#creating-lists)"
       section states...

       > "values separated by spaces and/or commas (for readability)"

       ...it doesn't warn about the non-obvious/non-intuitive
       behaviour when "bare words", "space separation" & "comma
       separataion" interact, e.g.:

       ```
       > [ a, b b, c]
       ╭───┬───╮
       │ 0 │ a │
       │ 1 │ b │
       │ 2 │ b │
       │ 3 │ c │
       ╰───┴───╯
       ```

       Note that `b b` is split as the commas seem to be ignored/overridden.

       The behaviour also occurs with `[a,b b,c]` but *doesn't* occur
       with the following variant (but clearly it also includes the
       single quote marks in the apparent concatenation result):

       ```
       > [a,b' 'b,c]
       ╭───┬───────╮
       │ 0 │ a     │
       │ 1 │ b' 'b │
       │ 2 │ c     │
       ╰───┴───────╯
       ```

       Also, via: <https://www.nushell.sh/book/types_of_data.html#lists>:

       > "List syntax [...] commas are not required to separate values
       > if Nushell can easily distinguish them!"

       No comment.


    Other:

     * via <https://www.nushell.sh/book/variables_and_subexpressions.html#subexpressions>:

       > "You can always evaluate a subexpression and use its result
       >  by wrapping the expression with parentheses `()` [...] The
       >  parentheses contain **a pipeline that will run to
       >  completion**, and the resulting value will then be
       >  used. [...] You can also use parentheses to run math
       >  expressions like `(2 + 3)`."

     * via <https://www.nushell.sh/book/metadata.html>:

       > "In using Nu, you may have come across times where you felt
       >  like there was something extra going on behind the scenes."

       Well, you're not wrong.. :D

       But perhaps we're thinking of different things...

       > "Currently, we track only the span of where values come from."


    **See also:**

     * _"Appendix B"_ below for more related tests.



##### 2. (boolean logical expressions)

[TODO]

 * via <https://www.nushell.sh/book/variables_and_subexpressions.html#short-hand-subexpressions-row-conditions>:

   > """
   > Short-hand subexpressions (row conditions)
   >
   > Nushell supports accessing columns in a subexpression using a
   > simple short-hand. [...]
   >
   > The `where size > 10kb` is a command with two parts: the
   > command name `where` and the short-hand expression `size >
   > 10kb`. We say **short-hand because `size` here is the
   > shortened version of writing `$it.size`**. [...]
   >
   > **For the short-hand syntax to work, the column name must
   > appear on the left-hand side of the operation** (like `size`
   > in `size > 10kb`).
   >
   > """

The latter point may have been at play in this situation? And maybe the use of `()`?



##### 3. (Variable naming & access + built-in vs "by convention")

[TODO]

As an example, the "[Iterating over lists](https://www.nushell.sh/book/working_with_lists.html#iterating-over-lists)"
section states:

> "The block parameter (e.g. `|it|` in `{ |it| print $it })`..."

and then shows an example (with `each`) where `it` is specified as a
manually supplied parameter name:

> ```nu
> let names = [Mark Tami Amanda Jeremy]
> $names | each { |it| $"Hello, ($it)!" }
> ```

But then in another example (with `where`) it shows `$it` being used
without having be defined anywhere:

> ```nu
> let colors = [red orange yellow green blue purple]
> $colors | where ($it | str ends-with 'e')
> # The block passed to `where` must evaluate to a boolean.
> ```

In this case `where` is apparently "different" because it uses a "row
condition" which *does* apparently provide a `$it` variable
automatically.

Then, again, later in the section it mentions the `reduce` command but
states about the first parameter that it is only "_conventionally_
named `it`":

> "The `reduce` command [...] uses a block which takes 2 parameters:
>  the current item (conventionally named `it`) [...]"

And, to a certain degree if we're using "magic" so much, why even
introduce `$it` in cases where there's only a single parameter?
e.g. this works:

```
[1, 2, 3] | any {$in == 1}
```

(Aside: re: "The `wrap` command converts a list to a table."
        `wrap` doesn't seem a particularly "obvious" name for this
         functionality. )



##### Other discoveries

 * It wasn't obvious to me that because it's valid for a closure to
   not take any parameters it's possible to not include the `||` part:

   ```
   # a valid no parameter closure
   each {}
   ```

   ```
   # *also* valid but more typing :)
   each {||}
   ```

   What makes this actually *useful* is that you can still get access
   to what ever is passed in via the magic `$in` variable:

   ```
   > ['a', 'b'] | each {$in + 'x'}
   ╭───┬────╮
   │ 0 │ ax │
   │ 1 │ bx │
   ╰───┴────╯
   ```

   Of course, this probably isn't a particularly *great* practice but
   it's handy when hacking things together. \*cough\* I mean,
   practicing an "iterative exploratory development" methodology.

   Oh, apparently there's *another* option, use `$in` _"implicitly"_ (via `help update`):

   ```
   Implicitly use the `$in` value in a closure to update 'authors'
   > [[project, authors]; ['nu', ['Andrés', 'JT', 'Yehuda']]] | update authors { str join ',' }
   ```

   via <https://www.nushell.sh/book/types_of_data.html#cell-paths>:

   > "You can also use a pipeline input as `$in` in **most** closures
   >  instead of providing an explicit parameter: `each { print $in }`"

   > "It is idiomatic to use `$it` as a parameter name in `each`
   >  blocks, but not required; [...]"

   Also via <https://www.nushell.sh/book/types_of_data.html#null>:

   > "`null` is not the same as the absence of a value!"



#### Appendix A: `ast` experiments & custom `ast-compare`/`ast-compare-inner` commands

This "appendix" is primarily here to remove the content out from the
flow of the "main text" and is slightly more of a "dump" for the sake
of expediency.

```
["sys", "(sys)"] | each { ast $in } | into record | flatten
```

```
def ast-compare [] { each { ast $in } | into record | flatten }
```

```
["sys", "(sys)"] | ast-compare
```

```
## Okay, this is pretty neat... Who needs a neanderthal text diff *now*? :D

ast "sys" --json | get block | from json | get pipelines
ast "(sys)" --json | get block | from json | get pipelines


ast "sys" --json | get block | from json | get pipelines.elements.0.expr
ast "(sys)" --json | get block | from json | get pipelines.elements.0.expr

# (Though I might've been inclined to choose a different default output format...)
```

```
# after some "spelunking"

> ast "sys" --json | get block | from json | get pipelines.elements.0.expr.0.expr | table --theme basic --expand # basic theme is less readable but more copypasta-able.

+------+--------------------------------------+
|      | +-------------+--------------------+ |
| Call | | decl_id     | 132                | |
|      | +-------------+--------------------+ |
|      | |             | +-------+--------+ | |
|      | | head        | | start | 116664 | | |
|      | |             | +-------+--------+ | |
|      | |             | | end   | 116667 | | |
|      | |             | +-------+--------+ | |
|      | +-------------+--------------------+ |
|      | | arguments   | [list 0 items]     | |
|      | +-------------+--------------------+ |
|      | | parser_info | {record 0 fields}  | |
|      | +-------------+--------------------+ |
+------+--------------------------------------+

> ast "(sys)" --json | get block | from json | get pipelines.elements.0.expr.0.expr | table --theme basic --expand
+--------------+----------------------------------------------------------------+
|              | +------+-----------------------------------------------------+ |
| FullCellPath | |      | +-------------------+-----------------------------+ | |
|              | | head | |                   | +---------------+------+    | | |
|              | |      | | expr              | | Subexpression | 1913 |    | | |
|              | |      | |                   | +---------------+------+    | | |
|              | |      | +-------------------+-----------------------------+ | |
|              | |      | |                   | +-------+--------+          | | |
|              | |      | | span              | | start | 116776 |          | | |
|              | |      | |                   | +-------+--------+          | | |
|              | |      | |                   | | end   | 116781 |          | | |
|              | |      | |                   | +-------+--------+          | | |
|              | |      | +-------------------+-----------------------------+ | |
|              | |      | |                   | +--------+----------------+ | | |
|              | |      | | ty                | | Record | [list 0 items] | | | |
|              | |      | |                   | +--------+----------------+ | | |
|              | |      | +-------------------+-----------------------------+ | |
|              | |      | | custom_completion |                             | | |
|              | |      | +-------------------+-----------------------------+ | |
|              | +------+-----------------------------------------------------+ |
|              | | tail | [list 0 items]                                      | |
|              | +------+-----------------------------------------------------+ |
+--------------+----------------------------------------------------------------+
```

```
# Probably more elegant ways to implement parts of this but still cool. :)

def ast-compare-inner [] { each { ast $in --json | get block | from json | get pipelines.elements.0.expr.0 } | select expr }
```

```
> ["sys", "(sys)"] | ast-compare-inner | table --theme basic --expand
+---+-----------------------------------------------------------------------------------+
| # |                                       expr                                        |
+---+-----------------------------------------------------------------------------------+
| 0 | +------+--------------------------------------+                                   |
|   | |      | +-------------+--------------------+ |                                   |
|   | | Call | | decl_id     | 132                | |                                   |
|   | |      | +-------------+--------------------+ |                                   |
|   | |      | |             | +-------+--------+ | |                                   |
|   | |      | | head        | | start | 118464 | | |                                   |
|   | |      | |             | +-------+--------+ | |                                   |
|   | |      | |             | | end   | 118467 | | |                                   |
|   | |      | |             | +-------+--------+ | |                                   |
|   | |      | +-------------+--------------------+ |                                   |
|   | |      | | arguments   | [list 0 items]     | |                                   |
|   | |      | +-------------+--------------------+ |                                   |
|   | |      | | parser_info | {record 0 fields}  | |                                   |
|   | |      | +-------------+--------------------+ |                                   |
|   | +------+--------------------------------------+                                   |
+---+-----------------------------------------------------------------------------------+
| 1 | +--------------+----------------------------------------------------------------+ |
|   | |              | +------+-----------------------------------------------------+ | |
|   | | FullCellPath | |      | +-------------------+-----------------------------+ | | |
|   | |              | | head | |                   | +---------------+------+    | | | |
|   | |              | |      | | expr              | | Subexpression | 2038 |    | | | |
|   | |              | |      | |                   | +---------------+------+    | | | |
|   | |              | |      | +-------------------+-----------------------------+ | | |
|   | |              | |      | |                   | +-------+--------+          | | | |
|   | |              | |      | | span              | | start | 118464 |          | | | |
|   | |              | |      | |                   | +-------+--------+          | | | |
|   | |              | |      | |                   | | end   | 118469 |          | | | |
|   | |              | |      | |                   | +-------+--------+          | | | |
|   | |              | |      | +-------------------+-----------------------------+ | | |
|   | |              | |      | |                   | +--------+----------------+ | | | |
|   | |              | |      | | ty                | | Record | [list 0 items] | | | | |
|   | |              | |      | |                   | +--------+----------------+ | | | |
|   | |              | |      | +-------------------+-----------------------------+ | | |
|   | |              | |      | | custom_completion |                             | | | |
|   | |              | |      | +-------------------+-----------------------------+ | | |
|   | |              | +------+-----------------------------------------------------+ | |
|   | |              | | tail | [list 0 items]                                      | | |
|   | |              | +------+-----------------------------------------------------+ | |
|   | +--------------+----------------------------------------------------------------+ |
+---+-----------------------------------------------------------------------------------+

```



#### Appendix B: Mostly "bare word" strings & related parsing tests/examples/errors

This "appendix" is primarily here to remove content out from the flow
of the "main text" and is more a "dump" for the sake of expediency.

```
> not 1==1
Error: nu::shell::type_mismatch

  × Type mismatch.
   ╭─[entry #1:1:5]
 1 │ not 1==1
   ·     ──┬─
   ·       ╰── expected bool, found string
   ╰────
```

```
> not (1==1)
Error: nu::parser::unknown_command

  × Unknown command.
   ╭─[entry #1:1:6]
 1 │ not (1==1)
   ·      ──┬─
   ·        ╰── unknown command
   ╰────
```

```
> if 1==1 {}
Error: nu::shell::cant_convert

  × Can't convert to bool.
   ╭─[entry #1:1:4]
 1 │ if 1==1 {}
   ·    ──┬─
   ·      ╰── can't convert string to bool
   ╰────
```

```
> not 1 == 1
Error: nu::shell::type_mismatch

  × Type mismatch.
   ╭─[entry #1:1:5]
 1 │ not 1 == 1
   ·     ┬
   ·     ╰── expected bool, found int
   ╰────
```

```
'where not foo'

'where not foo and foo'

'where not (foo and foo)'

'not (foo and foo)'

'(not foo and foo)'

'(not foo or foo)'

'not foo or foo'

'not true or true'

```

```
[ '1==1' , "[1==1]", "[ 1==1]", "[1==1 ]", "[ 1==1 ]", "['1==1' ]" ,  '(not 1==1)', 'not foo and (foo)', 'not (foo) or (foo)', 'not foo or (foo)', 'where not foo or foo', 'not foo or foo', 'where not foo' , 'where not foo and foo' , 'where not (foo and foo)' , 'not (foo and foo)' , '(not foo and foo)' , '(not foo or foo)' , 'not foo or foo' , 'not true or true' ] | each { ast $in | get error }
```

```
> not foo or foo
Error: nu::parser::unsupported_operation

  × boolean operation is not supported between bool and string.
   ╭─[entry #1:1:1]
 1 │ not foo or foo
   · ───┬─── ─┬ ─┬─
   ·    │     │  ╰── string
   ·    │     ╰── doesn't support these values
   ·    ╰── bool
   ╰────
```

```
> where not foo or foo
Error: nu::parser::unsupported_operation

  × boolean operation is not supported between bool and string.
   ╭─[entry #1:1:7]
 1 │ where not foo or foo
   ·       ───┬─── ─┬ ─┬─
   ·          │     │  ╰── string
   ·          │     ╰── doesn't support these values
   ·          ╰── bool
   ╰────
```

```
> not foo or (foo)
Error: nu::shell::type_mismatch

  × Type mismatch.
   ╭─[entry #1:1:5]
 1 │ not foo or (foo)
   ·     ─┬─
   ·      ╰── expected bool, found string
   ╰────
```

```
ast 'not foo or (foo)'
```

```
ast 'not (foo) or (foo)'
```

```
> not (foo) or (foo)
Error: nu::shell::external_command

  × External command failed
   ╭─[entry #1:1:6]
 1 │ not (foo) or (foo)
   ·      ─┬─
   ·       ╰── did you mean 'for'?
   ╰────
  help: No such file or directory (os error 2)
```

```
> not foo and (foo)
Error: nu::shell::type_mismatch

  × Type mismatch.
   ╭─[entry #1:1:5]
 1 │ not foo and (foo)
   ·     ─┬─
   ·      ╰── expected bool, found string
   ╰────
```

(lol, I *only* just realised the default timestamp date format doesn't match the locale. :D )
(Update: Maybe related to "(datetimes will similarly use the default config)" but also probably not.)


```
ast '(not 1==1)'
```

```
# wtf?
> ['1==1']
Error: nu::parser::unknown_command

  × Unknown command.
   ╭─[entry #1:1:1]
 1 │ ['1==1']
   · ────┬───
   ·     ╰── unknown command
   ╰────
```

```
# oh, of *course*... :p
> [ '1==1']
╭───┬──────╮
│ 0 │ 1==1 │
╰───┴──────╯
```

```
# I wasn't even *trying* to break anything, I just wanted to loop through a list of test cases.. :/
ast "['1==1']" # unknown command
ast ['1==1'] # unknown command
ast [ '1==1'] # ok
ast ['1==1' ] # *also* ok
ast "['1==1' ]" # *also* ok

```

```
> [ '1==1' , "[1==1]", "[ 1==1]", "[1==1 ]", "[ 1==1 ]", "['1==1' ]" ,  '(not 1==1)', 'not foo and (foo)', 'not (foo) or (foo)', 'not foo or (foo)', 'where not foo or foo', 'not foo or foo', 'where not foo' , 'where not foo and foo' , 'where not (foo and foo)' , 'not (foo and foo)' , '(not foo and foo)' , '(not foo or foo)' , 'not foo or foo' , 'not true or true' ] | each { [ [ "original" , "error" ]; [ $in, ( ast $in --json | get error | (from json | if $in != null {columns | first}) ) ] ]} | flatten
╭────┬─────────────────────────┬─────────────────────────╮
│  # │        original         │          error          │
├────┼─────────────────────────┼─────────────────────────┤
│  0 │ 1==1                    │ UnknownCommand          │
│  1 │ [1==1]                  │ UnknownCommand          │
│  2 │ [ 1==1]                 │                         │
│  3 │ [1==1 ]                 │                         │
│  4 │ [ 1==1 ]                │                         │
│  5 │ ['1==1' ]               │                         │
│  6 │ (not 1==1)              │                         │
│  7 │ not foo and (foo)       │                         │
│  8 │ not (foo) or (foo)      │                         │
│  9 │ not foo or (foo)        │                         │
│ 10 │ where not foo or foo    │ UnsupportedOperationRHS │
│ 11 │ not foo or foo          │ UnsupportedOperationRHS │
│ 12 │ where not foo           │                         │
│ 13 │ where not foo and foo   │ UnsupportedOperationRHS │
│ 14 │ where not (foo and foo) │                         │
│ 15 │ not (foo and foo)       │                         │
│ 16 │ (not foo and foo)       │ UnsupportedOperationRHS │
│ 17 │ (not foo or foo)        │ UnsupportedOperationRHS │
│ 18 │ not foo or foo          │ UnsupportedOperationRHS │
│ 19 │ not true or true        │                         │
╰────┴─────────────────────────┴─────────────────────────╯

#----

# But when entered directly:

> not (foo and foo) # --> Error: nu::shell::external_command (#15)

> where not (foo and foo) # --> Error: nu::shell::only_supports_this_input_type (#14)
> [] | where not (foo and foo) # --> no error, empty list (#14)
> [ 1 ] | where not (foo and foo) # --> Error: nu::shell::external_command (#14)

> [ 1 ] | where not foo # --> Error: nu::shell::incompatible_path_access / "Data cannot be accessed with a cell path" /  "int doesn't support cell paths" (#12)
> [ [] ] | where not foo # --> Error: nu::shell::type_mismatch / "expected bool, found list<any>" (#12)
> [ {} ] | where not foo # --> Error: nu::shell::column_not_found / "cannot find column 'foo'" (#12)
> [ {} ] | where not foo? # --> Error: nu::shell::type_mismatch / "expected bool, found nothing" (#12)
> [ { foo: 0 } ] | where not foo # --> Error: nu::shell::type_mismatch / "expected bool, found int" (#12)
> [ { foo: false } ] | where not foo # ok (#12)

> not foo or (foo) # --> Error: nu::shell::type_mismatch / "expected bool, found string" (#9)

> not (foo) or (foo) # --> Error: nu::shell::external_command (#8)

> not foo and (foo) # --> Error: nu::shell::type_mismatch / "expected bool, found string" (#7)

> (not 1==1) # --> Error: nu::shell::type_mismatch / "expected bool, found string" (#6)

> (not (1 == 1)) # ok (#6a)

# Stopped here, for now.


## also note:

> 1==1 # --> Error: nu::parser::unknown_command

# vs

> ==1 # --> Error: nu::shell::external_command

```

An alternative testing pipeline trying out using `wrap` (not really worth it..?):

```
> [ '1==1' , "[1==1]", "[ 1==1]", "['1==1' ]" ] | wrap "original" | insert 'error' { ast $in.original --json | get error | from json | if $in != null {columns | first}}
╭───┬───────────┬────────────────╮
│ # │ original  │     error      │
├───┼───────────┼────────────────┤
│ 0 │ 1==1      │ UnknownCommand │
│ 1 │ [1==1]    │ UnknownCommand │
│ 2 │ [ 1==1]   │                │
│ 3 │ ['1==1' ] │                │
╰───┴───────────┴────────────────╯
```


Keeping to remember how I got `merge` to work for at least the first step--but the whole null-value filtering out still needs to happen, so it's no better, I don't think?

```
> [ '1==1' , "[1==1]", "[ 1==1]", "['1==1' ]" ] | wrap "original" | merge ( $in | each {ast --json $in.original | reject block } )

╭───┬───────────┬───────────────────────╮
│ # │ original  │         error         │
├───┼───────────┼───────────────────────┤
│ 0 │ 1==1      │ {                     │
│   │           │   "UnknownCommand": { │
│   │           │     "start": 156264,  │
│   │           │     "end": 156268     │
│   │           │   }                   │
│   │           │ }                     │
│ 1 │ [1==1]    │ {                     │
│   │           │   "UnknownCommand": { │
│   │           │     "start": 156264,  │
│   │           │     "end": 156270     │
│   │           │   }                   │
│   │           │ }                     │
│ 2 │ [ 1==1]   │ null                  │
│ 3 │ ['1==1' ] │ null                  │
╰───┴───────────┴───────────────────────╯

```

```
> [ '1==1' , "[1==1]", "[ 1==1]", "['1==1' ]" ] | wrap "original" | merge ( $in | each {ast --json $in.original | reject block | update error { from json |   if $in != null {columns | first}    } })
```

```
> [ '1==1' , "[1==1]", "[ 1==1]", "['1==1' ]" ] | wrap "original" | merge ( $in | each {ast --json $in.original } ) | reject block | update error { from json |   if $in != null {columns | first}    }
```

```
> [ '1==1' , "[1==1]", "[ 1==1]" ] | wrap "original" | merge ( $in | insert "error" { ( ast --json $in.original).error | from json | if $in != null {columns | first}  }  )
╭───┬──────────┬────────────────╮
│ # │ original │     error      │
├───┼──────────┼────────────────┤
│ 0 │ 1==1     │ UnknownCommand │
│ 1 │ [1==1]   │ UnknownCommand │
│ 2 │ [ 1==1]  │                │
╰───┴──────────┴────────────────╯
```

```
> [ '1==1' , "[1==1]", "[ 1==1]" ] | wrap "original" | merge ( $in | insert "error" { $in.original | ast-error-if-exists  }  )
```


note: `select select` preserves the input as a table but `get error`/`.error` turns it into list, IIUC.
      (so, for `merge` select was preferable, but `merge` didn't seem to offer much/any benefit in this situation...)

```
def ast-error-if-exists [] { ( ast --json $in).error | from json | if $in != null {columns | first} }

> [ '1==1' , "[1==1]", "[ 1==1]" ] | wrap "original" | insert "error" { $in.original | ast-error-if-exists  }

╭───┬──────────┬────────────────╮
│ # │ original │     error      │
├───┼──────────┼────────────────┤
│ 0 │ 1==1     │ UnknownCommand │
│ 1 │ [1==1]   │ UnknownCommand │
│ 2 │ [ 1==1]  │                │
╰───┴──────────┴────────────────╯
```

So, really, this version the uses `{x:1}`-style table creation rather than `[[x];[1]]` and/or `wrap` seems *way* simpler:

```
def ast-error-if-exists [] { ( ast --json $in).error | from json | if $in != null {columns | first} }

> [ '1==1' , "[1==1]", "[ 1==1]" ] | each { { original: $in, error: ( $in | ast-error-if-exists ) } }

╭───┬──────────┬────────────────╮
│ # │ original │     error      │
├───┼──────────┼────────────────┤
│ 0 │ 1==1     │ UnknownCommand │
│ 1 │ [1==1]   │ UnknownCommand │
│ 2 │ [ 1==1]  │                │
╰───┴──────────┴────────────────╯

```

According to <https://www.nushell.sh/book/types_of_data.html#records>:

> "[...] iterate over records by first transposing it into a table: [..]"


So, I tried that and it avoids the null check & column stuff, but then
having to use `str join` at the end seems a bit meh:

```
def ast-error-if-exists [] { ( ast --json $in).error | from json | (transpose key value).key | str join }

> [ '1==1' , "[1==1]", "[ 1==1]" ] | each { { original: $in, error: ( $in | ast-error-if-exists ) } }

╭───┬──────────┬────────────────╮
│ # │ original │     error      │
├───┼──────────┼────────────────┤
│ 0 │ 1==1     │ UnknownCommand │
│ 1 │ [1==1]   │ UnknownCommand │
│ 2 │ [ 1==1]  │                │
╰───┴──────────┴────────────────╯
```

"Interestingly", the output is different as `describe` shows:

```
def ast-error-if-exists [] { ( ast --json $in).error | from json | if $in != null {columns | first} }

> [ '1==1' , "[1==1]", "[ 1==1]" ] | each { { original: $in, error: ( $in | ast-error-if-exists ) } } | describe
list<any> (stream)
```

vs

```
def ast-error-if-exists [] { ( ast --json $in).error | from json | (transpose key value).key | str join }

> [ '1==1' , "[1==1]", "[ 1==1]" ] | each { { original: $in, error: ( $in | ast-error-if-exists ) } } | describe

table<original: string, error: string> (stream)
```

So, I guess that's goood...? \*shrug\*


```
> "[1==1]" | (ast $in --json).error | from json | match $in {null => {}, _ => { items {|k, v| $k} }}
╭───┬────────────────╮
│ 0 │ UnknownCommand │
╰───┴────────────────╯
> "[ 1==1]" | (ast $in --json).error | from json | match $in {null => {}, _ => { items {|k, v| $k} }}
>
```


#### Appendix C: Job Control

`nushell` does not currently support Job Control (e.g. `Ctrl-Z`, `fg`, `bg` & `&` & friends).

Apparently this is a possible alternative way to still ~get job control (currently untested by me):

 * [`yshui/job-security`](https://github.com/yshui/job-security) -- "job control from anywhere" ([via](https://news.ycombinator.com/item?id=40317109))
