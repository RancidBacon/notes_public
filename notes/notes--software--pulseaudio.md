# Notes: Software : PulseAudio


### Temporary `module-match` configuration

```
#
# File path of this file: `~/.config/pulse/match.table`
#

#
# use via:
#
#   pacmd load-module module-match table=~/.config/pulse/match.table key=event.id
#
# also:
#
#   pacmd unload-module module-match
#
#   pacmd set-log-level 4 # "debug"
#   pacmd set-log-level 2 # "notice" (I think) a.k.a default
#

#
# Prevent "pop" sound sample[0] from being audible on media key volume changes.
#
# (For Gnome versions without the configurable "quiet" shortcut [hardcoded to ALT+<shortcut>].)
#
# [0] i.e. `/usr/share/sounds/freedesktop/stereo/audio-volume-change.oga`
#       or `/usr/share/sounds/elementary/stereo/audio-volume-change.wav`
#
^audio-volume-change 0
```

Notes:

 * Seems the `key=` argument isn't documented for [`module-match`](https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/User/Modules/#module-match)
   despite [being added 16+ years ago](https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/commit/e0f8ffe41f99789fafac575e944acf02e940bbf7). :D

   (Also, seemingly sample "names"/"titles" _don't_ appear to start
   with `sample:`?)

 * The default key appears to be `media.name` but for
   `audio-volume-change` the value for both it & `event.id` are the
   same, so it seemed "better" to be more explicit with the match.

 * Apparently `module-match` has also had support for ~13 years for
   ["`merge` or `replace` mode"](https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/commit/2ee398fd94fd9bbfaff61d0743c945f0a3411fa8). (I've
   not used it, just happened to notice it in the source code "blame"
   listing.)

 * [List of (predefined) `proplist` key names](https://freedesktop.org/software/pulseaudio/doxygen/proplist_8h.html)
   (via "[Application Properties – Developer Documentation – PulseAudio](https://www.freedesktop.org/wiki/Software/PulseAudio/Documentation/Developer/Clients/ApplicationProperties/)")

 * Turns out PulseAudio does actually correctly use `~/.config/pulse/`
   rather than `~/.pulse/`, so that would probably be a "better"
   location to use?


### Permanent `module-match` configuration

For "permanent" use it's possible to use a `default.pa` user
script--although this was not as straight-forward as anticipated:

```
#
# File path of this file: `.config/pulse/default.pa`
#

# via <https://wiki.archlinux.org/title/PulseAudio/Examples#Creating_user_configuration_files>
.include /etc/pulse/default.pa

#
# It turns out that use of (at least) `load-module module-match` via
# the CLI vs via `default.pa` is not directly interchangeable.
#
# Specifically, while it's possible to supply the `table` arg via the
# CLI with `table=~/.pulse/match.table`, this form does not work in a
# `default.pa` file. AFAICT this is due to (when used in `default.pa`):
#
#  * The `~` does not get expanded (nor does `$HOME`).
#
#  * A bare `match.table` does not appear to use the config directory.
#
#  * The filename/filepath seems to get used directly and without any
#    further processing/path normalization.
#
# Unfortunately this seems to mean that it's not possible to
# explicitly document the default file name used in the `default.pa`
# itself.
#
# This was the command + arg form original used via `pacmd`:
#
#   load-module module-match table=~/.config/pulse/match.table key=event.id
#

#
# Prevent "pop" sound sample from being audible on media key volume changes
# (also requires accommpanying `~/.config/pulse/match.table` file).
#
# via `~/.config/pulse/match.table` and <https://gitlab.com/RancidBacon/notes_public/-/blob/main/notes/notes--software--pulseaudio.md>
#

load-module module-match key=event.id
```

With the content of `~/.config/pulse/match.table` being as presented
earlier on this page.

You'll also need to reboot or `pulseaudio --kill` (and possibly kill
`gsd-media-keys` also (*and* also double check the expected audio
device is selected/enabled afterward)).


### Links

Related links:

 * This comment started me down the road of a "PulseAudio
   sample"-based solution:
   "[pulseaudio - How to mute sound effects in pavucontrol?
    ](https://unix.stackexchange.com/questions/505921/how-to-mute-sound-effects-in-pavucontrol#505943)"

 * The most common but "less elegant" suggested solution:

    * "[unity - How can I disable the "popping" sound when adjusting the volume?
       ](https://askubuntu.com/questions/61878/how-can-i-disable-the-popping-sound-when-adjusting-the-volume)"

 * Mention of `libcanberra-pulse`:

    * "[How to enable volume adjust sound effect for keyboard shortcuts on Ubuntu 18.04
       ](https://askubuntu.com/questions/1043578/how-to-enable-volume-adjust-sound-effect-for-keyboard-shortcuts-on-ubuntu-18-04#1089239)"

 * Code related to loading of `match.table`/processing of `table`
   filename/path arg:

    * `load_rules()`: <https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/blob/3e2bb8a1ece02ae4e2c217273c9c0929cb9f5cae/src/modules/module-match.c#L82-94>

    * `pa_open_config_file()`: <https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/blob/3e2bb8a1ece02ae4e2c217273c9c0929cb9f5cae/src/pulsecore/core-util.c?page=2#L1941-2025>

      Note that this checks more paths than explcitly documented.

    * `get_pulse_home()`: <https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/blob/440f37af00e798ad91580b82e7414d25990b8cce/src/pulsecore/core-util.c#L1568-1597>

      ( + `pa_get_home_dir_malloc()` + `pa_get_home_dir()` + `pa_append_to_home_dir()` + `pa_get_config_home_dir()` + `pa_append_to_config_home_dir()` )

    * `pa_fopen_cloexec()`: <https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/blob/3e2bb8a1ece02ae4e2c217273c9c0929cb9f5cae/src/pulsecore/core-util.c?page=4#L3730-3753>

    * Also, `pa_module_load()`: <https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/blob/3e2bb8a1ece02ae4e2c217273c9c0929cb9f5cae/src/pulsecore/module.c#L115>

    * Note: This `normalize_path()` function is *not* used here:
            <https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/blob/3e2bb8a1ece02ae4e2c217273c9c0929cb9f5cae/src/pulsecore/authkey.c#L132-143>

    * Semi-related: <https://gitlab.freedesktop.org/pulseaudio/pulseaudio/-/issues/1095#note_762891>


Other semi-/un-related links:

 * [`module-device-manager` mention
   ](https://www.freedesktop.org/wiki/Software/PulseAudio/RFC/PriorityRouting/#imgoingtomakeaneditorfortheprioritylistshowcanigetnicenamesforthedeviceportentriesthatarenotcurrentlyavailable)
   (& [another here](https://web.archive.org/web/20200226020948/https://colin.guthr.ie/2010/02/this-is-the-route-to-hell/))

 * <https://cgit.freedesktop.org/pulseaudio/pulseaudio/tree/doc/stream_restore_fallback_table_example.table>
