## Notes: Uncategorized

### Linux / system related

 * [binpash/try](https://github.com/binpash/try) -- "...lets you run a command and inspect its effects before changing your live system...uses Linux's namespaces (via unshare) and the overlayfs union filesystem."



### Firefox related

 * "Can I define Alt key menu keyboard shortcuts for bookmarks in Firefox 3.6? - Super User":
   <https://superuser.com/questions/146769/can-i-define-alt-key-menu-keyboard-shortcuts-for-bookmarks-in-firefox-3-6#156202>

   Closest thing to "activate javascript bookmarklet by keyboard
   shortcut" I've found so far:

    * `Alt-B` then first letter of bookmarklet title e.g. `l` for
      `link-md`, or press `l` a second time for next title match, e.g. `link`.



### Terminal emulator related

 * Apparently "ANSI Music" is/was a thing: :D

    * <http://www.bbsdocumentary.com/library/PROGRAMS/GRAPHICS/ANSI/ansimusic.txt>

    * <http://www.textfiles.com/artscene/ansimusic/information/dybczak.txt>

    * <https://web.archive.org/web/20220130155836/https://github.com/klamonte/qodem/blob/master/source/music.c>
